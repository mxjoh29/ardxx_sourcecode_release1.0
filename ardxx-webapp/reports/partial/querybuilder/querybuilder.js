angular.module('reports').controller('QuerybuilderCtrl',function($scope){

	$scope.entitiesList = [
		{
			name:'Protocol',
			columns: [
				{name:'Protocol ID', type: 'number'},
				{name:'Code', type: 'string'},
				{name:'Approval Date', type: 'date'}, 
				{name:'Expiration Date', type: 'date'}, 
				{name:'Status', type: 'string'}
			]
		},
		{
			name:'Study',
			columns: [
				{name:'Number', type: 'number'},
				{name:'Name', type: 'string'},
				{name:'Charge Code', type: 'string'},
				{name:'PI', type: 'string'},
				{name:'Creation Date', type: 'date'},
				{name:'Closed Date', type: 'date'}
			]
		},
		{
			name:'Procedure',
			columns: [
				{name:'Id', type: 'number'},
				{name:'Type', type: 'string'},
				{name:'Lab Technician', type: 'string'},
				{name:'Vet Technician', type: 'string'},
				{name:'Status', type: 'string'}
			]
		},
		{
			name:'Animal',
			columns: [
				{name:'New Id', type: 'number'},
				{name:'Old Id', type: 'string'},
				{name:'Species', type: 'string'},
				{name:'Sex', type: 'string'},
				{name:'Client', type: 'string'},
				{name:'Status', type: 'string'}
			]
		}
	];

	$scope.filterItems = ['equal to','starts with', 'ends with', 'between', 'less than', 'more than'];

	$scope.selectedColumns = [];

	$scope.addSelectedColumns = function (colName, entityName){
		$scope.selectedColumns.push({'sl':$scope.selectedColumns.length, 'column':colName, 'entity':entityName});
	};

	$scope.deleteSelectedColumn = function(item){
		var index = $scope.selectedColumns.indexOf(item);
  		$scope.selectedColumns.splice(index, 1);
	};

	$scope.selectedFilters = [];

	$scope.addSelectedFilters = function (colName, entityName, type){
		$scope.selectedFilters.push({'sl':$scope.selectedColumns.length, 'column':colName,'colType':type, 'entity':entityName, 'filterType':'equal to'});
	};

	$scope.deleteSelectedFilter = function(item){
		var index = $scope.selectedFilters.indexOf(item);
  		$scope.selectedFilters.splice(index, 1);
	};

	window.scope = $scope;
});