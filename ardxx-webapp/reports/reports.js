angular.module('reports', ['ui.bootstrap','ui.utils','ui.router','ngAnimate']);

angular.module('reports').config(function($stateProvider) {

    $stateProvider.state('querybuilder', {
        url: '/report-query',
        templateUrl: 'reports/partial/querybuilder/querybuilder.html',
        data: { pageTitle: 'Query Builder' }
    });
    /* Add New States Above */

});

