angular.module('protocol').factory('protocolService', function ($http, configService, $q, $rootScope) {

    var protocolService = {};

    protocolService.list = function () {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'protocol?page=0&size=100')
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    protocolService.details = function (id) {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'protocol/' + id)
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    protocolService.getUsersByRoleId = function (id) {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'users/piusers/' + id)
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    protocolService.getProtocolsByPIUser = function (id) {
        var deferred = $q.defer();
        var url = configService.getHostName + 'protocol';
        var filterCriteria = {};
        if ($rootScope.currentUser.role.indexOf('PRI_INV') > -1) {
            url = configService.getHostName + 'protocol/piuser/' + id;
        } else if ($rootScope.currentUser.role.indexOf('CHR_PER') > -1) {
            url = configService.getHostName + 'protocol';
            filterCriteria.status = ['REVIEW'];
        } else if ($rootScope.currentUser.role.indexOf('COM_MEM') > -1) {
            url = configService.getHostName + 'protocol';
            filterCriteria.status = ['PRE_REVIEW'];
        }
        $http.get(url)
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    protocolService.create = function (userId) {
        var deferred = $q.defer();
        $http.post(configService.getHostName + 'protocol', { "userId": userId })
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    protocolService.updateProtocol = function (section, sectionId, fieldName, fieldValue, protocolId) {
        var deferred = $q.defer();
        var url;
        if (section === '') {
            url = configService.getHostName + 'protocol/' + protocolId;
        } else {
            url = configService.getHostName + 'protocol/' + protocolId + '/' + section + '/' + sectionId;
        }
        $http.put(url, {
            'fieldName': fieldName,
            'fieldValue': fieldValue
        }).
			success(function (data, status, headers, config) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});

        return deferred.promise;
    };
    
    protocolService.createPersonals = function (protocolId, adminId, personalId, personalValue) {
        var deferred = $q.defer();
        $http.put(configService.getHostName + 'protocol/' + protocolId + '/protocolAdministrativeData/'+ adminId + '/animalProceduresPersonals/' + (personalId? personalId : '-1'), {'fieldName':'animalProceduresPersonals','fieldValue':personalValue})
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };
   protocolService.createcoInvestigators = function (protocolId, adminId, investigatorId, investigatorValue) {
        var deferred = $q.defer();
        $http.put(configService.getHostName + 'protocol/' + protocolId + '/protocolAdministrativeData/'+ adminId + '/coInvestigators/' + (investigatorId? investigatorId : '-1'), {'fieldName':'coInvestigators','fieldValue':investigatorValue})
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };
      protocolService.createotherPersonals = function (protocolId, adminId, otherPersonalId, otherPersonalValue) {
        var deferred = $q.defer();
        $http.put(configService.getHostName + 'protocol/' + protocolId + '/protocolAdministrativeData/'+ adminId + '/otherPersonals/' + (otherPersonalId? otherPersonalId : '-1'), {'fieldName':'otherPersonals','fieldValue':otherPersonalValue})
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    protocolService.createAnimalRequirements = function (protocolId) {
        var deferred = $q.defer();
        $http.put(configService.getHostName + 'protocol/' + protocolId + '/animalRequirements/-1', {})
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    protocolService.updatePi = function (protocolId, pid) {
        var deferred = $q.defer();
        $http.put(configService.getHostName + 'protocol/' + protocolId + '/piuser/' + pid, {})
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    protocolService.getProtocolByUserAndStatus = function (userId, status) {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'protocol/piuser/' + userId + '/status/' + status)
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    protocolService.studyObjectsQuestions = [
		{ inputId: '-1', id: '1', value: 'Provide no more than a 300 word summary of the objectives of this work. Why is this work important/interesting? How might this work benefithumans and/or animals? This should be written so that a non-scientist can easily understand it. Please eliminate or minimize abbreviations, technical terms, and jargon. Where necessary, they should be defined.' },
		{ inputId: '-1', id: '2', value: 'For renewal proposals, provide a brief summary of recent progress made using the last three-years’ animals.' },
		{ inputId: '-1', id: '3', value: 'For new and renewal proposals, a brief synopsis of the scientific rationale or foundation for the study may optionally be provided.' },
		{ inputId: '-1', id: '4', value: 'For modifications, provide a summary of why the modification is required.' }
    ];

    protocolService.rationaleQuestions = [
		{ inputId: '-1', id: '1', value: 'Explain your rationale for animal use.' },
		{ inputId: '-1', id: '2', value: 'Justify the appropriateness of the species selected.' },
		{ inputId: '-1', id: '3', value: 'Justify the number of animals to be used.' }
    ];

    protocolService.experimentalDesignQuestions = [
		{ inputId: '-1', id: '1', value: 'Briefly explain the experimental design.', hint: 'An outline, table or flowchart presentation might be helpful' },
		{ inputId: '-1', id: '2', value: 'Injections or inoculations', hint: '(substances, e.g., infectious agents, adjuvants, etc.; dose, sites, volume, route, and schedules)' },
		{ inputId: '-1', id: '3', value: 'Blood withdrawals', hint: '(volume, frequency, withdrawal sites, and methodology)' },
		{ inputId: '-1', id: '4', value: 'Minor surgical procedures', hint: '(that do not penetrate and expose a body cavity)' },
		{ inputId: '-1', id: '5', value: 'Non-survival surgical procedures', hint: '(Provide details of survival surgical procedures in Section G.)' },
		{ inputId: '-1', id: '6', value: 'Radiation', hint: '(dosage and schedule)' },
		{ inputId: '-1', id: '7', value: 'Food/fluid restriction', hint: '(If food and/or fluid will be restricted, describe method for assessing the health and wellbeing of the animalsrelative to the guide).' },
		{ inputId: '-1', id: '8', value: 'Methods of restraint', hint: '(e.g., restraint chairs, manual restraint, chemical restraint, collars, vests, harnesses, slings, etc.)' },
		{ inputId: '-1', id: '9', value: 'Animal identification methods', hint: '(e.g., ear tags, tattoos, collar, cage card, etc.)' },
		{ inputId: '-1', id: '10', value: 'Other procedures', hint: '(e.g., breeding, genotyping, etc.)' },
		{ inputId: '-1', id: '11', value: 'Potentially Painful or Distressful Effects,', hint: 'if any, the animals are expected to experience (e.g., pain or discomfort, ascitesproduction, etc.), by experimental procedure if applicable. For Column E studies provide: a. A description of the procedure(s)producing pain and/or distress; b. Scientific justification why pain and/or distress cannot be relieved.' },
		{ inputId: '-1', id: '12', value: 'Veterinary Care', hint: '(Indicate the plan of action in case of animal illness, e.g., initiate treatment, call investigator prior to initiatingtreatment, euthanize, etc.).' },
		{ inputId: '-1', id: '13', value: 'Experimental endpoint criteria', hint: '(i.e., tumor size, percentage body weight gain or loss, inability to eat or drink, behavioralabnormalities, clinical signalment, or signs of toxicity) must be specified when the administration of tumor cells, biologics,infectious agents, radiation, or toxic chemicals are expected to cause significant clinical signs or are potentially lethal. List thecriteria to be used to determine when euthanasia is to be performed. Death as an endpoint must always be scientifically justified.List by experimental procedure if applicable.' },
		{ inputId: '-1', id: '14', value: 'Disposition of Animals at the End of the Study', hint: '(for those animals not meeting Experimental Endpoint Criteria above, e.g. retiredbreeders, etc.) not expected to experience clinical signs:' }
    ];

    protocolService.surgeryQuestions = [
		{ inputId: '-1', id: '1', value: 'Describe the surgical procedure(s) to be performed. Include the aseptic methods to be utilized.' },
		{ inputId: '-1', id: '2', value: 'Who will perform surgery and what are their qualifications and/or experience?' },
		{ inputId: '-1', id: '3', value: 'Where will surgery be performed (Building and Room)?' },
		{ inputId: '-1', id: '4', value: 'Describe post-operative care required, including consideration of the use of post-operative analgesics, and identify theresponsible individual:' },
		{ inputId: '-1', id: '5', value: 'Has major survival surgery been performed on any animal prior to being placed on this study?' },
		{ inputId: '-1', id: '6', value: 'Will more than one major survival surgery be performed on an animal while on this study?' }
    ];

    protocolService.questions = {};

    protocolService.questions.painAndDistress = [
		{ inputId: '-1', id: '1', value: 'USDA Column B', hint: 'No Pain or Distress' },
		{ inputId: '-1', id: '2', value: 'USDA Column C', hint: 'Slight (Minimal), Momentary (Transient), or No Pain or Distress' },
		{ inputId: '-1', id: '3', value: 'USDA Column D', hint: 'Pain or Distress Relieved By Appropriate Measures' },
		{ inputId: '-1', id: '4', value: 'USDA Column E', hint: 'Unrelieved Pain or Distress' }
    ];

    return protocolService;
});
