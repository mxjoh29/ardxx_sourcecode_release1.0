angular.module('protocol').controller('ProtocoldetailsCtrl', function ($scope, $rootScope, protocolService, $stateParams, $filter, toaster, pdfService, userService, $cookieStore, $state) {

    $scope.speciesList = [{ name: 'Chinese Rhesus' }, { name: 'Cynomolgus (Cyno)' }, { name: 'Ferret' }, { name: 'Guinea Pig' }, { name: 'Hamster' }, { name: 'Indian Rhesus' }, { name: 'Mouse' }, { name: 'Pigtail' }, { name: 'Rabbit' }, { name: 'Rat' }];
    $scope.sexList =[{name:'Male'},{name:'Female'},{name:'Unknown'}];
    $scope.viralstatusList=[{name:'Positive'},{name:'Negative'}];
    var roles = {
        chairmanRole: 'CHR_PER',
        piRole: 'PRI_INV',
        memberRole: 'COM_MEM',
        labRole :'LAB_VET',
        researchRole:'RES_SCI'
    };

    $scope.isChairman = $rootScope.currentUser.role.indexOf(roles.chairmanRole) > -1;
    $scope.isCM = $rootScope.currentUser.role.indexOf(roles.memberRole) > -1;
    $scope.isPI = $rootScope.currentUser.role.indexOf(roles.piRole) > -1;
    $scope.isLab = $rootScope.currentUser.role.indexOf(roles.labRole) > -1;
    $scope.isRes = $rootScope.currentUser.role.indexOf(roles.researchRole) > -1;




    $scope.protocolId = $stateParams.id;
    $scope.code = $stateParams.code;
    $scope.protocolType = $stateParams.type;
    window.scope = $scope;
    $scope.species = protocolService.species;

    $scope.statuses = [];

    $scope.individuals = [];

    $scope.otherPersonnels = [];

    $scope.newCommitteeMember = {
        username: '',
        otherUsername: ''
    };

    $scope.addCommitteeMember = function () {
        if (typeof $scope.details.review === 'undefined' || $scope.details.review === null) {
            $scope.details.review = { userNames: [] };
        }
        $scope.details.review.userNames.push(angular.copy($scope.newCommitteeMember.username === 'Others' ? $scope.newCommitteeMember.otherUsername : $scope.newCommitteeMember.username));

        $scope.updateReviewUser('userNames', $scope.newCommitteeMember.username === 'Others' ? $scope.newCommitteeMember.otherUsername : $scope.newCommitteeMember.username);
        $scope.newCommitteeMember = {
            username: '',
            otherUsername: ''
        };
    };



    $scope.getUsersList = function () {
        userService.usersList()
            .then(function (data) {
                if (data) {
                    $scope.usersList = data;
                }
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
    };
    $scope.getUsersList();
    $scope.getUserRoles = function () {
        userService.rolesList()
            .then(function (data) {
                if (data) {
                    $scope.userRoles = data;
                }
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
    };
    $scope.getUserRoles();
    $scope.removeAnimal = function (index) {
        $scope.details.animalRequirements.splice(index, 1);
    };

    $scope.AddAnimal = function () {
        protocolService.createAnimalRequirements($scope.protocolId)
            .then(function (animalRequirements) {
                if (!$scope.details.animalRequirements) {
                    $scope.details.animalRequirements = [];
                }
                $scope.details.animalRequirements.push(animalRequirements);
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });

    };

    $scope.newUserComment = { text: '', user: '', addedDate: '04/06/2015' };

    $scope.studyObjectsQuestions = protocolService.studyObjectsQuestions;

    $scope.rationaleQuestions = protocolService.rationaleQuestions;

    $scope.experimentalDesignQuestions = protocolService.experimentalDesignQuestions;

    $scope.surgeryQuestions = protocolService.surgeryQuestions;

    $scope.updateProtocolDetails = function (fieldName, fieldValue) {
        protocolService.updateProtocol('', '', fieldName, fieldValue, $scope.protocolId)
            .then(function (data) {
                bindProtocolDetails(data);

                if (fieldName === 'status' && fieldValue === 'APPROVED') {
                    toaster.pop('success', 'Approved', 'Protocol approved successfully.');
                } else if (fieldName === 'status' && fieldValue === 'REVIEW') {
                    toaster.pop('success', 'Review', 'Protocol sent for review successfully.');
                } else if (fieldName === 'status' && fieldValue === 'PRE_REVIEW') {
                    toaster.pop('success', 'Pre-Review', 'Protocol set to Pre-Review state successfully.');
                }
                if (fieldName === 'status') {
                    $scope.details.status = fieldValue;
                    //var globals = $cookieStore.get('globals');
                    protocolService.list()
                        .then(function (protocolList) {
                            $scope.protocols = protocolList;
                            $cookieStore.put('protocols', $scope.protocols);
                            $rootScope.allProtocols = protocolList;
                            if ($scope.isPI || $scope.isLab ||$scope.isRes) {
                                $rootScope.protocols = $filter('filter')(protocolList, { status: 'PENDING' });
                            } else if ($scope.isChairman) {
                                $rootScope.protocols = $filter('filter')(protocolList, { status: 'REVIEW' });
                            }
                        },
                            function (data) {
                                toaster.pop('error', 'Error', data.errorMessage);
                            });
                }
            }, function (data) {
                toaster.pop('error', 'Error', data.errorMessage);
                if (!data.errorMessage) {
                    bindProtocolDetails(data);
                }
            });

    };

    $scope.updateAdminstrativeData = function (fieldName, fieldValue) {
        protocolService.updateProtocol('protocolAdministrativeData', $scope.details.protocolAdministrativeData.id, fieldName, fieldValue, $scope.protocolId);
    };
    
    $scope.updateAdminstrativePersonals = function (personalValues) {
        var usr = personalValues;
            var isExists = false;
            angular.forEach(usr, function (value) {
                console.log(value, 'user');
                if (angular.isNumber(parseInt(value)) && !isNaN(parseInt(value))) {  
                    console.log($scope.details.protocolAdministrativeData.animalProceduresPersonals);                  
                    angular.forEach($scope.details.protocolAdministrativeData.animalProceduresPersonals, function(personal){                        
                        if(value === personal.id){
                            isExists = true;
                        }
                    });
                    if(!isExists){
                        protocolService.createPersonals($scope.protocolId, $scope.details.protocolAdministrativeData.id,'-1', value );
                    }
                    isExists = false;
                }
            });
    };
      $scope.updateAdminstrativecoInvestigators = function (investigatorValues) {
        var inv = investigatorValues;
            var isExists = false;
            angular.forEach(inv, function (value) {
                console.log(value, 'user');
                if (angular.isNumber(parseInt(value)) && !isNaN(parseInt(value))) {  
                    console.log($scope.details.protocolAdministrativeData.coInvestigators);                  
                    angular.forEach($scope.details.protocolAdministrativeData.coInvestigators, function(investigator){                        
                        if(value === investigator.id){
                            isExists = true;
                        }
                    });
                    if(!isExists){
                        protocolService.createcoInvestigators($scope.protocolId, $scope.details.protocolAdministrativeData.id,'-1', value );
                    }
                    isExists = false;
                }
            });
    };
     $scope.updateAdminstrativeotherPersonals = function (otherPersonalsValues) {
        var other = otherPersonalsValues;
            var isExists = false;
            angular.forEach(other, function (value) {
                console.log(value, 'user');
                if (angular.isNumber(parseInt(value)) && !isNaN(parseInt(value))) {  
                    console.log($scope.details.protocolAdministrativeData.otherPersonals);                  
                    angular.forEach($scope.details.protocolAdministrativeData.otherPersonals, function(otherPersonal){                        
                        if(value === otherPersonal.id){
                            isExists = true;
                        }
                    });
                    if(!isExists){
                        protocolService.createotherPersonals($scope.protocolId, $scope.details.protocolAdministrativeData.id,'-1', value );
                    }
                    isExists = false;
                }
            });
    };

    $scope.updateTransportation = function (fieldName, fieldValue) {
        protocolService.updateProtocol('transportation', $scope.details.transportation.id, fieldName, fieldValue, $scope.protocolId);
    };

    $scope.updateAnimalRequirements = function (id, fieldName, fieldValue) {
        protocolService.updateProtocol('animalRequirements', id, fieldName, fieldValue, $scope.protocolId);
    };

    $scope.updatenumberOfAnimals = function (animalId, id, fieldName, fieldValue) {
        protocolService.updateProtocol('animalRequirements', animalId + '/numberOfAnimals/' + id, fieldName, fieldValue, $scope.protocolId);
    };

    $scope.animalsTotal = function (animNums) {
        return parseInt(animNums.year1) + parseInt(animNums.year2) + parseInt(animNums.year3);
    };

    $scope.updateMajorSurvialSurgery = function (id, fieldName, fieldValue) {
        protocolService.updateProtocol('majorSurvialSurgery', id, fieldName, fieldValue, $scope.protocolId);
    };

    $scope.updatePainOrDistress = function (fieldName, fieldValue) {
        protocolService.updateProtocol('recordpainordistresscategory', $scope.details.recordPainDistressCategory.id, fieldName, fieldValue, $scope.protocolId);
    };

    $scope.updatePainOrDistressColumns = function (columnDetails, fieldName, fieldValue) {
        protocolService.updateProtocol('recordpainordistresscategory', $scope.details.recordPainDistressCategory.id + '/usdacolumn/' + columnDetails.id, fieldName, fieldValue, $scope.protocolId);
    };

    $scope.updateAnesthesiaMethod = function (fieldName, fieldValue) {
        protocolService.updateProtocol('anesthesiamethod', $scope.details.anesthesiaMethod.id, fieldName, fieldValue, $scope.protocolId);
    };

    $scope.updateAnesthesiaMethodComments = function (id, fieldName, fieldValue) {
        var studyId = -1;
        if (typeof id !== 'undefined') {
            studyId = id;
        }
        protocolService.updateProtocol('anesthesiamethod', $scope.details.anesthesiaMethod.id + '/userinput/' + studyId, fieldName, fieldValue, $scope.protocolId)
            .then(function (data) {
                $scope.details.anesthesiaMethod = data;
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
    };

    $scope.updateEuthanasiaMethod = function (fieldName, fieldValue) {
        protocolService.updateProtocol('euthanasiamethod', $scope.details.euthanasiaMethod.id, fieldName, fieldValue, $scope.protocolId);
    };

    $scope.updateEuthanasiaMethodComments = function (id, fieldName, fieldValue) {
        var studyId = -1;
        if (typeof id !== 'undefined') {
            studyId = id;
        }
        protocolService.updateProtocol('euthanasiamethod', $scope.details.euthanasiaMethod.id + '/userinput/' + studyId, fieldName, fieldValue, $scope.protocolId)
            .then(function (data) {
                $scope.details.euthanasiaMethod = data;
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
    };

    $scope.updateSpecialRequirementOfStudy = function (fieldName, fieldValue) {
        protocolService.updateProtocol('specialrequirementofstudy', $scope.details.specialRequirementOfStudy.id, fieldName, fieldValue, $scope.protocolId)
            .then(function (data) {
                $scope.details.specialRequirementOfStudy = data;
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
    };

    $scope.updateSpecialRequirementOfStudyComments = function (id, fieldName, fieldValue) {
        var studyId = -1;
        if (typeof id !== 'undefined') {
            studyId = id;
        }
        protocolService.updateProtocol('specialrequirementofstudy', $scope.details.specialRequirementOfStudy.id + '/userinput/' + studyId, fieldName, fieldValue, $scope.protocolId)
            .then(function (data) {
                $scope.details.specialRequirementOfStudy = data;
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
    };

    $scope.hazardousagents = {
        animalBiosafetyLevel: {
            id: -1,
            value: null
        },
        safetyConsiderations: {
            id: -1,
            value: null
        },
        occupationalHealth: {
            id: -1,
            value: null
        }
    };

    var bindHazardousagents = function (data) {
        angular.forEach(data, function (agent, index) {
            if (agent.label === 'animalBiosafetyLevel') {
                $scope.hazardousagents.animalBiosafetyLevel.id = agent.id;
                $scope.hazardousagents.animalBiosafetyLevel.value = agent.value;
            } else if (agent.label === 'safetyConsiderations') {
                $scope.hazardousagents.safetyConsiderations.id = agent.id;
                $scope.hazardousagents.safetyConsiderations.value = agent.value;
            } else if (agent.label === 'occupationalHealth') {
                $scope.hazardousagents.occupationalHealth.id = agent.id;
                $scope.hazardousagents.occupationalHealth.value = agent.value;
            }
        });
        $scope.details.hazardousagents = $scope.hazardousagents;
    };

    $scope.updateHazardousAgents = function (fieldName, fieldValue) {
        if (fieldName === 'animalBiosafetyLevel') {
            protocolService.updateProtocol('hazardousagents', $scope.details.hazardousAgents.id + '/userinput/' + $scope.hazardousagents.animalBiosafetyLevel.id, 'animalBiosafetyLevel', $scope.hazardousagents.animalBiosafetyLevel.value, $scope.protocolId)
                .then(function (data) {
                    bindHazardousagents(data.userInputs);
                },
                    function (data) {
                        toaster.pop('error', 'Error', data.errorMessage);
                    });
        } else if (fieldName === 'safetyConsiderations') {
            protocolService.updateProtocol('hazardousagents', $scope.details.hazardousAgents.id + '/userinput/' + $scope.hazardousagents.safetyConsiderations.id, 'safetyConsiderations', $scope.hazardousagents.safetyConsiderations.value, $scope.protocolId)
                .then(function (data) {
                    bindHazardousagents(data.userInputs);
                },
                    function (data) {
                        toaster.pop('error', 'Error', data.errorMessage);
                    });
        } else if (fieldName === 'occupationalHealth') {
            protocolService.updateProtocol('hazardousagents', $scope.details.hazardousAgents.id + '/userinput/' + $scope.hazardousagents.occupationalHealth.id, 'occupationalHealth', $scope.hazardousagents.occupationalHealth.value, $scope.protocolId)
                .then(function (data) {
                    bindHazardousagents(data.userInputs);
                },
                    function (data) {
                        toaster.pop('error', 'Error', data.errorMessage);
                    });
        }
        else {
            protocolService.updateProtocol('hazardousagents', $scope.details.hazardousAgents.id, fieldName, fieldValue, $scope.protocolId);
        }
    };

    $scope.addOrUpdateQuestion = function (question, questionType) {
        var userObjectes;
        var questions;
        if (questionType === 'studyObjective') {
            userObjectes = $scope.details.studyObjective;
            questions = $scope.studyObjectsQuestions;
        } else if (questionType === 'rationalUseOfAnimals') {
            userObjectes = $scope.details.rationalUseOfAnimals;
            questions = $scope.rationaleQuestions;
        } else if (questionType === 'descriptionOfExperiment') {
            userObjectes = $scope.details.descriptionOfExperiment;
            questions = $scope.experimentalDesignQuestions;
        } else if (questionType === 'majorSurvialSurgery') {
            userObjectes = $scope.details.majorSurvialSurgery;
            questions = $scope.studyObjectsQusurgeryQuestionsestions;
        }

        protocolService.updateProtocol(questionType, userObjectes.id + '/userInputs/' + question.inputId, question.id, question.text, $scope.protocolId)
            .then(function (data) {
                bindQuestions(data.userInputs, questions);
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
    };

    $scope.addUserComment = function (comment) {
        console.log($rootScope.currentUser);
        protocolService.updateProtocol('comments', '-1', 'comment', $rootScope.currentUser.firstName + ' ' + $rootScope.currentUser.lastName + '#username#' + comment, $scope.protocolId);
        $scope.details.comments.push({ 'comment': $rootScope.currentUser.firstName + ' ' + $rootScope.currentUser.lastName + '#username#' + comment, addedDate: new Date() });
        $("#textarea1").val("");
    };

    $scope.radioYesNo = [
        { value: true, text: 'Yes' },
        { value: false, text: 'No' }
    ];
    $scope.showStatus = function (status) {
        var selected = $filter('filter')($scope.radioYesNo, { value: status });
        return (status && selected.length) ? selected[0].text : 'No';
    };
    $scope.piUser = {
        id: 0,
        userName: '',
        firstName: '', // original value
        lastName: ''
    };

    $scope.questions = protocolService.questions;
    $scope.piUsers = [];
    $scope.loadPiUsers = function () {
        protocolService.getUsersByRoleId(1)
            .then(function (data) {
                $scope.pUsers = data;
                protocolService.getUsersByRoleId(8)
            .then(function (data) {
                $scope.rUsers = data;
                $scope.rsUsers   =$scope.rUsers.concat($scope.pUsers);
                 protocolService.getUsersByRoleId(10)
            .then(function (data) {
                $scope.lUsers = data;
             $scope.piUsers = $scope.rsUsers.concat($scope.lUsers);
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
             
    };

    $scope.committeeMembers = [];
    $scope.loadCommitteMembers = function () {
        protocolService.getUsersByRoleId(7)
            .then(function (data) {
                $scope.committeeMembers = data;
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
    };

    $scope.loadCommitteMembers();

    $scope.getProtocolAlerts = function () {
        protocolService.getProtocolsByPIUser($rootScope.currentUser.id)
            .then(function (protocolList) {
                $rootScope.protocols = protocolList;
                $cookieStore.put('protocols', $scope.protocols);
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
    };

    //$scope.$watch('piUserId', function (newVal, oldVal) {
    //    if (newVal !== oldVal) {
    //        var selected = $filter('filter')($scope.piUsers, { id: piUserId });
    //        $scope.details.piUser = selected.length ? selected[0]: null;
    //    }
    //});

    protocolService.details($scope.protocolId)
        .then(function (data) {
            bindProtocolDetails(data);
        },
            function (data) {
                if (data.errorMessage.includes('Protocol Not found for the given protocol id')) {
                    $state.go('error404', {});
                } else {
                    toaster.pop('error', 'Error', data.errorMessage);
                }
            });

    function bindProtocolDetails(data) {
        $scope.getProtocolAlerts();
        $scope.details = data;
        $scope.pSelected = ($scope.details.protocolAdministrativeData.renewalProposalNumber != null);
        $scope.isModification=  $scope.details.protocolAdministrativeData.modification;
        $scope.isInitial =$scope.details.protocolAdministrativeData.initialSubmission;
          $scope.animalProceduresPersonalsUsers = angular.copy(data.protocolAdministrativeData.animalProceduresPersonals);
                angular.forEach(data.protocolAdministrativeData.animalProceduresPersonals, function (value, key) {
                    if (angular.isNumber(parseInt(value.id)) && !isNaN(parseInt(value.id))) {
                        $scope.details.protocolAdministrativeData.animalProceduresPersonals.push(value.id);
                    }
                });
                 $scope.coInvestigatorsUsers = angular.copy(data.protocolAdministrativeData.coInvestigators);
                angular.forEach(data.protocolAdministrativeData.coInvestigators, function (value, key) {
                    if (angular.isNumber(parseInt(value.id)) && !isNaN(parseInt(value.id))) {
                        $scope.details.protocolAdministrativeData.coInvestigators.push(value.id);
                    }
                });
                 $scope.otherPersonalsUsers = angular.copy(data.protocolAdministrativeData.otherPersonals);
                angular.forEach(data.protocolAdministrativeData.otherPersonals, function (value, key) {
                    if (angular.isNumber(parseInt(value.id)) && !isNaN(parseInt(value.id))) {
                        $scope.details.protocolAdministrativeData.otherPersonals.push(value.id);
                    }
                });
        $scope.isEditable = ($scope.isChairman || (($scope.isPI || $scope.isLab || $scope.isRes) && ($scope.details.piUser && $rootScope.currentUser.id === $scope.details.piUser.id)));

        //Bind STUDY OBJECTIVES
        if ($scope.details.studyObjective) {
            bindQuestions($scope.details.studyObjective.userInputs, $scope.studyObjectsQuestions);
        }

        //Bind RATIONALE FOR USE OF ANIMALS
        if ($scope.details.rationalUseOfAnimals) {
            bindQuestions($scope.details.rationalUseOfAnimals.userInputs, $scope.rationaleQuestions);
        }

        //Bind DESCRIPTION OF EXPERIMENTAL DESIGN AND ANIMAL PROCEDURES
        if ($scope.details.descriptionOfExperiment) {
            bindQuestions($scope.details.descriptionOfExperiment.userInputs, $scope.experimentalDesignQuestions);
            $scope.details.experimentalDesignQuestions = $scope.experimentalDesignQuestions;
        }

        //Bind MAJOR SURVIVAL SURGERY
        if ($scope.details.majorSurvialSurgery) {
            bindQuestions($scope.details.majorSurvialSurgery.userInputs, $scope.surgeryQuestions);

            $scope.details.surgeryQuestions = $scope.surgeryQuestions;
        }

        //Bind RECORDING PAIN OR DISTRESS CATEGORY
        if ($scope.details.recordPainDistressCategory !== null) {
            bindQuestions($scope.details.recordPainDistressCategory.userInputs, $scope.questions.painAndDistress);
        }
        if ($scope.details.recordPainDistressCategory) {
            var rdc = $scope.details.recordPainDistressCategory;
            $scope.painOrDistressTotal = {
                year1: parseInt(rdc.usdacolumnB.year1) + parseInt(rdc.usdacolumnC.year1) + parseInt(rdc.usdacolumnD.year1) + parseInt(rdc.usdacolumnE.year1),
                year2: parseInt(rdc.usdacolumnB.year2) + parseInt(rdc.usdacolumnC.year2) + parseInt(rdc.usdacolumnD.year2) + parseInt(rdc.usdacolumnE.year2),
                year3: parseInt(rdc.usdacolumnB.year3) + parseInt(rdc.usdacolumnC.year3) + parseInt(rdc.usdacolumnD.year3) + parseInt(rdc.usdacolumnE.year3),
            };
        }
        if ($scope.details.hazardousAgents !== null) {
            bindHazardousagents($scope.details.hazardousAgents.userInputs);
        }
    }

    function bindQuestions(inputs, questions) {
        angular.forEach(inputs, function (input, index) {
            var question = $filter('filter')(questions, { id: input.label });
            if (typeof question !== 'undefined' && question.length > 0) {
                question[0].inputId = input.id;
                question[0].text = input.value;
            }
        });
    }



    $scope.updateBioAnimalMaterial = function (fieldName, fieldValue) {
        protocolService.updateProtocol('bioanimalmaterial', $scope.details.bioAnimalMaterial.id, fieldName, fieldValue, $scope.protocolId)
            .then(function (data) {
                $scope.details.bioAnimalMaterial = data;
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
    };

    $scope.addBioAnimalMaterialsUsed = function () {
        protocolService.updateProtocol('bioanimalmaterial', $scope.details.bioAnimalMaterial.id + '/biomaterialsused/-1', 'sterileAttenuated', false, $scope.protocolId)
            .then(function (data) {
                $scope.details.bioAnimalMaterial = data;
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
    };

    $scope.updateBioAnimalMaterialsUsed = function (fieldName, fieldValue, id) {
        protocolService.updateProtocol('bioanimalmaterial', $scope.details.bioAnimalMaterial.id + '/biomaterialsused/' + id, fieldName, fieldValue, $scope.protocolId)
            .then(function (data) {
                $scope.details.bioAnimalMaterial = data;
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
    };
    $scope.exportPdf = function () {
        if ($scope.details) {
            pdfService.printPdf($scope.details, 'ProtocolDetails.pdf');
        }
    };
    
    $scope.updateReviewType = function (fieldName, fieldValue) {
        protocolService.updateProtocol('review', $scope.details.review ? $scope.details.review.id : '-1', fieldName, fieldValue, $scope.protocolId)
            .then(function (data) {
                //$scope.details = data;
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
    };

    $scope.updatePi = function (pid) {
        protocolService.updatePi($scope.protocolId, pid)
            .then(function (data) {
                $scope.details = data;
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
    };
    $scope.updateReviewUser = function (fieldName, fieldValue) {
        protocolService.updateProtocol('review', ($scope.details.review.id ? $scope.details.review.id : '-1') + '/user', fieldName, fieldValue, $scope.protocolId)
            .then(function (data) {
                //$scope.details = data;
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
    };

    $scope.getCommentsusername = function (comment) {
        var comm = comment.split('#username#');
        if (comm.length > 1) {
            return comm[0];
        }
        return '';
    };

    $scope.getCommentsText = function (comment) {
        var comm = comment.split('#username#');
        if (comm.length > 1) {
            return comm[1];
        }
        return comment;
    };
    
});