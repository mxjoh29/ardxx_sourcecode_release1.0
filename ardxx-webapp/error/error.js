angular.module('error', ['ui.bootstrap','ui.utils','ui.router','ngAnimate']);

angular.module('error').config(function($stateProvider) {

    $stateProvider.state('error500', {
        url: '/internalerror',
        templateUrl: 'error/partial/error500/error500.html'
    });
    $stateProvider.state('error404', {
        url: '/notfound',
        templateUrl: 'error/partial/error404/error404.html'
    });
    $stateProvider.state('errorservice', {
        url: '/service-error',
        templateUrl: 'error/partial/errorService/errorService.html'
    });
    /* Add New States Above */

});

