angular.module('animals').controller('AnimaldetailsCtrl', function ($sce, $scope, $stateParams, animalsService, toaster, testResultsService, protocolService) {

    $scope.detailsEditable = false;

    $scope.isFormSubmitted = false;
    $scope.elisaDetails = {};

    //$scope.speciesList = [{ name: 'CHINESE RHESUS' }, { name: 'CYNOMOLGUS' }, { name: 'FERRET' }, { name: 'GUINEA PIG' }, { name: 'HAMSTER' }, { name: 'INDIAN RHESUS' }, { name: 'MOUSE' }, { name: 'PIGTAIL' }, { name: 'RABBIT' }, { name: 'RATS' }];
    $scope.animal = { status: 'HOLDING' };

    $scope.countries = [
		'Africa',
		'Antarctica',
		'Asia',
		'Australasia',
		'Europe',
		'North America',
		'Afghanistan',
		'Albania',
		'Algeria',
		'Andorra',
		'Angola',
		'Antigua and Barbuda',
		'Argentina',
		'Armenia',
		'Australia',
		'Austria',
		'Azerbaijan',
		'Bahamas',
		'Bahrain',
		'Bangladesh',
		'Barbados',
		'Belarus',
		'Belgium',
		'Belize',
		'Benin',
		'Bhutan',
		'Bolivia',
		'Bosnia and Herzegovina',
		'Botswana',
		'Brazil',
		'Brunei',
		'Bulgaria',
		'Burkina Faso',
		'Burundi',
		'Cambodia',
		'Cameroon',
		'Canada',
		'Cape Verde',
		'Central African Republic',
		'Chad',
		'Chile',
		'China',
		'Colombi',
		'Comoros',
		'Congo (Brazzaville)',
		'Congo',
		'Costa Rica',
		'Cote d\'Ivoire',
		'Croatia',
		'Cuba',
		'Cyprus',
		'Czech Republic',
		'Denmark',
		'Djibouti',
		'Dominica',
		'Dominican Republic',
		'East Timor (Timor Timur)',
		'Ecuador',
		'Egypt',
		'El Salvador',
		'Equatorial Guinea',
		'Eritrea',
		'Estonia',
		'Ethiopia',
		'Fiji',
		'Finland',
		'France',
		'Gabon',
		'Gambia, The',
		'Georgia',
		'Germany',
		'Ghana',
		'Greece',
		'Grenada',
		'Guatemala',
		'Guinea',
		'Guinea-Bissau',
		'Guyana',
		'Haiti',
		'Honduras',
		'Hungary',
		'Iceland',
		'India',
		'Indonesia',
		'Iran',
		'Iraq',
		'Ireland',
		'Israel',
		'Italy',
		'Jamaica',
		'Japan',
		'Jordan',
		'Kazakhstan',
		'Kenya',
		'Kiribati',
		'Korea, North',
		'Korea, South',
		'Kuwait',
		'Kyrgyzstan',
		'Laos',
		'Latvia',
		'Lebanon',
		'Lesotho',
		'Liberia',
		'Libya',
		'Liechtenstein',
		'Lithuania',
		'Luxembourg',
		'Macedonia',
		'Madagascar',
		'Malawi',
		'Malaysia',
		'Maldives',
		'Mali',
		'Malta',
		'Marshall Islands',
		'Mauritania',
		'Mauritius',
		'Mexico',
		'Micronesia',
		'Moldova',
		'Monaco',
		'Mongolia',
		'Morocco',
		'Mozambique',
		'Myanmar',
		'Namibia',
		'Nauru',
		'Nepal',
		'Netherlands',
		'New Zealand',
		'Nicaragua',
		'Niger',
		'Nigeria',
		'Norway',
		'Oman',
		'Pakistan',
		'Palau',
		'Panama',
		'Papua New Guinea',
		'Paraguay',
		'Peru',
		'Philippines',
		'Poland',
		'Portugal',
		'Qatar',
		'Romania',
		'Russia',
		'Rwanda',
		'Saint Kitts and Nevis',
		'Saint Lucia',
		'Saint Vincent',
		'Samoa',
		'San Marino',
		'Sao Tome and Principe',
		'Saudi Arabia',
		'Senegal',
		'Serbia and Montenegro',
		'Seychelles',
		'Sierra Leone',
		'Singapore',
		'Slovakia',
		'Slovenia',
		'Solomon Islands',
		'Somalia',
		'South Africa',
		'Spain',
		'Sri Lanka',
		'Sudan',
		'Suriname',
		'Swaziland',
		'Sweden',
		'Switzerland',
		'Syria',
		'Taiwan',
		'Tajikistan',
		'Tanzania',
		'Thailand',
		'Togo',
		'Tonga',
		'Trinidad and Tobago',
		'Tunisia',
		'Turkey',
		'Turkmenistan',
		'Tuvalu',
		'Uganda',
		'Ukraine',
		'United Arab Emirates',
		'United Kingdom',
		'United States',
		'Uruguay',
		'Uzbekistan',
		'Vanuatu',
		'Vatican City',
		'Venezuela',
		'Vietnam',
		'Yemen',
		'Zambia',
		'Zimbabwe'
    ];

    $scope.animalHistory = [];

    $scope.getAnimalHistory = function (animalId) {
        animalsService.getAnimalHistory(animalId)
			.then(function (data) {
			    $scope.animalHistory = data;
			},
				function (data) {
				    toaster.pop('error', 'Error', data.errorMessage);
				});
    };

    animalsService.getSpeciesList()
		.then(function (data) {
		    $scope.allSpecies = data;
		},
			function (data) {
			    toaster.pop('error', 'Error', data.errorMessage);
			});

    if ($stateParams.id && $stateParams.id !== '0') {
        $scope.animal.id = $stateParams.id;
        animalsService.getAnimalDetails($scope.animal.id)
			.then(function (data) {
			    $scope.animal = data;
                if(data.vetTech){
                    $scope.vetTechnician =data.vetTech.id;
                }
			},
				function (data) {
				    toaster.pop('error', 'Error', data.errorMessage);
				});
        $scope.getAnimalHistory($scope.animal.id);
    } else {
        $scope.detailsEditable = true;
    }
    window.scope = $scope;
    $scope.virusList = [];
    $scope.getVirusList = function () {
        animalsService.getVirusList()
			.then(function (data) {
			    $scope.virusList = data;
			},
				function (data) {
				    toaster.pop('error', 'Error', data.errorMessage);
				});
    };
    $scope.getVirusList();
    var weightHistory = [
		{ date: '01/02/2015', weight: 7 },
		{ date: '01/05/2015', weight: 10 },
		{ date: '01/09/2015', weight: 9 }
    ];
    var tempContent = '';
    angular.forEach(weightHistory, function (value) {
        tempContent = tempContent + '<tr><td>' + value.date + '</td><td>' + value.weight + '</td></tr>';
    });
    $scope.wightHistoryContent = '<table class="table table-condensed"><tr><th>Date</th><th>Weight</th></tr>' + tempContent + '</table>';

    $scope.htmlTooltip = $sce.trustAsHtml('I\'ve been made <b>bold</b>!');

    var defaultElisaResults = {
        'collectedDate': new Date(),
        'result': '',
        'value': 0,
        'virus': 0,
        'animals': $scope.animal.id
    };

    var defaultMamuResults = {
        'collectedDate': new Date(),
        'result': '',
        'value': 0,
        'mamu': 0,
        'animals': $scope.animal.id
    };

    $scope.elisaDetails = angular.copy(defaultElisaResults);

    $scope.mamuDetails = angular.copy(defaultMamuResults);

    //$scope.getVirusList();

    $scope.saveAnimalDetails = function () {
        $scope.isFormSubmitted = true;
        if ($scope.animalForm.$valid) {

            $scope.isFormSubmitted = true;

            if($scope.vetTechnician){
                $scope.animal.vetTech = {'id':parseInt($scope.vetTechnician)};
            }
            if (!$scope.animal.id || ($scope.animal.id && $scope.animal.id === 0)) {
                animalsService.createAnimal($scope.animal)
					.then(function (data) {
					    $scope.animal = data;
                        if(data.vetTech){
                            $scope.vetTechnician =data.vetTech.id;
                        }
					    $scope.isFormSubmitted = false;
					    toaster.pop('success', 'Success', 'Animal created successfully.');
					    $scope.detailsEditable = false;

					    $scope.elisaDetails.animals = data.id;

					    $scope.mamuDetails.animals = data.id;

					    $scope.getAnimalHistory($scope.animal.id);
					},
						function (data) {
						    toaster.pop('error', 'Error', data.errorMessage);
						    $scope.isFormSubmitted = false;
						});
            } else if ($scope.animal.id && $scope.animal.id !== 0) {
                animalsService.updateAnimal($scope.animal)
					.then(function (data) {
					    $scope.animal = data;
                        if(data.vetTech){
                            $scope.vetTechnician =data.vetTech.id;
                        }
					    $scope.isFormSubmitted = false;
					    toaster.pop('success', 'Success', 'Animal details updated successfully.');
					    $scope.detailsEditable = false;
					    $scope.getAnimalHistory($scope.animal.id);
					},
						function (data) {
						    toaster.pop('error', 'Error', data.errorMessage);
						    $scope.isFormSubmitted = false;
						});
            }
        } else {
            toaster.pop('error', 'Error', 'Please correct details.');
        }
    };

    $scope.getTestResults = function () {
        testResultsService.getTestResultsforAnimal($scope.animal.id)
            .then(function (data) {
                if (data) {
                    $scope.allTestResults = data;
                }
            },
				function (data) {
				    toaster.pop('error', 'Error', data.errorMessage);
				});
    };
    $scope.elisaList = [];
    $scope.getElisaList = function () {
        animalsService.getElisaList($scope.animal.animalId)
            .then(function (data) {
                $scope.elisaList = data;
            },
				function (data) {
				    toaster.pop('error', 'Error', data.errorMessage);
				});
    };

    $scope.mamuList = [];
    $scope.getMamuList = function () {
        animalsService.getMamuResults($scope.animal.animalId)
            .then(function (data) {
                $scope.mamuList = data;
            },
				function (data) {
				    toaster.pop('error', 'Error', data.errorMessage);
				});
    };
    $scope.mamuTypes = [];
    $scope.getMamuTypes = function () {
        animalsService.getMamuTypes()
            .then(function (data) {
                $scope.mamuTypes = data;
            },
				function (data) {
				    toaster.pop('error', 'Error', data.errorMessage);
				});
    };
    $scope.getMamuTypes();
    $scope.addElisaDetails = function () {
        animalsService.createElisaDetails($scope.elisaDetails)
            .then(function (data) {
                if (data) {
                    $scope.elisaDetails = angular.copy(defaultElisaResults);
                    $scope.getElisaList();
                    toaster.pop('success', 'Success', "Successfully created ELISA details.");
                }
            },
				function (data) {
				    toaster.pop('error', 'Error', data.errorMessage);
				});
    };

    $scope.addMamuDetails = function () {
        animalsService.createMamuDetails($scope.mamuDetails)
            .then(function (data) {
                if (data) {
                    $scope.mamuDetails = angular.copy(defaultMamuResults);
                    $scope.getMamuList();
                    toaster.pop('success', 'Success', "Successfully created MAMU details.");
                }
            },
				function (data) {
				    toaster.pop('error', 'Error', data.errorMessage);
				});
    };

    protocolService.getUsersByRoleId(9)
        .then(function (data) {
            $scope.vetUsers = data;
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
});
