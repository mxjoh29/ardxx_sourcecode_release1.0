angular.module('animals').controller('AnimalListCtrl', function ($scope, $rootScope, $filter, $q, $document, DTColumnBuilder, $timeout, CSV, animalsService, toaster, DTOptionsBuilder, DTColumnDefBuilder, testResultsService) {

    $scope.isImportLoading = false;
    var languageOptionsAnimals = {
        "lengthMenu": '_MENU_ entries per page',
        "search": '<i class="fa fa-search"></i>',
        "paginate": {
            "previous": '<i class="fa fa-angle-left"></i>',
            "next": '<i class="fa fa-angle-right"></i>'
        }
    };

    $scope.dtOptionsAnimals = DTOptionsBuilder.newOptions()
        .withDOM('lCfrtip')
        .withOption('language', languageOptionsAnimals)
        .withColVis();

    $scope.dtOptionsAnimalsReport = DTOptionsBuilder.newOptions()
        .withDOM('lCfrtip')
        .withOption('language', languageOptionsAnimals)
        .withColVis()
        .withTableTools('assets/swf/copy_csv_xls_pdf.swf')
        .withTableToolsButtons([
        {
            'sExtends': 'collection',
            'sButtonText': 'Export',
            'aButtons': ['xls', 'pdf']
        }
    ])
        .withColumnFilter({ sPlaceHolder: 'head:after'});

    $scope.columnsAnimalsReport = [
        DTColumnBuilder.newColumn('animalId').withTitle('Animal Id').withClass('text-danger'),
        DTColumnBuilder.newColumn('oldId').withTitle('Old Id'),
        DTColumnBuilder.newColumn('species').withTitle('Species'),
        DTColumnBuilder.newColumn('sex').withTitle('Sex'),
        DTColumnBuilder.newColumn('client').withTitle('Client'),
        DTColumnBuilder.newColumn('status').withTitle('Status')
    ];

    $scope.dtOptionsCbcFullResults = DTOptionsBuilder.newOptions()
        .withDOM('lCfrtip')
        .withOption('language', languageOptionsAnimals)
        .withColVis()
        .withTableTools('assets/swf/copy_csv_xls_pdf.swf')
        .withTableToolsButtons([
        {
            'sExtends': 'collection',
            'sButtonText': 'Export',
            'aButtons': ['xls', 'pdf']
        }
    ])
        .withOption('scrollX', '100%')
        .withColumnFilter({ sPlaceHolder: 'head:after'});

    $scope.dtOptionsImport = DTOptionsBuilder.newOptions()
        .withDOM('lCfrtip')
        .withOption('language', languageOptionsAnimals)
        .withOption('order', [0, 'desc'])
        .withColVis();

    $scope.dtColumnDefsImport = [
        //DTColumnDefBuilder.newColumnDef(4).notSortable()
    ];

    $scope.dtInstance = {};

    $scope.fileTypes = [
        { value: 'Animal', text: 'Animals' },
        //{ value: 'ELISA', text: 'Virus ELISA test results' },
        //{ value: 'MAMU', text: 'Mamu information' },
        { value: 'CBC', text: 'CBC-Chem Results' }
    ];

    $scope.selectedFileType = 'Animal';
    $scope.animalsList = [];
    $scope.getAnimalsList = function () {
        animalsService.getAnimalListing()
            .then(function (data) {
            if (data) {
                $scope.animalsList = data;
            }
        },
            function (data) {
                toaster.pop('error', 'Error', data.errorMessage);
            });
    };

    $scope.getImportList = function () {
        animalsService.getImportHistory()
            .then(function (data) {
            if (data) {
                $scope.importHistoryList = data;
            }
        },
            function (data) {
                toaster.pop('error', 'Error', data.errorMessage);
            });
    };

    $scope.initData = function () {
        $scope.selectedTab = 'animals';
        $scope.getAnimalsList();
        $scope.getImportList();
    };

    $scope.$watch('fileContent', function (newValue, oldValue) {
        if (typeof newValue !== 'undefined') {
            $scope.csvArray = newValue.split(/\r\n|\n/);
        }
    });

    $scope.openFile = function () {
        angular.element('#readCsvFile').click();
    };

    $scope.removeFile = function () {
        var fileinput = angular.element('#readCsvFile');
        fileinput.replaceWith(fileinput.val('').clone(true));
        $scope.fileName = '';
        $scope.fileContent = '';
    };

    $scope.uploadFileDetails = function () {
        if ($scope.fileName === '') {
            toaster.pop('error', 'Error', 'Please select a file.');
        } else if ($scope.fileContent === '') {
            toaster.pop('error', 'Error', 'This file has not content in it.');
        } else {
            $scope.isImportLoading = true;
            var details = {
                "animalsList": $scope.csvArray,
                "format": $scope.selectedFileType,
                "uploadedBy": $rootScope.currentUser.email
            };
            animalsService.uploadDetails(details)
                .then(function (data) {
                if (data) {
                    //$scope.animalsList = data;
                    $scope.getImportList();
                    $scope.getAnimalsList();
                    $scope.removeFile();
                    toaster.pop('success', 'Success', 'Sucessfully imported data.');
                }
                $scope.isImportLoading = false;
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                    $scope.isImportLoading = false;
                });
        }
    };

    $scope.getImportResponse = function (id) {
        animalsService.getImportHistoryById(id)
            .then(function (data) {
            if (data) {
                $scope.importResponse = data;
                $scope.options = {};

                var csvResult = $scope.importResponse.join('\r\n');
                exportCsv(csvResult, 'Response.csv');
            }
        },
            function (data) {
                toaster.pop('error', 'Error', data.errorMessage);
            });
    };

    function exportCsv(data, filename) {
        var charset = $scope.charset || "utf-8";
        var blob = new Blob([data], {
            type: "text/csv;charset=" + charset + ";"
        });

        if (window.navigator.msSaveOrOpenBlob) {
            navigator.msSaveBlob(blob, filename);
        } else {

            var downloadLink = angular.element('<a></a>');
            downloadLink.attr('href', window.URL.createObjectURL(blob));
            downloadLink.attr('download', filename);
            downloadLink.attr('target', '_blank');

            $document.find('body').append(downloadLink);
            $timeout(function () {
                downloadLink[0].click();
                downloadLink.remove();
            }, null);
        }
    }

    $scope.exportAnimals = function () {
        var filteredAnimals = $filter('filter')($scope.animalsList, $scope.animalSearch);
        var animalsList = [];
        var stringValue = '';
        angular.forEach(filteredAnimals, function (value, key) {
            stringValue = '';
            stringValue = value.animalId + ',' + value.oldId + ',' + value.species + ',' + value.sex + ',' + value.client + ',' + value.status;
            this.push(stringValue);

        }, animalsList);
        exportCsv(animalsList.join('\r\n'), 'animals.csv');
    };

    $scope.getTestResults = function () {
        testResultsService.getAllTestResults()
            .then(function (data) {
            if (data) {
                $scope.allTestResults = data;
            }
        },
            function (data) {
                toaster.pop('error', 'Error', data.errorMessage);
            });
    };

    $scope.chart = null;

    $scope.chartType = 'spline';

    $scope.timeLines = ['2015-01-01', '2015-01-08', '2015-01-15', '2015-01-22', '2015-01-29', '2015-02-06', '2015-02-13', '2015-02-20'];
    $scope.rbcCount = [6.5, 7.5, 9, 8.3, 9.6, 5.2, 6.4, 8.2];
    $scope.wbcCount = [10, 9.8, 12.2, 15.6, 17.7, 12.4, 14.4, 16];
    $scope.showGraph = function () {
        $scope.chart = window.c3.generate({
            data: {
                x: 'x',
                json: {
                    'x': ['2015-01-01', '2015-01-08', '2015-01-15', '2015-01-22', '2015-01-29', '2015-02-06', '2015-02-13', '2015-02-20'],
                    'Animal1': [6.5, 7.5, 9, 8.3, 9.6, 5.2, 6.4, 8.2],
                    'Animal2': [10, 9.8, 12.2, 15.6, 17.7, 12.4, 14.4, 16]
                },
                type: $scope.chartType
            },
            axis: {
                x: {
                    type: 'timeseries',
                    tick: {
                        format: '%Y-%m-%d'
                    }
                }
            },
            color: {
                pattern: ['#1f77b4', '#ff7f0e']
            },
            grid: {
                y: {
                    lines: [
                        { value: 8, text: 'Minimum count (8)' },
                        { value: 15, text: 'Maximum count (15)' }
                    ]
                }
            }
        });

        $scope.$watch('chartType', function () {
            $scope.chart.transform($scope.chartType);
        });
    };



});
