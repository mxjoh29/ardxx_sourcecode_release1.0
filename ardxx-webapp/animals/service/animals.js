angular.module('animals').factory('animalsService', function ($http, configService, $q) {

    var animals = {};

    animals.getAnimalListing = function () {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'animals')
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    animals.getAnimalDetails = function (animalId) {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'animals/' + animalId)
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    animals.getAnimalsByStatus = function (status) {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'animals/status/' + status)
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    animals.getImportHistory = function () {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'animals/importFileHistoryList')
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    animals.getImportHistoryById = function (id) {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'animals/importFileHistoryList/' + id)
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    animals.uploadDetails = function (details) {
        var deferred = $q.defer();
        $http.post(configService.getHostName + 'animals/uploadanimals', details)
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function () {
                deferred.reject('Failed to upload details.');
            });
        return deferred.promise;
    };

    animals.createAnimal = function (animal) {
        var deferred = $q.defer();
        $http.post(configService.getHostName + 'animals', animal)
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    animals.updateAnimal = function (animal) {
        var deferred = $q.defer();
        $http.put(configService.getHostName + 'animals', animal)
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    animals.createElisaDetails = function (elisaDetails) {
        var deferred = $q.defer();
        $http.post(configService.getHostName + 'animals/elisa', elisaDetails)
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    animals.createMamuDetails = function (mamuDetails) {
        var deferred = $q.defer();
        $http.post(configService.getHostName + 'animals/mamu', mamuDetails)
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    animals.getElisaList = function (animalId) {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'animals/elisalist/' + animalId)
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    animals.getMamuResults = function (animalId) {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'animals/mamulist/' + animalId)
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    animals.getMamuTypes = function () {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'animals/mamulist')
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    animals.getVirusList = function () {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'animals/viruslist')
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    animals.getSpeciesList = function () {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'animals/species')
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    animals.getAnimalHistory = function (animalId) {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'animals/' + animalId + '/history')
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    return animals;
});
