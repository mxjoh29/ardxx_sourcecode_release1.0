angular.module('dashboard').controller('DashboardCtrl', function ($scope, $rootScope, toaster, protocolService, $cookieStore, uiCalendarConfig, $compile, studyService) {
    var userRoles = $rootScope.currentUser.role;
    $scope.isTechnician = (userRoles.indexOf('VET_TEC') > -1 || userRoles.indexOf('LAB_TEC') > -1) || userRoles.indexOf('PJT_MGR') > -1;
    $scope.filterProcedureSchedules = [];

    $scope.calendarView = true;

    $scope.showProcedureList = function () {
        $scope.calendarView = false;
    };
    $scope.showCalendarView = function () {
        $scope.calendarView = true;
    };
    $scope.procedureSource = [];

    var getSchedulesByUser = function () {
        var userType = 'vettec';
        if (userRoles.indexOf('LAB_TEC') > -1) {
            userType = 'labtec';
        } else if (userRoles.indexOf('PJT_MGR') > -1) {
            userType = 'pm';
        }

        studyService.getSchedulesByUser(userType, $rootScope.currentUser.id)
           .then(function (data) {
               $scope.procedureSchedules = data;
               mapProcedureSchedules();
               $scope.procedureSource = [$scope.procedureSourceList];
           },
               function (data) {
                   toaster.pop('error', 'Error', data.errorMessage);
               });
    };

    var mapProcedureSchedules = function () {
        $scope.procedureSourceList = [];

        angular.forEach($scope.procedureSchedules, function (data, index) {
            var eventData = {
                id: data.scheduleId,
                title: data.studyName + ' - ' + data.procedureId + ' - ' + data.status+ ' - ' + data.tubeType+ ' - ' + data.quantity+ data.unit,
                start: moment.utc(data.scheduleDate).format("YYYY-MM-DDTHH:mm:ss"),
                status: data.status
            };
            if (data.status === 'ACTIVE' || data.status === 'IN PROGRESS') {
                $scope.procedureSourceList.push(eventData);
                uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('renderEvent', eventData, true);
            }
        });
    };


    if ($scope.isTechnician) {

        $scope.uiConfig = {
            calendar: {
                height: '100%',
                editable: false,
                header: false,
                eventRender: $scope.eventRender,
                timezone:false
            }
        };

        getSchedulesByUser();

        //$scope.procedureSource = [$scope.procedureSourceList];


        $scope.eventRender = function (event, element, view) {
            element.attr({
                'tooltip': event.title,
                'tooltip-append-to-body': true
            });
            $compile(element)($scope);
        };


        $scope.dayChanged = function (event, element) {
            console.log(element);
        };

        //protocolService.getProtocolsByPIUser($rootScope.currentUser.id)
        //    .then(function (protocolList) {
        //        $rootScope.protocols = protocolList;
        //        $cookieStore.put('protocols', protocolList);
        //    },
        //        function (data) {
        //            toaster.pop('error', 'Error', data.errorMessage);
        //        });

        //protocolService.list()
        //    .then(function (protocolList) {
        //        $scope.allProtocols = protocolList;
        //    },
        //        function (data) {
        //            toaster.pop('error', 'Error', data.errorMessage);
        //        });

        $scope.changeView = function (view, calendar) {
            uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
            $scope.displayDate();
        };

        $scope.nextDay = function (calendar) {
            uiCalendarConfig.calendars[calendar].fullCalendar('next');
            $scope.displayDate();
        };

        $scope.prevDay = function (calendar) {
            uiCalendarConfig.calendars[calendar].fullCalendar('prev');
            $scope.displayDate();
        };

        $scope.displayDate = function () {
            var selectedDate, view, format;
            if (uiCalendarConfig.calendars['procedureCalendar']) {
                selectedDate = uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('getDate');
                view = uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('getView');
            } else {
                selectedDate = new Date();
            }

            if (view) {
                $scope.selectedDay = view.title;
            } else {
                $scope.selectedDay = moment(selectedDate).format("MMMM YYYY");
            }
        };
        $scope.activeSchedules = function (schedule) {
            return (schedule.status === 'ACTIVE' || schedule.status === 'IN PROGRESS');
        };


        $scope.displayDate();
    }

    $scope.convertUtcDate = function(date,format){
        return moment.utc(date).format(format);
    };

});
