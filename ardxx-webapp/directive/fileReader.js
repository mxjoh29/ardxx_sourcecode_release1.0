angular.module('ardxx').directive('fileReader', function() {
	return {
	    restrict: 'A',
	    scope: {
	        fileReader: "=?bind",
	        fileName: "=?filename"
	    },
		link: function(scope, element, attrs, fn) {
		    angular.element(element).on('change', function (changeEvent) {
		        var files = changeEvent.target.files;
		        if (files.length) {
		            var r = new FileReader();
		            r.onload = function (e) {
		                var contents = e.target.result;
		                scope.$apply(function () {
		                    scope.fileReader = contents;
		                    scope.fileName = files[0].name;
		                });
		            };
		            r.readAsText(files[0]);
		        } else {
		            scope.fileReader = '';
		            scope.fileName = '';
		        }
		    });
		}
	};
});