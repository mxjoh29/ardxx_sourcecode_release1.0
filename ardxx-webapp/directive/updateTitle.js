angular.module('ardxx')
    .directive('updateTitle', function ($rootScope, $timeout) {
        return {
            link: function (scope, element) {

                var listener = function (event, toState) {

                    var title = 'Default Title';
                    if (toState.data && toState.data.pageTitle) {
                        title = toState.data.pageTitle;
                    }
                    $timeout(function () {
                        element.text(title + ' : ARDXX');
                    }, 0, false);
                };

                $rootScope.$on('$stateChangeSuccess', listener);
            }
        };
    })
    .directive('updateBodyclass', function ($rootScope, $timeout) {
        return {
            link: function (scope, element) {

                var listener = function (event, toState) {

                    var title = 'Default Title';
                    if (toState.data && toState.data.pageTitle && toState.data.pageTitle === 'Login') {
                        $timeout(function () {
                            element.addClass('login-bg');
                        }, 0, false);
                    } else {
                        element.removeClass('login-bg');
                    }
                };

                $rootScope.$on('$stateChangeSuccess', listener);
            }
        };
    })
    .directive('uiTabs', function ($rootScope, $timeout) {
        return {
            link: function (scope, element) {
                element.find('a').click(function (e) {
                    e.preventDefault();
                    angular.element(this).tab('show');
                });
            }
        };
    })
    .directive('appMenu', function ($rootScope, $timeout) {
        return {
            link: function (scope, element) {
                window.matsadmin.AppNavigation.initialize();
            }
        };
    })
    .filter('zpad', function () {
        return function (input, n) {
            if (input === undefined) {
                input = "";
            }
            if (input.length >= n) {
                return input;
            }
            var zeros = "0".repeat(n);
            return (zeros + input).slice(-1 * n);
        };
    });
