if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] !== 'undefined' ? args[number] : match;
        });
    };
}

angular.module('ardxx', ['ui.bootstrap', 'ui.utils', 'ui.router', 'ngAnimate', 'ngResource', 'ngCookies', 'datatables', 'datatables.colvis', 'datatables.tabletools', 'datatables.columnfilter', 'xeditable', 'protocol', 'login', 'toaster', 'users', 'ui.select2', 'animals', 'procedures', 'dashboard', 'gridshore.c3js.chart', 'datePicker', 'ngCsv', 'error', 'ngMask', 'study', 'ui.calendar', 'mgcrea.ngStrap', 'ngDragDrop', 'smart-table', 'clinicalProcedures', 'reports']);

angular.module('ardxx').config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    //delete $httpProvider.defaults.headers.common['X-Requested-With'];
    //$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    //Reset headers to avoid OPTIONS request (aka preflight)
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.defaults.withCredentials = true;

    $httpProvider.responseInterceptors.push('myHttpInterceptor');
    /* Add New States Above */
    $urlRouterProvider.otherwise('/dashboard');

});

angular.module('ardxx').run(function ($rootScope, editableOptions, editableThemes, $state, $cookieStore, $http, $filter, configService) {

    $rootScope.safeApply = function (fn) {
        var phase = $rootScope.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof (fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

    $rootScope.buildEnv = configService.buildEnv;

    $rootScope.$on('loginRequired', function () {
        $rootScope.globals = {};
        $cookieStore.remove('globals');
        $state.go('login', {});
    });

    $rootScope.$on('noApiConnection', function () {
        $state.go('errorservice', {});
    });

    $rootScope.userAuthenticated = false;
    editableOptions.theme = 'bs3'; // bootstrap3 theme.
    editableOptions.blur = 'cancel';
    editableThemes.bs3.inputClass = 'input-sm ';
    editableThemes.bs3.buttonsClass = 'btn-icon-toggle';
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
        var appNav = window.matsadmin.AppNavigation;
        appNav._handleMenubarLeave();
        var globals = $cookieStore.get('globals');
        var protocols = $cookieStore.get('protocols');
        if (toState.url !== '/login' && (typeof globals === 'undefined' || typeof globals.currentUser === 'undefined')) {
            event.preventDefault();
            $state.go('login', {});
        } else {
            if (typeof globals !== 'undefined') {
                $rootScope.userAuthenticated = true;
                $rootScope.currentUser = globals.currentUser;
                $rootScope.allProtocols = protocols;
                if ($rootScope.currentUser.role.indexOf('PRI_INV') > -1) {
                    $rootScope.filterCriteria = '';
                } else if ($rootScope.currentUser.role.indexOf('CHR_PER') > -1) {
                    $rootScope.filterCriteria = 'REVIEW';
                    $rootScope.filterCriteriaSpecific = false;
                } else if ($rootScope.currentUser.role.indexOf('COM_MEM') > -1) {
                    $rootScope.filterCriteria = 'REVIEW';
                    $rootScope.filterCriteriaSpecific = true;
                } else {
                    $rootScope.filterCriteria = 'NONE';
                    $rootScope.filterCriteriaSpecific = true;
                }
            }
            else {
                $rootScope.userAuthenticated = false;
            }
        }
        var elem = angular.element(document.querySelector('body'));
        elem.removeClass('menubar-visible');
    });

    $rootScope.$on('$viewContentLoaded',
        function (event) {
            window.matsadmin.App.initialize();
            window.matsadmin.AppOffcanvas.initialize();
            window.matsadmin.AppCard.initialize();
            window.matsadmin.AppForm.initialize();
            window.matsadmin.AppNavSearch.initialize();
            window.matsadmin.AppVendor.initialize();
        });

})
    .factory('myHttpInterceptor', function ($q, $rootScope) {
        return function (promise) {
            return promise.then(function (response) {
                // do something on success
                if (response && response.headers()['content-type'] === "application/json; charset=utf-8") {
                    // Validate response if not ok reject
                    var data = response; // assumes this function is available
                    if (!data) {
                        return $q.reject(response);
                    }
                }
                return response;
            }, function (response) {
                if (response.status === 0) {
                    $rootScope.$emit('noApiConnection');
                    return $q.reject(response);
                } else if (response.status === 401 && response.data !== 'Bad credentials') {
                    $rootScope.$emit('loginRequired');
                    $q.reject(response);
                } else if (response.status === 500) {
                    if (response.data && response.data.errorMessages) {
                        var errMessages = [];
                        angular.forEach(response.data.errorMessages, function (valMessage) {
                            angular.forEach(valMessage, function (value, key) {
                                errMessages.push(key + ': ' + value);
                            });
                        });
                        response.data.errorMessage = errMessages.join("<br>");

                    } else if (response.data.exception) {
                        response.data.errorMessage = response.data.exception;
                    }
                    return $q.reject(response);
                } else {
                    return response;
                }
            });
        };
    });
