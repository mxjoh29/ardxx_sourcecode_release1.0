﻿angular.module('study').controller('StudydetailsCtrl', function ($stateParams, $scope, studyService, protocolService, toaster, $filter, animalsService, $rootScope, uiCalendarConfig, $compile, $timeout) {

    var roles = {
        chairmanRole: 'CHR_PER',
        piRole: 'PRI_INV',
        memberRole: 'COM_MEM',
        labRole: 'LAB_TEC',
        researchRole: 'RES_SCI',
        vetTech: 'VET_TEC',
        prjManager: 'PJT_MGR'
    };

    $scope.changeCodesList = ['CHG_CODE_01', 'CHG_CODE_02', 'CHG_CODE_03', 'CHG_CODE_04', 'CHG_CODE_05'];

    $scope.procVetTechnician = '';
    $scope.procLabTechnician = '';

    $scope.scheduleCalendarView = true;

    $scope.isChairman = $rootScope.currentUser.role.indexOf(roles.chairmanRole) > -1;
    $scope.isCM = $rootScope.currentUser.role.indexOf(roles.memberRole) > -1;
    $scope.isPI = $rootScope.currentUser.role.indexOf(roles.piRole) > -1;
    $scope.isLab = $rootScope.currentUser.role.indexOf(roles.labRole) > -1;
    $scope.isRes = $rootScope.currentUser.role.indexOf(roles.researchRole) > -1;
    $scope.isVetTech = $rootScope.currentUser.role.indexOf(roles.vetTech) > -1;
    $scope.isPM = $rootScope.currentUser.role.indexOf(roles.prjManager) > -1;

    $scope.studyId = parseInt($stateParams.id) || 0;

    $scope.detailsEditable = false;

    $scope.procedureCreationStatus = 'view';
    $scope.procedureTestsCreationStatus = 'view';

    protocolService.getUsersByRoleId(9)
        .then(function (data) {
            $scope.vetUsers = data;
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });

    protocolService.getUsersByRoleId(10)
       .then(function (data) {
           $scope.labTecUsers = data;
       }, function (data) {
           toaster.pop('error', 'Error', data.errorMessage);
       });


    $scope.getStudyDetails = function () {
        studyService.studyDetails($scope.studyId)
        .then(function (data) {
            $scope.study = data;
            $scope.study.protocolId = data.protocol.id;
            $scope.getGroupsByStudy();
            $scope.getAllAnimalsByStudy();
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.allSpecies = [];

    animalsService.getSpeciesList()
		.then(function (data) {
		    $scope.allSpecies = data;
		},
			function (data) {
			    toaster.pop('error', 'Error', data.errorMessage);
			});

    if ($scope.studyId === 0) {
        $scope.detailsEditable = true;
    }
    else {
        $scope.getStudyDetails();
    }

    $scope.isFormSubmitted = false;
    $scope.groupAnimals = true;
    $scope.selectedGroupId = -1;

    $scope.isProcedureDetails = false;
    $scope.isAnimalAdd = false;
    $scope.studyCollapsed = false;

    $scope.animalsList = [];

    $scope.proceduresList = [];

    $scope.animalGroups = [];

    $scope.changeProcedureView = function () {
        $scope.isProcedureDetails = !$scope.isProcedureDetails;
    };

    $scope.changeAnimalStatus = function () {
        $scope.isAnimalAdd = false;
    };

    protocolService.getProtocolByUserAndStatus($rootScope.currentUser.id, 'APPROVED')
		.then(function (protocolList) {
		    $scope.protocols = protocolList;
		},
		function (data) {
		    toaster.pop('error', 'Error', data.errorMessage);
		});

    var dummyStudy = {
        'serialNumber': '',
        'name': '',
        'chargeCode': '',
        'closedDate': '',
        'comment': '',
        'protocolId': null,
        'animalIds': null,
        'groups': null
    };

    $scope.study = angular.copy(dummyStudy);

    $scope.saveStudyInformation = function () {
        $scope.isFormSubmitted = true;
        if ($scope.studyForm.$valid) {
            if ($scope.study.id) {
                studyService.updateStudy($scope.study)
            .then(function (data) {
                $scope.study = data;
                $scope.study.protocolId = data.protocol.id;
                $scope.detailsEditable = false;

                $scope.isFormSubmitted = false;

                toaster.pop('success', 'Updated', 'Study details updated successfully.');
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                    $scope.isFormSubmitted = false;
                });
            } else {
                studyService.createStudy($scope.study)
            .then(function (data) {
                $scope.study = data;
                $scope.study.protocolId = data.protocol.id;
                $scope.detailsEditable = false;

                $scope.isFormSubmitted = false;

                toaster.pop('success', 'Created', 'Study created successfully.');
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                    $scope.isFormSubmitted = false;
                });
            }

        }
    };

    $scope.showGroupAnimals = function () {
        $scope.groupAnimals = true;
    };

    $scope.hideGroupAnimals = function () {
        $scope.groupAnimals = false;
    };

    $scope.getCloseDate = function () {
        $scope.study.closedDate = '';
        if ($scope.study.protocolId && $scope.study.protocolId !== '' && $scope.study.protocolId !== 0) {
            var prots = $filter('filter')($scope.protocols, $scope.study.protocolId);
            $scope.study.closedDate = prots[0].expirationDate;
        }
    };

    $scope.createGroup = function () {
        $scope.isGroupFormSubmitted = true;
        if ($scope.groupForm.$valid) {
            studyService.addGroup($scope.studyId, $scope.newGroupName)
            .then(function (data) {
                $scope.newGroupName = '';
                $scope.animalGroups = data;
                $scope.isGroupFormSubmitted = false;
                $scope.groupForm.$setPristine();
            }, function (data) {
                toaster.pop('error', 'Error', data.errorMessage);
                $scope.isGroupFormSubmitted = false;
                $scope.groupForm.$setPristine();
            });
        }
    };

    $scope.setGroupActive = function (event, group) {
        $scope.assignAnimalsSelected = false;
        angular.element('.group-li').removeClass('active');
        angular.element(event.currentTarget).addClass('active');
         if (group === -1) {
            $scope.selectedGroupId = -1;
            $scope.getAllAnimalsByStudy();
        }
        else if (group === 0) {
            $scope.selectedGroupId = 0;
            $scope.getAnimalsByStudy();
        } else {
            $scope.selectedGroupId = group.id;
            $scope.animalsList = group.animals;
        }
    };

    $scope.studyAnimals = [];
    $scope.procedureAllAnimals = [];
    $scope.procedureAssignedAnimals = [];
    $scope.unassignedAnimalsSafe = [];

    $scope.getUnassignedAnimals = function () {
        animalsService.getAnimalsByStatus('HOLDING')
        .then(function (data) {
            $scope.unassignedAnimals = data;
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.selectAllAnimals = function(isSelected,animalsArray){
        animalsArray.forEach(function (val) {
            val.selected = isSelected;
          });
    };

    $scope.assignAnimalsToGroup = function () {
        if ($scope.assignToGroup && $scope.assignToGroup !== '') {
            var selectedAnimals = $filter('filter')($scope.animalsList, { selected: true });
            var animalsArray = [];
            angular.forEach(selectedAnimals, function (data, index) {
                animalsArray.push(data.id);
            });
            studyService.assignAnimalsToGroup($scope.studyId, $scope.assignToGroup, animalsArray)
            .then(function (data) {
                $scope.animalGroups = data.animalGroups;
                $scope.animalsList = data.animals;
                toaster.pop('success', 'Success', 'Animals assigned successfully');
                $scope.assignToGroup = '';
            }, function (data) {
                toaster.pop('error', 'Error', data.errorMessage);
            });
        } else {
            toaster.pop('error', 'Error', 'Please select group to assign animals');
        }
    };

    $scope.assignAnimalsTostudy = function () {
        var selectedAnimals = $filter('filter')($scope.unassignedAnimals, { selected: true });
        var animallsArray = [];
        angular.forEach(selectedAnimals, function (data, index) {
            animallsArray.push(data.id);
        });
        studyService.assignAnimalsToStudy($scope.study.id, animallsArray)
        .then(function (data) {
            toaster.pop('success', 'Success', 'Animals assigned successfully');
            $scope.getUnassignedAnimals();
            $scope.animalsList = data.animals;
            $scope.showGroupAnimals();
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.getGroupsByStudy = function () {
        studyService.getGroupsByStudy($scope.study.id)
        .then(function (data) {
            $scope.animalGroups = data;
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.getAnimalsByStudy = function () {
        studyService.getAnimalsByStudy($scope.study.id)
        .then(function (data) {
            $scope.animalsList = data;
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.getAllAnimalsByStudy = function () {
        studyService.getAllAnimalsByStudy($scope.study.id)
        .then(function (data) {
            $scope.animalsList = data;
            $scope.allStudyAnimals = data;
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.unassignAnimalsFromGroup = function (animalId) {
        studyService.unassignAnimalsFromGroup($scope.study.id, $scope.selectedGroupId, [animalId])
        .then(function (data) {
            $scope.animalGroups = data.animalGroups;
            $scope.animalsList = $filter('filter')($scope.animalGroups, { id: $scope.selectedGroupId })[0].animals;
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.checkAssignedAnimals = function () {
        $scope.assignAnimalsSelected = $filter('filter')($scope.animalsList, { selected: true });
    };

    $scope.startCallback = function (event, ui, title) {
        $scope.oldData = title.selected;
        title.selected = true;
        var selectedItems = $filter('filter')($scope.animalsList, { selected: true });
        $scope.selectedAnimalsList = angular.copy(selectedItems);
        ui.helper.removeClass('list-group-item').addClass('btn btn-default').html('Moving ' + selectedItems.length + ' item');
        $scope.$apply();
    };

    $scope.stopCallback = function (event, ui, item) {
        item.selected = $scope.oldData;
        $scope.$apply();
    };

    $scope.dropCallback = function (event, ui, group) {
        var animalsArray = [];
        angular.forEach($scope.selectedAnimalsList, function (data, index) {
            animalsArray.push(data.id);
        });
        studyService.assignAnimalsToGroup($scope.studyId, group.id, animalsArray)
        .then(function (data) {
            $scope.animalGroups = data.animalGroups;
            $scope.animalsList = data.animals;
            toaster.pop('success', 'Success', 'Animals assigned successfully');
            $scope.assignToGroup = '';
            //$scope.$apply();
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.unassignAnimalsFromStudy = function (index, animalId) {
        studyService.unassignAnimalsFromStudy($scope.study.id, [animalId])
        .then(function (data) {
            toaster.pop('success', 'Success', 'Animals assigned successfully');
            $scope.students.splice(index, 1);
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.unassignAnimals = function (index, animalId) {
        if ($scope.selectedGroupId === 0) {
            $scope.unassignAnimalsFromStudy(index, animalId);
        } else {
            $scope.unassignAnimalsFromGroup(animalId);
        }
    };

    $scope.isAddProcedure = false;
    $scope.showAddProcedure = function () {
        $scope.isProcedureDetails = true;
        $scope.procedureCreationStatus = 'create';
        $scope.studyProcedure = {};
    };

    $scope.hideAddProcedure = function () {
        $scope.isProcedureDetails = true;
    };

    $scope.procedureTypes = [];
    $scope.procedureSubTypes = [];

    //Blood sample fields
    $scope.bloodSampleFields = {
        tubeTypes: [],
        quantity: [],
        multipleTubes: [],
        quantityPerTube: [],
        test: [],
        destination: [],
        storageRequirements: [],
        packagingRequirements: [],
        timeShippedBy: [],
        shippedTo: [],
        building: [],
        freezer: [],
        shelf: [],
        rack: [],
        position: []
    };

    $scope.getProcedureTypes = function () {
        studyService.getProcedureTypes()
        .then(function (data) {
            $scope.procedureTypes = data;
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };
    $scope.studyProcedure = {};
    $scope.saveProcedureDetails = function () {
        if($scope.studyProcedure){
            if ($scope.studyProcedure && $scope.studyProcedure.procedureType === 'BLOOD SAMPLE'){
                if (!$scope.studyProcedure.bloodSample.unit) {
                    $scope.studyProcedure.bloodSample.unit = 'UL';
                }
                $scope.studyProcedure.inoculation = null;
            } else if($scope.studyProcedure.procedureType === 'INOCULATION'){
                if (!$scope.studyProcedure.inoculation.dosageUnit) {
                    $scope.studyProcedure.inoculation.dosageUnit = 'ul/kg';
                }
                if (!$scope.studyProcedure.inoculation.volumeUnit) {
                    $scope.studyProcedure.inoculation.volumeUnit = 'ul';
                }
                $scope.studyProcedure.bloodSample = null;
            }
        }

        studyService.addProcedure($scope.study.id, $scope.studyProcedure)
        .then(function (data) {
            var procLab = $scope.studyProcedure.procLabTechnician;
            var procVet = $scope.studyProcedure.procVetTechnician;
            $scope.studyProcedure = data;
            if (procLab) {
                $scope.assignTecToProcedure('LAB_TEC', procLab);
            }

            if (procVet) {
                $scope.assignTecToProcedure('VET_TEC', procVet);
            }

            toaster.pop('success', 'Success', 'Procedure created successfully.');
            $scope.isProcedureDetails = false;
            $scope.getStudyDetails();
            $scope.procedureCreationStatus = 'view';
            $scope.procedureTestsCreationStatus = 'view';
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.procedureConstantValues = function () {
        studyService.getProcedureConstantValues()
        .then(function (data) {
            $scope.procedureConstantValues = data;
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.procedureConstantValues();

    $scope.biopsyFields = {
        specimen: []
    };

    $scope.isProcedureAnimalsView = true;

    $scope.getProcedureDetails = function (procedureId) {
        studyService.getProcedureDetails(procedureId)
       .then(function (data) {
           $scope.studyProcedure = data;
           if ($scope.studyProcedure.vetTechnician) {
               $scope.studyProcedure.procLabTechnician = $scope.studyProcedure.labTechnician ? $scope.studyProcedure.labTechnician.id : undefined;
               $scope.studyProcedure.procVetTechnician = $scope.studyProcedure.vetTechnician ? $scope.studyProcedure.vetTechnician.id : undefined;
               $scope.procTechnicianDetails = $scope.studyProcedure.vetTechnician;
           }
           if (data.bloodSample.procedureTests && data.bloodSample.procedureTests.length > 0) {
               $scope.procedureTestsCreationStatus = 'view';
           } else {
               $scope.procedureTestsCreationStatus = 'create';
           }
       }, function (data) {
           toaster.pop('error', 'Error', data.errorMessage);
       });
    };

    $scope.changeProcedureStatus = function (procedurestatus) {
        studyService.changeProcedureStatus($scope.study.id, $scope.studyProcedure.id, procedurestatus)
      .then(function (data) {
          $scope.studyProcedure = data;
          toaster.pop('success', 'Status', 'Status updated successfully');
      }, function (data) {
          toaster.pop('error', 'Error', data.errorMessage);
      });
    };

    $scope.assignTecToProcedure = function (userType, userId) {
        studyService.assignTecToProcedure($scope.study.id, $scope.studyProcedure.id, userType, userId)
      .then(function (data) {
          $scope.studyProcedure = data;
      }, function (data) {
          toaster.pop('error', 'Error', data.errorMessage);
      });
    };

    $scope.assignVetTecToProcedure = function () {
        studyService.assignTecToProcedure($scope.study.id, $scope.studyProcedure.id, 'VET_TEC', $scope.studyProcedure.procVetTechnician)
      .then(function (data) {
          $scope.studyProcedure = data;
      }, function (data) {
          toaster.pop('error', 'Error', data.errorMessage);
      });
    };

    $scope.saveProcedureTestDetails = function () {
        var proc = $scope.studyProcedure;

        var testDetails = {
            test: proc.bloodSample.test,
            destination: proc.bloodSample.destination,
            storageRequirements: proc.bloodSample.storageRequirements,
            packagingRequirements: proc.bloodSample.packagingRequirements,
            timeShipppedBy: proc.bloodSample.timeShipppedBy,
            shippedTo: proc.bloodSample.shippedTo
        };
        studyService.addProcedureTestDetails($scope.study.id, $scope.studyProcedure.id, testDetails)
       .then(function (data) {
           $scope.studyProcedure = data;
       }, function (data) {
           toaster.pop('error', 'Error', data.errorMessage);
       });
    };

    $scope.showProcedureEditDetails = function (procId) {
        $scope.procedureCreationStatus = 'view';
        $scope.procedureTestsCreationStatus = 'view';
        $scope.changeProcedureView();
        $scope.getProcedureDetails(procId);
    };
    $scope.allStudyAnimals = [];

    $scope.assignAnimalsToProcedure = function () {
        var selectedAnimals = $filter('filter')($scope.allStudyAnimals, { selected: true });
        var animalsArray = [];
        angular.forEach(selectedAnimals, function (data, index) {
            animalsArray.push(data.id);
        });
        studyService.assignAnimalsToProcedure($scope.studyId, $scope.studyProcedure.id, animalsArray)
        .then(function (data) {
            $scope.studyProcedure = data;
            toaster.pop('success', 'Success', 'Animals assigned successfully');
            $scope.assignToGroup = '';
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.unassignAnimalsFromProcedure = function (animalId) {
        studyService.unassignAnimalsFromProcedure($scope.study.id, $scope.studyProcedure.id, [animalId])
        .then(function (data) {
            $scope.studyProcedure = data;
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.createSchedule = function (sheduleDate) {
        var schedule = {
            "scheduleDate": moment(sheduleDate).format("YYYY-MM-DDTHH:mm:ss"),
            "status": "SCHEDULED"
        };
        studyService.scheduleProcedure($scope.study.id, $scope.studyProcedure.id, schedule)
        .then(function (data) {
            $scope.studyProcedure = data;
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.deleteSchedule = function (sheduleId) {
        studyService.deleteSchedule($scope.study.id, $scope.studyProcedure.id, sheduleId)
        .then(function (data) {
            //$scope.studyProcedure = data;
            toaster.pop('success', 'Deleted', 'Deleted schedule successfully');
            mapProcedureSchedules();
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    function mapProcedureSchedules() {
        $scope.procedureSource = [];

        angular.forEach($scope.studyProcedure.procedureSchedules, function (data, index) {
            var eventData = {
                id:data.id,
                title: 'SCHEDULE - '+ data.status,
                start: moment.utc(data.scheduleDate).format("YYYY-MM-DDTHH:mm:ss"),
                status: data.status
            };
            $scope.procedureSource.push(eventData);

            uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('renderEvent', eventData, true);
        });
    }

    $scope.getSchedulesByStudy = function (sheduleId) {
        studyService.getSchedulesByStudy($scope.study.id)
        .then(function (data) {
            $scope.studySchedulesList = data;
            mapStudySchedules();
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    function mapStudySchedules() {
        $scope.studySchedules = [];

        angular.forEach($scope.studySchedulesList, function (data, index) {
            var eventData = {
                id:data.id,
                title: data.procedureId + ' - ' + data.status+ ' - ' + data.tubeType+ ' - ' + data.quantity + data.unit,
                start: moment.utc(data.scheduleDate).format("YYYY-MM-DDTHH:mm:ss"),
                status: data.status
            };
            $scope.studySchedules.push(eventData);

            uiCalendarConfig.calendars['studyScheduleCalendar'].fullCalendar('renderEvent', eventData, true);
        });
    }

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $scope.uiConfig = {
        calendar: {
            height: '100%',
            editable: ($scope.isPI || $scope.isRes || $scope.isPM),
            header: false,
            selectable: ($scope.isPI || $scope.isRes || $scope.isPM),
            eventRender: $scope.eventRender,
            select: function (startDate, endDate) {
                if (window.confirm('Do you want create this schedule')) {
                    $scope.createSchedule(startDate);
                    var eventData;
                    eventData = {
                        title: 'Schedule',
                        start: startDate
                    };
                    uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('renderEvent', eventData, true); // stick? = true
                    uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('unselect');
                }
            }
        },
        studyCalendar: {
            height: '100%',
            editable: ($scope.isPI || $scope.isRes || $scope.isPM),
            header: false,
            selectable: ($scope.isPI || $scope.isRes || $scope.isPM),
            dayClick: function (data, event, view) {
                $scope.selectedScheduleDay = data.utc().format('MM/DD/YYYY');
                $('#studyScheduleModal').modal('show');
            }
        }
    };

    $scope.dateToClone = '';

    $scope.cloneStudySchedules = function(){
        studyService.cloneSchedules($scope.study.id,moment($scope.selectedScheduleDay).format("YYYY-MM-DDT00:00:00"),moment($scope.dateToClone).format("YYYY-MM-DDT00:00:00"))
        .then(function (data) {
            $scope.studySchedulesList = data;
            mapStudySchedules();
            toaster.pop('success', 'Success', 'Cloned sccessfully');
            $scope.selectedScheduleDay = '';
            $('#studyScheduleModal').modal('hide');
            $scope.dateToClone = '';
        }, function (data) {
            toaster.pop('error', 'Error', data.errorMessage);
        });
    };

    $scope.procedureSource = [];


    $scope.eventRender = function (event, element, view) {
        element.attr({
            'tooltip': event.title,
            'tooltip-append-to-body': true
        });
        $compile(element)($scope);
    };


    $scope.dayChanged = function (event, element) {
        console.log(element);
    };

    $scope.changeView = function (view, calendar) {
        uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
        $scope.displayDate();
        $scope.displayScheduleDate();
    };

    $scope.nextDay = function (calendar) {
        uiCalendarConfig.calendars[calendar].fullCalendar('next');
        $scope.displayDate();
        $scope.displayScheduleDate();
    };

    $scope.prevDay = function (calendar) {
        uiCalendarConfig.calendars[calendar].fullCalendar('prev');
        $scope.displayDate();
        $scope.displayScheduleDate();
    };

    $scope.displayDate = function () {
        var selectedDate, view, format;
        if (uiCalendarConfig.calendars['procedureCalendar']) {
            selectedDate = uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('getDate');
            view = uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('getView');
        } else {
            selectedDate = new Date();
        }
        if (view && view.title) {
            $scope.selectedDay = view.title;
        } else {
            $scope.selectedDay = moment(selectedDate).format("MMMM YYYY");
        }
    };

    $scope.displayScheduleDate = function () {
        var selectedDate, view, format;
        if (uiCalendarConfig.calendars['studyScheduleCalendar']) {
            selectedDate = uiCalendarConfig.calendars['studyScheduleCalendar'].fullCalendar('getDate');
            view = uiCalendarConfig.calendars['studyScheduleCalendar'].fullCalendar('getView');
        } else {
            selectedDate = new Date();
        }
        if (view && view.title) {
            $scope.selectedScheduleDay = view.title;
        } else {
            $scope.selectedScheduleDay = moment(selectedDate).format("MMMM YYYY");
        }
    };

    //$scope.displayDate();

    $scope.renderCalendar = function () {
        $timeout(function () {
            $scope.displayDate();
            uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('render');
            uiCalendarConfig.calendars['procedureCalendar'].fullCalendar('rerenderEvents');
            mapProcedureSchedules();
        }, 0);
    };

    $scope.studySchedules = [];

    $scope.renderStudyCalendar = function () {
        $timeout(function () {
            $scope.displayScheduleDate();
            uiCalendarConfig.calendars['studyScheduleCalendar'].fullCalendar('render');
            uiCalendarConfig.calendars['studyScheduleCalendar'].fullCalendar('rerenderEvents');
            $scope.getSchedulesByStudy();
        }, 0);
    };

    $scope.showScheduleListView = function () {
        $scope.scheduleCalendarView = false;
    };

    $scope.showScheduleCalendarView = function () {
        $scope.scheduleCalendarView = true;
    };

    $scope.convertUtcDate = function(date,format){
        return moment.utc(date).format(format);
    };

    window.scope = $scope;
});
