angular.module('study').controller('ProceduredetailsCtrl', function ($scope, $stateParams, $rootScope, studyService, protocolService, toaster,$filter) {
    $scope.scheduleId = parseInt($stateParams.id) || 0;

    $scope.scheduleTime = $stateParams.time || 0;

    var roles = {
        chairmanRole: 'CHR_PER',
        piRole: 'PRI_INV',
        memberRole: 'COM_MEM',
        labRole: 'LAB_TEC',
        researchRole: 'RES_SCI',
        vetTech: 'VET_TEC',
        prjManager: 'PJT_MGR'
    };

    $scope.procVetTechnician = '';
    $scope.procLabTechnician = '';

    $scope.scheduleCalendarView = true;

    $scope.currentSchedule = {};

    $scope.isChairman = $rootScope.currentUser.role.indexOf(roles.chairmanRole) > -1;
    $scope.isCM = $rootScope.currentUser.role.indexOf(roles.memberRole) > -1;
    $scope.isPI = $rootScope.currentUser.role.indexOf(roles.piRole) > -1;
    $scope.isLab = $rootScope.currentUser.role.indexOf(roles.labRole) > -1;
    $scope.isRes = $rootScope.currentUser.role.indexOf(roles.researchRole) > -1;
    $scope.isVetTech = $rootScope.currentUser.role.indexOf(roles.vetTech) > -1;
    $scope.isPM = $rootScope.currentUser.role.indexOf(roles.prjManager) > -1;

    $scope.sedationDrugs = [
        { drug: 'KETAMINE', concentration: 100, unit: 'mg/ml' },
        { drug: 'TELAZOL', concentration: 50, unit: 'mg/ml' },
        { drug: 'PROPOFOL', concentration: 10, unit: 'mg/ml' },
        { drug: 'BUPRENORPHINE', concentration: 0.3, unit: 'mg/ml' },
        { drug: 'ZOLOFT', concentration: 100, unit: 'mg/tablet' },
        { drug: 'HALOPERIDOL', concentration: 100, unit: 'mg/tablet' }
    ];

    $scope.selectedDrug = {};
    $scope.sedationDetails = {};

    $scope.changeSelectedDrug = function () {
        $scope.selectedDrug = $filter('filter')($scope.sedationDrugs, $scope.sedationDetails.substance)[0];
        $scope.calculateVolume();
    };

    $scope.calculateVolume = function () {
        $scope.sedationDetails.volume = (parseFloat($scope.sedationAnimal.weight) * parseFloat($scope.sedationDetails.dosage)) / parseFloat($scope.selectedDrug.concentration);
    };

    $scope.getProcedureDetails = function (scheduleId) {
        studyService.getProcedureBySchedule(scheduleId)
       .then(function (data) {
           $scope.studyProcedure = data;
           $scope.getAnimalsBySchedule();
           $scope.currentSchedule = $filter('filter')($scope.studyProcedure.procedureSchedules, { id: scheduleId })[0];
       }, function (data) {
           toaster.pop('error', 'Error', data.errorMessage);
       });
    };

    $scope.getProcedureDetails($scope.scheduleId);

    $scope.changeScheduleStatus = function (procedurestatus) {

        var schedule = {
            id: $scope.scheduleId,
            scheduleDate: $filter('date')($scope.scheduleTime, "yyyy-MM-ddTHH:mm:ss"),
            status: procedurestatus
        };
        studyService.updateSchedule(1, $scope.studyProcedure.id, schedule)
      .then(function (data) {
          $scope.studyProcedure = data;
          if (procedurestatus === 'IN PROGRESS') {
              toaster.pop('success', 'Seccess', 'Schedule initiated successfully');
          } else if (procedurestatus === 'COMPLETE') {
              toaster.pop('success', 'Seccess', 'Schedule completed successfully');
          }

          $scope.getProcedureDetails($scope.scheduleId);
      }, function (data) {
          toaster.pop('error', 'Error', data.errorMessage);
      });
    };

    $scope.showStorageDetails = function (id) {
        $scope.isStorageDetails = true;
    };

    $scope.hideStorageDetails = function () {
        $scope.isStorageDetails = false;
    };

    $scope.sedationAnimal = {};

    $scope.openSedationDetails = function (isSedationRequired) {
        $scope.sedationDetails.sedationRequired = isSedationRequired;
        var animal = $scope.sedationAnimal;
        if (isSedationRequired) {
            $scope.sedationDetails = {
                substance: animal.substance,
                dosage: animal.dosage,
                volume:animal.volume,
                temperature: animal.temperature,
                pulse: animal.pulse,
                respiration: animal.respiration,
                weight: animal.weight,
                notes: animal.notes
            };
            angular.element('#sedationModalDetails').modal('show');
        } else {
            $scope.sedationDetails = {};
            $scope.sedationDetails.sedationRequired = false;
            $scope.saveSedationDetails();
        }
    };

    $scope.tprwinfo = {};

    $scope.opentprwDetails = function () {
        var animal = $scope.sedationAnimal;
        $scope.sedationDetails = {
            substance: animal.substance,
            dosage: animal.dosage,
            volume: animal.volume,
            temperature: animal.temperature,
            pulse: animal.pulse,
            respiration: animal.respiration,
            weight: animal.weight,
            notes: animal.notes
        };
        $scope.sedationAnimal.sedationDetails = $scope.sedationDetails;
        angular.element('#tprwModalDetails').modal('show');
    };

    $scope.showDetails = function (data) {
        $scope.sedationAnimal = data;
    };

    $scope.saveSedationDetails = function () {
        console.log($scope.sedationDetails);
        studyService.saveSedationDetails($scope.studyProcedure.studyId, $scope.studyProcedure.id, $scope.scheduleId, $scope.sedationAnimal.id, $scope.sedationDetails)
      .then(function (data) {
          $scope.studyProcedure = data;
          $scope.sedationDetails = {};
          $scope.selectedDrug = {};
          angular.element('#sedationModalDetails').modal('hide');
          $scope.getProcedureDetails($scope.scheduleId);
      }, function (data) {
          toaster.pop('error', 'Error', data.errorMessage);
      });
    };

    $scope.saveTPRWInformation = function () {
        studyService.saveSedationDetails($scope.studyProcedure.studyId, $scope.studyProcedure.id, $scope.scheduleId, $scope.sedationAnimal.id, $scope.sedationDetails)
      .then(function (data) {
          $scope.sedationDetails = {};
          angular.element('#tprwModalDetails').modal('hide');
          $scope.getProcedureDetails($scope.scheduleId);
      }, function (data) {
          toaster.pop('error', 'Error', data.errorMessage);
      });
    };

    $scope.getAnimalsBySchedule = function () {
        studyService.getAnimalsBySchedule($scope.studyProcedure.studyId, $scope.studyProcedure.id, $scope.scheduleId)
      .then(function (data) {
          $scope.scheduleAnimals = data;
      }, function (data) {
          toaster.pop('error', 'Error', data.errorMessage);
      });
    };

    $scope.tprwEdit = false;

    $scope.updateSeataionDetaials = function() {
        studyService.updateSedationDetails($scope.studyProcedure.studyId, $scope.scheduleId, $scope.sedationDetails.substance)
          .then(function (data) {
              $scope.scheduleAnimals = data;
          }, function (data) {
              toaster.pop('error', 'Error', data.errorMessage);
          });
    };

    $scope.savetprwValues = function() {
        var animalDetails = [];

        angular.forEach($scope.scheduleAnimals, function (data, index) {
                animalDetails.push(
                {
                    'id':data.animalTableId,
                    'temperature':data.temperature,
                    'pulse':data.pulse,
                    'respiration':data.respiration,
                    'weight':data.weight,
                    'volume':data.volume
                }
                );
            });
        studyService.updateTprwValues($scope.studyProcedure.studyId, $scope.studyProcedure.id, $scope.scheduleId, animalDetails)
          .then(function (data) {
              $scope.scheduleAnimals = data;
              $scope.tprwEdit = false;
          }, function (data) {
              toaster.pop('error', 'Error', data.errorMessage);
          });
    };

    $scope.getstaticDataSedation = function(){
        studyService.getStaticData('substance.name')
          .then(function (data) {
              $scope.sedationDrugs = data;
          }, function (data) {
              toaster.pop('error', 'Error', data.errorMessage);
          });
    };

    $scope.getstaticDataSedation();

    window.scope = $scope;
});
