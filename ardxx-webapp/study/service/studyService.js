angular.module('study').factory('studyService', function ($http, configService, $q) {

    var studyService = {};

    studyService.studyList = function () {
        var deferred = $q.defer();
        $http.get('{0}study?page={1}&size={2}'.format(configService.getHostName, 0, 100))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.studyDetails = function (studyId) {
        var deferred = $q.defer();
        $http.get('{0}study/{1}'.format(configService.getHostName, studyId))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.createStudy = function (study) {
        var deferred = $q.defer();
        $http.post('{0}study'.format(configService.getHostName), study)
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.updateStudy = function (study) {
        var deferred = $q.defer();
        $http.put('{0}study/{1}'.format(configService.getHostName,study.id), study)
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.assignAnimalsToStudy = function (studyId, animals) {
        var deferred = $q.defer();
        $http.put('{0}study/{1}/animals'.format(configService.getHostName, studyId), { 'animalIds': animals })
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.unassignAnimalsFromStudy = function (studyId, animals) {
        var deferred = $q.defer();
        $http.delete('{0}study/{1}/animals'.format(configService.getHostName, studyId), { 'animalIds': animals })
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.assignAnimalsToProcedure = function (studyId, procedureId, animals) {
        var deferred = $q.defer();
        $http.put('{0}study/{1}/procedure/{2}/animals'.format(configService.getHostName, studyId, procedureId), { 'animalIds': animals })
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.unassignAnimalsFromProcedure = function (studyId, procedureId, animals) {
        var deferred = $q.defer();
        $http.delete('{0}study/{1}/procedure/{2}/animals'.format(configService.getHostName, studyId, procedureId), { 'animalIds': animals })
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.unassignAnimalsFromProcedureById = function (studyId, procedureId, animalId) {
        var deferred = $q.defer();
        $http.delete('{0}study/{1}/procedure/{2}/animals/{3}'.format(configService.getHostName, studyId, procedureId,animalId))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.assignAnimalsToGroup = function (studyId, groupId, animals) {
        var deferred = $q.defer();
        $http.put('{0}study/{1}/group/{2}/assign'.format(configService.getHostName, studyId, groupId), { 'animalIds': animals })
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.unassignAnimalsFromGroup = function (studyId, groupId, animals) {
        var deferred = $q.defer();
        $http.put('{0}study/{1}/group/{2}/unassign'.format(configService.getHostName, studyId, groupId), { 'animalIds': animals })
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.addGroup = function (studyId, groupName) {
        console.log(groupName);
        var deferred = $q.defer();
        $http.put('{0}study/{1}/group'.format(configService.getHostName, studyId), { 'groupName': groupName })
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.deleteGroup = function (studyId, groupId) {
        var deferred = $q.defer();
        $http.post('{0}study/group'.format(configService.getHostName), { 'strudyId': studyId, 'group': groupId })
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.getGroupsByStudy = function (studyId) {
        var deferred = $q.defer();
        $http.get('{0}study/{1}/group'.format(configService.getHostName, studyId))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.getAnimalsByStudy = function (studyId) {
        var deferred = $q.defer();
        $http.get('{0}study/{1}/animals'.format(configService.getHostName, studyId))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.getAllAnimalsByStudy = function (studyId) {
        var deferred = $q.defer();
        $http.get('{0}study/{1}/allanimals'.format(configService.getHostName, studyId))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.getAnimalsByProcedure = function (studyId, procedureId) {
        var deferred = $q.defer();
        $http.get('{0}study/{1}/procedure/{2}/allanimals'.format(configService.getHostName, studyId, procedureId))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.getProcedureTypes = function () {
        var deferred = $q.defer();
        $http.get('{0}study/procedure/type'.format(configService.getHostName))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.getSubProcedureTypes = function (procedureType) {
        var deferred = $q.defer();
        $http.get('{0}study/procedure/type/{1}'.format(configService.getHostName, procedureType))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.getFieldsByType = function (procedureType, subType, field) {
        var deferred = $q.defer();
        $http.get('{0}study/procedure/type/{1}/subtype/{2}/field/{3}'.format(configService.getHostName, procedureType, subType, field))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.getProcedureDetails = function (procedureId) {
        var deferred = $q.defer();
        $http.get('{0}study/procedure/{1}'.format(configService.getHostName, procedureId))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.addProcedure = function (studyId, procedreDetails) {
        var deferred = $q.defer();
        $http.put('{0}study/{1}/procedure'.format(configService.getHostName, studyId), procedreDetails)
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.getProcedureConstantValues = function () {
        var deferred = $q.defer();
        $http.get('{0}study/procedurevalues'.format(configService.getHostName))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.addProcedureTestDetails = function (studyId, procedureId, testDetails) {
        var deferred = $q.defer();
        $http.put('{0}study/{1}/procedure/{2}/tests'.format(configService.getHostName, studyId, procedureId), testDetails)
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.changeProcedureStatus = function (studyId, procedureId, status) {
        var deferred = $q.defer();
        $http.put('{0}study/{1}/procedure/{2}/status/{3}'.format(configService.getHostName, studyId, procedureId, status))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.scheduleProcedure = function (studyId, procedureId, schedule) {
        var deferred = $q.defer();
        $http.put('{0}study/{1}/procedure/{2}/schedule'.format(configService.getHostName, studyId, procedureId),schedule)
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.updateSchedule = function (studyId, procedureId, schedule) {
        var deferred = $q.defer();
        $http.put('{0}study/{1}/procedure/{2}/schedule/{3}'.format(configService.getHostName, studyId, procedureId, schedule.id), schedule)
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.deleteSchedule = function (studyId, procedureId, scheduleId) {
        var deferred = $q.defer();
        $http.delete('{0}study/{1}/procedure/{2}/schedule/{3}'.format(configService.getHostName, studyId, procedureId, scheduleId))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.assignTecToProcedure = function (studyId, procedureId, userType, userId) {
        var deferred = $q.defer();
        $http.put('{0}study/{1}/procedure/{2}/assign/{3}/user/{4}'.format(configService.getHostName, studyId, procedureId, userType, userId))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.getSchedulesByUser = function (userType, userId) {
        var deferred = $q.defer();
        $http.get('{0}study/getProcedureSchedules/{1}/{2}'.format(configService.getHostName, userType, userType === 'pm'?'':userId))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.getProcedureBySchedule = function (scheduleId) {
        var deferred = $q.defer();
        $http.get('{0}study/getProcedures/schedule/{1}/'.format(configService.getHostName, scheduleId))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.saveSedationDetails = function (studyId, procedureId, scheduleId, animalId, sedationDetails) {
        var deferred = $q.defer();
        $http.put('{0}study/{1}/procedure/{2}/schedule/{3}/animal/{4}'.format(configService.getHostName, studyId, procedureId, scheduleId, animalId), sedationDetails)
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.getAnimalsBySchedule = function (studyId, procedureId, scheduleId) {
        var deferred = $q.defer();
        $http.get('{0}study/{1}/procedure/{2}/schedule/{3}/animals'.format(configService.getHostName, studyId, procedureId, scheduleId))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.getSchedulesByStudy = function (studyId) {
        var deferred = $q.defer();
        $http.get('{0}study/{1}/procedure'.format(configService.getHostName, studyId))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };
    studyService.cloneSchedules = function (studyId, fromDate, toDate) {
        var deferred = $q.defer();
        $http.put('{0}study/{1}/cloneschedule'.format(configService.getHostName, studyId),
            {
                'fromDate' : fromDate,
                'toDate':toDate
            }
        )
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.updateSedationDetails = function (studyId, scheduleId, substanceName) {
        var deferred = $q.defer();
        $http.put('{0}study/{1}/schedule/{2}/volume'.format(configService.getHostName, studyId, scheduleId),
            {
                'substanceName':substanceName
            }
        )
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.updateTprwValues = function (studyId, procedureId, scheduleId, animalDetails) {
        var deferred = $q.defer();
        $http.put('{0}study/{1}/procedure/{2}/schedule/{3}/tprw'.format(configService.getHostName, studyId,procedureId, scheduleId), animalDetails)
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.getStaticData = function (constantName) {
        var deferred = $q.defer();
        $http.get('{0}study/staticdata?key={1}'.format(configService.getHostName, constantName))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    studyService.clinicalProceduresList = function () {
        var deferred = $q.defer();
        $http.get('{0}study/clinicalProcedure?page={1}&size={2}'.format(configService.getHostName, 0, 100))
			.success(function (data) {
			    deferred.resolve(data);
			})
			.error(function (data) {
			    deferred.reject(data);
			});
        return deferred.promise;
    };

    return studyService;
});
