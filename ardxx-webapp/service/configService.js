angular.module('ardxx').factory('configService', function () {

	var configService = {};

	var myConfig = {
		"url": "http://52.6.54.126",
		"port": "9000",
		"buildEnv": "QA"
	};

	configService.getHostName = myConfig.url + ':' + myConfig.port + '/';
	
	configService.buildEnv = myConfig.buildEnv;

	return configService;
});