angular.module('users').controller('UserprofileCtrl', function ($scope, $rootScope, $state, $stateParams, toaster, userService) {
    $scope.canCreate = false;
    if ($state.current.name === 'myProfile') {
        $scope.userId = $rootScope.currentUser.id;
    } else {
        $scope.userId = parseInt($stateParams.id) || 0;
    }

    $scope.ownProfile = $scope.userId === $rootScope.currentUser.id;
    var userRoles = $rootScope.currentUser.role;

    $scope.canCreate = (userRoles.indexOf('LAB_ADM') > -1) || (userRoles.indexOf('SUP_ADM') > -1);

    if (!$scope.canCreate && $scope.userId !== $rootScope.currentUser.id) {
        $state.go('users-list', {});
    }



    $scope.roles = [];
    window.scope = $scope;
    userService.rolesList()
        .then(function (data) {
            $scope.roles = data;
        },
            function (data) {
                toaster.pop('error', 'Error', data.errorMessage);
            });

    if ($scope.userId && $scope.userId !== '0') {
        userService.getUserDetails($scope.userId)
            .then(function (data) {
                $scope.user = angular.copy(data);
                $scope.user.password ="******";
                $scope.user.confirmPassword="******";
                $scope.user.roles = [];
                $scope.userRoles = angular.copy(data.roles);
                angular.forEach(data.roles, function (value, key) {
                    if (angular.isNumber(parseInt(value.id)) && !isNaN(parseInt(value.id))) {
                        $scope.user.roles.push(value.id);
                    }
                });
            }, function (data) {
                toaster.pop('error', 'Error', data.errorMessage);
            });
    } else {
        $scope.user = {
            'email': '',
            'firstName': '',
            'password': '',
            'confirmPassword': '',
            'middleName': '',
            'lastName': '',
            'phoneNumber': '',
            'roles': [],
            'status': 'ACTIVE',
            'externalUser': false
        };
        $scope.detailsEditable = true;
    }

    $scope.saveProfile = function () {
        $scope.isSave = true;
        if($scope.user.password === "******")
        {
            $scope.user.password ="";
            $scope.user.confirmPassword ="";
            
        }
        $scope.isFormSubmitted = true;
        if ($scope.user.externalUser && $scope.user.roles.length > 0 && $scope.user.roles.map(Number).indexOf(7) <= -1) {
             $scope.isSave = false;
            toaster.pop('error', 'Error', 'External user can\'t be true for users apart from COM_MEM.');
            return;
        }
        if ($scope.userForm.$valid && $scope.user.password === $scope.user.confirmPassword) {
            var usr = $scope.user;


            var usrroles = [];
            angular.forEach(usr.roles, function (value) {
                if (angular.isNumber(parseInt(value)) && !isNaN(parseInt(value))) {
                    usrroles.push(parseInt(value));
                }
            });

            var user = {
                'email': usr.email,
                'firstName': usr.firstName,
                'middleName': usr.middleName,
                'lastName': usr.lastName,
                'roleIds': usrroles.map(Number),
                'status': usr.status,
                'phoneNumber': usr.phoneNumber,
                'externalUser': usr.externalUser
            };
            if (usr.id) {
                if (usr.password) {
                    user.passwordHash = usr.password;
                }
                user.id = usr.id;
                userService.updateDetails(user)
                    .then(function (data) {
                        toaster.pop('success', 'Updated', 'User updated successfully.');
                        $scope.user = angular.copy(data);
                        // $scope.user.roles == [];
                        // if($scope.user){
                        //     angular.forEach(data.roles, function(value){
                        //         $scope.user.roles.push(value.id);
                        //     });
                        // }
                        $scope.user.roleIds = usrroles.map(Number);
                        $scope.user.roles = usrroles.map(Number);
                        $scope.userRoles = data.roles;

                        $scope.userForm.$setPristine();
                        $scope.user.password ="******";
                        $scope.user.confirmPassword ="******";
                        $scope.isSave = false;
                        
                        $scope.isFormSubmitted = false;
                        $scope.detailsEditable = false;
                    },
                        function (data) { 
                            $scope.isSave = false;
                            toaster.pop('error', 'Error', data.errorMessage);
                        });
            } else {
                user.passwordHash = usr.password;
                userService.create(user)
                    .then(function (data) {
                        toaster.pop('success', 'Updated', 'User created successfully.');
                        $scope.user = angular.copy(data);
                        
                        $scope.user.password ="******";
                        $scope.user.confirmPassword ="******";
                        $scope.user.roleIds = usrroles.map(Number);
                        $scope.user.roles = usrroles.map(Number);
                        $scope.userRoles = data.roles;
                        // $state.go('userProfile', {
                        //     id: user.id
                        // });
                        $scope.isSave = false;
                        $scope.detailsEditable = false;
                        // $scope.userForm.$setPristine();
                        // $scope.isFormSubmitted = false;

                        // $scope.user = {
                        //     'email': '',
                        //     'firstName': '',
                        //     'password': '',
                        //     'confirmPassword': '',
                        //     'middleName': '',
                        //     'lastName': '',
                        //     'phoneNumber': '',
                        //     'roles': [],
                        //     'status': 'Active',
                        //     'externalUser': false
                        // };
                        //$scope.$apply();
                    },
                        function (data) {
                             $scope.isSave =false;
                            toaster.pop('error', 'Error', data.errorMessage);
                        });
            }
        } else {
             $scope.isSave = false;
            toaster.pop('error', 'Error', 'Please correct details.');
        }
    };

    $scope.getUserDetails = function (userId) {
        userService.getUserDetails(userId)
            .then(function (data) {
                if (data) {
                    $scope.user = data;
                }
            },
                function (data) {
                    toaster.pop('error', 'Error', data.errorMessage);
                });
    };
});