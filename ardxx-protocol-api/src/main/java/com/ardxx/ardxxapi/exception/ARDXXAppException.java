package com.ardxx.ardxxapi.exception;

import java.util.List;

/**
 * 
 * @author Raj
 *
 */
public class ARDXXAppException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String errorCode;
	
	private List<String> errorCodes;
	
	public ARDXXAppException()
	{
	}
	
	public ARDXXAppException(String errorCode)
	{
		this.errorCode = errorCode;
	}
	
	@Override
	public String getMessage()
	{
		return this.errorCode;
	}
	
	@Override
	public String getLocalizedMessage()
	{
		return this.errorCode;
	}

	public List<String> getErrorCodes()
	{
		return errorCodes;
	}

	public void setErrorCodes(List<String> errorCodes)
	{
		this.errorCodes = errorCodes;
	}

}
