package com.ardxx.ardxxapi.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * 
 * ARDXXUtils class represents contants and utility methods
 * 
 * @author naikraj
 *
 */
public final class ARDXXUtils
{

	private ARDXXUtils()
	{
		super();
	}

	public static final String EMPTY_STRING = " ";
	public static final String SQL_UPDATE = "UPDATE";
	public static final String SQL_SET = "SET";
	public static final String SQL_SELECT = "SELECT";
	public static final String SQL_DELETE = "DELETE";
	public static final String SQL_WHERE = "WHERE";
	public static final String SQL_IN = "IN";
	public static final String SQL_FROM = "FROM";
	public static final String SQL_EQUAL = "=";
	public static final String SQL_SINGLE_QUATE = "'";
	public static final String SQL_SINGLE_QUATE_REPLACE = "''";
	public static final String DATE_FORMAT = "MM/dd/yyyy";
//	public static final String ANIMALS_HEADER="ID,OLDID,SPECIES,SEX,AGECATEGORY,CLIENT,DOB,SOURCE,RECEIVEDDATE,DEPARTEDDATE,ROOMNUM,VIRUS,WEIGHT,REMARKS";	
//	public static final String CBC_HEADER="Accession Number,Group Number,Animal Id,Sex,Species,Age,Breed,Collected Date,A/G RATIO,ABSOLUTE BASOPHIL,ABSOLUTE EOSINOPHIL,ABSOLUTE LYMPHOCYTE,ABSOLUTE METAMYELOCYTES,ABSOLUTE MONOCYTE,ABSOLUTE NEUTROPHIL BAND,ABSOLUTE NEUTROPHIL SEG,ABSOLUTE PROMYELOCYTES,ALBUMIN,ALKALINE PHOSPHATASE,ANISOCYTOSIS,B/C RATIO,BASOPHIL,BICARBONATE,BUN,CALCIUM,CHLORIDE,CHOLESTEROL,CPK,CREATININE,DIRECT BILIRUBIN,EOSINOPHIL,GLOBULIN,GLUCOSE,HCT,HEINZ BODIES,HEMOLYSIS INDEX,HGB,INDIRECT BILIRUBIN,LIPEMIA INDEX,LYMPHOCYTE,MCH,MCHC,MCV,METAMYELOCYTE,MONOCYTE,MYELOCYTE,NA/K RATIO,NEUTROPHIL BAND,NEUTROPHIL SEG,NRBC,PHOSPHORUS,PLATELET COUNT,PLATELET ESTIMATE,POIKILOCYTOSIS,POLYCHROMASIA,POTASSIUM,PROMYELOCYTE,RBC,REMARKS,SGOT (AST),SGPT (ALT),SODIUM,TOTAL BILIRUBIN,TOTAL PROTEIN,WBC,REMARKS";
	public static final String MESSAGE = "message";
	public static final String ANIMAL_ID_PREFIX = "AN-";
	public static final String BLOOD_SAMPLE = "BLOOD SAMPLE";
	public static final String INOCULATION = "INOCULATION";
	public static final String TREATMENT = "TREATMENT";

	
	/**
	 * isEmpty method check the value is null or empty.
	 * 
	 * @param value
	 * @return true if value is null or empty else false
	 */
	public static boolean isEmpty(String value)
	{
		if (value == null)
		{
			return true;
		} else
		{
			return value.trim().isEmpty();
		}
	}

	public static Date dateFormat(String value, String pattern)
	{
		try
		{
			if (value != null && !value.trim().isEmpty())
			{
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
				return simpleDateFormat.parse(value);
			} else
			{
				return null;
			}
		} catch (Exception e)
		{
			System.out.println("[ERROR: ]" + e.getMessage());
			return null;
		}

	}

	/**
	 * generateAnimalId method will generate the animal id.
	 * 
	 * @return alphanumeric value
	 */
	public static String generateAnimalId()
	{
		String value = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		Random rnd = new Random();

		StringBuilder sb = new StringBuilder(6);
		for (int i = 0; i < 6; i++)
			sb.append(value.charAt(rnd.nextInt(value.length())));
		return sb.toString();
	}

}
