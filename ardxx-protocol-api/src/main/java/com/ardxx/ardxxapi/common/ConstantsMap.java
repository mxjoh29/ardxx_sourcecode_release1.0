package com.ardxx.ardxxapi.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

import com.ardxx.ardxxapi.common.domain.Constants;
import com.ardxx.ardxxapi.common.service.ConstantsService;

/**
 * 
 * @author Raj
 *
 */
public class ConstantsMap implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	ConstantsService constantsService;

	@PostConstruct
	public void initialize()
	{
		loadConstants();
	}
	
	@Cacheable(value = "speciescache")
	public Map<String, List<String>> loadConstants()
	{
		Map<String, List<String>> map = null;
		System.out.println("ConstantsMap.loadConstants() ----------------------------- ");
		Iterable<Constants> constants = constantsService.getAllConstants();
		map = new HashMap<String, List<String>>();
		List<String> values = null;
		for (Constants constant : constants)
		{
			if (map.containsKey(constant.getName()))
			{
				if (map.get(constant.getName()) != null)
				{
					map.get(constant.getName()).add(constant.getValue());
				} else
				{
					values = new ArrayList<String>();
					values.add(constant.getValue());
					map.put(constant.getName(), values);
				}
			} else
			{
				values = new ArrayList<String>();
				values.add(constant.getValue());
				map.put(constant.getName(), values);
			}
		}

		return map;
	}

}
