package com.ardxx.ardxxapi.common.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ardxx.ardxxapi.common.dao.ConstantsRepository;
import com.ardxx.ardxxapi.common.domain.Constants;

/**
 * 
 * @author Raj
 *
 */
@Service
public class ConstantsServiceImpl implements ConstantsService
{

	@Autowired
	ConstantsRepository constantsRepository;
		
	@Override
	public Iterable<Constants> getAllConstants()
	{
		return constantsRepository.findAll();
	}

}
