package com.ardxx.ardxxapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.ardxx.ardxxapi.animalmanagement.dao.AnimalsEventHandler;
import com.ardxx.ardxxapi.animalmanagement.service.AnimalHistoryServiceImpl;
import com.ardxx.ardxxapi.common.ConstantsMap;

@SpringBootApplication
// @Configuration
// @EnableAutoConfiguration
// @ComponentScan
@ComponentScan({ "com.ardxx.ardxxapi", "com.tn.security","com.tn.commons" })
@EnableJpaRepositories({ "com.ardxx.ardxxapi", "com.tn.security" })
@EntityScan({ "com.ardxx.ardxxapi", "com.tn.security" })
public class ArdxxapiApplication
{

	public static void main(String[] args)
	{
		SpringApplication.run(ArdxxapiApplication.class, args);
	}
	
	@Bean
	public ConstantsMap constantsMap()
	{
		return new ConstantsMap();
	}
	
	@Bean
	public AnimalsEventHandler animalEventHandler()
	{
		System.out.println("ArdxxapiApplication.animalEventHandler() ***************************************************");
		return new AnimalsEventHandler();
	}
	
	
	@Bean
	public AnimalHistoryServiceImpl animalHistoryServiceImpl()
	{
		System.out.println("ArdxxapiApplication.animalEventHandler() ***************************************************");
		return new AnimalHistoryServiceImpl();
	}
	
	
}
