package com.ardxx.ardxxapi.protocol.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ardxx.ardxxapi.protocol.domain.AnimalRequirements;

public interface AnimalRequirementsRepository extends PagingAndSortingRepository<AnimalRequirements, Long>
{

}
