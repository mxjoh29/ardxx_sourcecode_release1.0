package com.ardxx.ardxxapi.protocol.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Hazardous agents, Section K of protocol data lives in this entity.
 * 
 * @author gopikrishnappa
 *
 */

@Entity
public class HazardousAgents implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private boolean none;

	private boolean radionuclides;
	private String radionuclidesAgentsAndRegNum;
	private boolean bioAgents;
	private String bioAgentsAgentsAndRegNum;
	private boolean chemicalsAndDrugs;
	private String chemicalsAndDrugsAgentsAndRegNum;
	private boolean recombinantDNA;
	private String recombinantDNAAgentsAndRegNum;
	/**
	 * handling procedures, additional safety considerations, occupational health and safety consiterations are captured in this list of user inputs.
	 */
	@ElementCollection
	private List<UserInput> userInputs;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public boolean isNone()
	{
		return none;
	}

	public void setNone(boolean none)
	{
		this.none = none;
	}

	public boolean isRadionuclides()
	{
		return radionuclides;
	}

	public void setRadionuclides(boolean radionuclides)
	{
		this.radionuclides = radionuclides;
	}

	public String getRadionuclidesAgentsAndRegNum()
	{
		return radionuclidesAgentsAndRegNum;
	}

	public void setRadionuclidesAgentsAndRegNum(String radionuclidesAgentsAndRegNum)
	{
		this.radionuclidesAgentsAndRegNum = radionuclidesAgentsAndRegNum;
	}

	public boolean isBioAgents()
	{
		return bioAgents;
	}

	public void setBioAgents(boolean bioAgents)
	{
		this.bioAgents = bioAgents;
	}

	public String getBioAgentsAgentsAndRegNum()
	{
		return bioAgentsAgentsAndRegNum;
	}

	public void setBioAgentsAgentsAndRegNum(String bioAgentsAgentsAndRegNum)
	{
		this.bioAgentsAgentsAndRegNum = bioAgentsAgentsAndRegNum;
	}

	public boolean isChemicalsAndDrugs()
	{
		return chemicalsAndDrugs;
	}

	public void setChemicalsAndDrugs(boolean chemicalsAndDrugs)
	{
		this.chemicalsAndDrugs = chemicalsAndDrugs;
	}

	public String getChemicalsAndDrugsAgentsAndRegNum()
	{
		return chemicalsAndDrugsAgentsAndRegNum;
	}

	public void setChemicalsAndDrugsAgentsAndRegNum(String chemicalsAndDrugsAgentsAndRegNum)
	{
		this.chemicalsAndDrugsAgentsAndRegNum = chemicalsAndDrugsAgentsAndRegNum;
	}

	public boolean isRecombinantDNA()
	{
		return recombinantDNA;
	}

	public void setRecombinantDNA(boolean recombinantDNA)
	{
		this.recombinantDNA = recombinantDNA;
	}

	public String getRecombinantDNAAgentsAndRegNum()
	{
		return recombinantDNAAgentsAndRegNum;
	}

	public void setRecombinantDNAAgentsAndRegNum(String recombinantDNAAgentsAndRegNum)
	{
		this.recombinantDNAAgentsAndRegNum = recombinantDNAAgentsAndRegNum;
	}

	public List<UserInput> getUserInputs()
	{
		return userInputs;
	}

	public void setUserInputs(List<UserInput> userInputs)
	{
		this.userInputs = userInputs;
	}

}
