package com.ardxx.ardxxapi.protocol.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ardxx.ardxxapi.common.Messages;
import com.ardxx.ardxxapi.exception.ARDXXAppException;
import com.ardxx.ardxxapi.exception.ARDXXErrorPayload;
import com.ardxx.ardxxapi.protocol.domain.AnesthesiaMethod;
import com.ardxx.ardxxapi.protocol.domain.AnimalRequirements;
import com.ardxx.ardxxapi.protocol.domain.BioAnimalMaterial;
import com.ardxx.ardxxapi.protocol.domain.Comment;
import com.ardxx.ardxxapi.protocol.domain.DescriptionOfExperiment;
import com.ardxx.ardxxapi.protocol.domain.EuthanasiaMethod;
import com.ardxx.ardxxapi.protocol.domain.HazardousAgents;
import com.ardxx.ardxxapi.protocol.domain.MajorSurvialSurgery;
import com.ardxx.ardxxapi.protocol.domain.NumberOfAnimals;
import com.ardxx.ardxxapi.protocol.domain.PICertification;
import com.ardxx.ardxxapi.protocol.domain.Protocol;
import com.ardxx.ardxxapi.protocol.domain.ProtocolAdministrativeData;
import com.ardxx.ardxxapi.protocol.domain.ProtocolStatus;
import com.ardxx.ardxxapi.protocol.domain.RationalUseOfAnimals;
import com.ardxx.ardxxapi.protocol.domain.RecordPainDistressCategory;
import com.ardxx.ardxxapi.protocol.domain.Review;
import com.ardxx.ardxxapi.protocol.domain.SpecialRequirementOfStudy;
import com.ardxx.ardxxapi.protocol.domain.StudyObjective;
import com.ardxx.ardxxapi.protocol.domain.Transportation;
import com.ardxx.ardxxapi.protocol.dto.ProtocolDTO;
import com.ardxx.ardxxapi.protocol.dto.UpdateJson;
import com.ardxx.ardxxapi.protocol.service.ProtocolService;
import com.tn.commons.email.util.EmailUtil;
import com.tn.security.basic.domain.CurrentUser;
import com.tn.security.basic.dto.UserDTO;
import com.tn.security.exception.ErrorPayload;
import com.tn.security.service.user.UserService;

/**
 * Rest controller to manage Protocol
 * 
 * @author Raj
 *
 */
@RestController
@RequestMapping(value = "/protocol")
public class ProtocolController
{

	private static final Logger logger = LoggerFactory.getLogger(ProtocolController.class);

	@Autowired
	private ProtocolService protocolService;

	@Autowired
	private EmailUtil emailUtil;

	@Autowired
	private UserService userService;

	private static final Long COM_MEM_ROLE_ID = 7L;
	private static final Long CHR_PER_ROLE_ID = 2L;

	// private static final Long PRI_ENV_ROLE_ID = 1L;
	/**
	 * Ping mthod used to know the service is up or not.
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping(value = "/ping/{name}", method = RequestMethod.GET)
	public String ping(@PathVariable("name") String name)
	{
		return "Hi ".concat(name).concat(" protocol service up and running!!!");
	}

	/**
	 * Create protocol method creates the new protocol with empty value.
	 * 
	 * @param {@link Protocol}
	 * @return {@link Protocol}
	 */
	@RequestMapping(method = RequestMethod.POST)
	public Protocol createProtocol(@RequestBody UpdateJson updateJson, @AuthenticationPrincipal CurrentUser activeUser)
	{

		logger.debug("Getting active user ...");
		logger.debug("Active user = " + activeUser.getUsername());
		Protocol protocolFromDB = protocolService.createProtocol(updateJson);

		emailUtil.createEmailTemplate(new String[] { activeUser.getUsername() }, activeUser.getUser().getFirstName(), activeUser.getUser()
				.getLastName(), protocolFromDB.getStatus().toString());

		return protocolFromDB;
	}

	/**
	 * Update protocol method updates the protocol information.
	 * 
	 * @param {@link updateJson}
	 * @return updated {@link Protocol} object.
	 */
	@RequestMapping(value = "/{protocolId}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateProtocol(@PathVariable("protocolId") Long protocolId, @RequestBody UpdateJson updateJson)
	{

		try
		{
			Protocol protocol = protocolService.updateProtocol(updateJson, protocolId);
			if (updateJson.getFieldName().equalsIgnoreCase("status"))
			{

				String[] userEmails = null;
				List<UserDTO> users = new ArrayList<UserDTO>();
				List<UserDTO> committeeMembers = userService.getUsersByRole(COM_MEM_ROLE_ID);
				List<UserDTO> chairPersom = userService.getUsersByRole(CHR_PER_ROLE_ID);

				if (committeeMembers != null && committeeMembers.size() > 0)
				{
					users.addAll(committeeMembers);
				}
				if (chairPersom != null && chairPersom.size() > 0)
				{
					users.addAll(chairPersom);
				}

				if (users != null && users.size() > 0)
				{
					userEmails = new String[users.size()];

					for (int i = 0; i < users.size(); i++)
					{
						userEmails[i] = users.get(i).getEmail();
					}
				} else
				{
					userEmails = new String[] { protocol.getPiUser().getEmail() };
				}

				if (updateJson.getFieldValue().equalsIgnoreCase(ProtocolStatus.REVIEW.name()))
				{

					emailUtil.createEmailTemplate("Review.vm", userEmails, protocol.getId(), protocol.getPiUser().getFirstName());
				}

				else if (updateJson.getFieldValue().equalsIgnoreCase(ProtocolStatus.APPROVED.name()))
				{

					emailUtil.createEmailTemplate("Approved.vm", userEmails, protocol.getId(), protocol.getPiUser().getFirstName());

				}

			}

			return new ResponseEntity<Object>(protocol, HttpStatus.OK);

		} catch (Exception exception)
		{
			exception.printStackTrace();
			System.out.println("[ERROR]: Exception occured. Message:" + exception.getMessage());
			if (exception instanceof ARDXXAppException)
			{
				ARDXXAppException ardxxAppException = (ARDXXAppException) exception;
				return new ResponseEntity<Object>(new ErrorPayload(ardxxAppException.getErrorCodes().toArray(
						new String[ardxxAppException.getErrorCodes().size()])), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{

				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}

		}

	}

	/**
	 * Get protocol method returns the protocol information for a given id
	 * 
	 * @param id
	 * @return {@link Protocol}
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> getProtocol(@PathVariable("id") Long id) throws Exception
	{
		Protocol protocol = protocolService.getProtocol(id);
		if (protocol == null)
		{
			return new ResponseEntity<Object>(new ARDXXErrorPayload(Messages.properties.getProperty("protocol.not.found")),
					HttpStatus.INTERNAL_SERVER_ERROR);
		} else
		{
			return new ResponseEntity<Object>(protocol, HttpStatus.OK);
		}

	}

	/**
	 * Get protocol list method returns the all protocols.
	 * 
	 * @return List<{@link ProtocolDTO}>
	 */
	@RequestMapping(method = RequestMethod.GET)
	public List<ProtocolDTO> getProtocolList(@RequestParam int page, @RequestParam int size)
	{
		return protocolService.getProtocolList(page, size);
	}

	/**
	 * Get method to get all the protocol that are created by given user
	 * 
	 * @param userId
	 *            User Id
	 * @return
	 */
	@RequestMapping(value = "/createdBy/{userId}", method = RequestMethod.GET)
	public ResponseEntity<Object> getProtocolListByUser(@PathVariable("userId") Long userId)
	{
		try
		{
			List<ProtocolDTO> protocols = protocolService.getProtocolList(userId);
			return new ResponseEntity<Object>(protocols, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}

		}
	}

	@RequestMapping(value = "/piuser/{userId}", method = RequestMethod.GET)
	public ResponseEntity<Object> getProtocolListByPIUser(@PathVariable("userId") Long userId)
	{
		try
		{
			List<ProtocolDTO> protocols = protocolService.getProtocolListByPIUser(userId);
			return new ResponseEntity<Object>(protocols, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}

		}
	}

	/**
	 * Delete protocol method delete the particular protocol based on protocol id.
	 * 
	 * @param id
	 * @return deleted status
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deleteProtocol(@PathVariable("id") Long id)
	{
		try
		{
			String message = protocolService.deleteProtocol(id);
			return new ResponseEntity<Object>(message, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	/**
	 * PUT method to update the animal requirements values
	 * 
	 * @param protocolId
	 * @param animalRequirementsId
	 * @param {@link UpdateJson}
	 * @return Returns updated {@link AnimalRequirements}
	 */
	@RequestMapping(value = "/{protocolId}/animalRequirements/{animalRequirementsId}", method = RequestMethod.PUT)
	public AnimalRequirements updateAnimalRequirements(@PathVariable("protocolId") Long protocolId,
			@PathVariable("animalRequirementsId") Long animalRequirementsId, @RequestBody UpdateJson updateJson)
	{
		try
		{
			return protocolService.updateAnimalRequirements(updateJson, protocolId, animalRequirementsId);
		} catch (Exception e)
		{
			System.out.println("[ERROR]: Exception occured. Message:" + e.getMessage());
			return null;
		}

	}

	@RequestMapping(value = "/{protocolId}/animalRequirements/{animalRequirementsId}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deleteAnimalRequirements(@PathVariable("protocolId") Long protocolId,
			@PathVariable("animalRequirementsId") Long animalRequirementsId)
	{
		try
		{
			protocolService.deleteAnimalRequirements(protocolId, animalRequirementsId);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

	}

	/**
	 * PUT method to update Number of animals field values in Animal Requirements
	 * 
	 * @param protocolId
	 * @param animalRequirementsId
	 * @param opid
	 * @param {@link UpdateJson}
	 * @return Returns updated {@link NumberOfAnimals}
	 */
	@RequestMapping(value = "/{protocolId}/animalRequirements/{animalRequirementsId}/numberOfAnimals/{numberOfAnimalsId}", method = RequestMethod.PUT)
	public NumberOfAnimals updateNumberOfAnimals(@PathVariable("protocolId") Long protocolId,
			@PathVariable("animalRequirementsId") Long animalRequirementsId, @PathVariable("numberOfAnimalsId") Long numberOfAnimalsId,
			@RequestBody UpdateJson updateJson)
	{
		try
		{
			return protocolService.updateNumberOfAnimals(updateJson, protocolId, animalRequirementsId, numberOfAnimalsId);
		} catch (Exception e)
		{
			System.out.println("[ERROR]: Exception occured. Message:" + e.getMessage());
			return null;
		}

	}

	/**
	 * PUT method to update the Protocol Administrative Data values
	 * 
	 * @param protocolId
	 * @param protocolAdministrativeDataId
	 * @param {@link UpdateJson}
	 * @return Returns updated {@link ProtocolAdministrativeData}
	 */
	@RequestMapping(value = "/{protocolId}/protocolAdministrativeData/{protocolAdministrativeDataId}", method = RequestMethod.PUT)
	public ProtocolAdministrativeData updateProtocolAdministrativeData(@PathVariable("protocolId") Long protocolId,
			@PathVariable("protocolAdministrativeDataId") Long protocolAdministrativeDataId, @RequestBody UpdateJson updateJson)
	{
		try
		{
			return protocolService.updateProtocolAdministrativeData(updateJson, protocolId, protocolAdministrativeDataId);
		} catch (Exception e)
		{
			System.out.println("[ERROR]: Exception occured. Message:" + e.getMessage());
			return null;
		}

	}

	/**
	 * PUT method to update Animal Procedural Personals values in Protocol Administrative data
	 * 
	 * @param protocolId
	 * @param protocolAdministrativeDataId
	 * @param animalProceduresPersonalsId
	 * @param {@link UpdateJson}
	 * @return Returns updated {@link ProtocolAdministrativeData}
	 */
	@RequestMapping(value = "/{protocolId}/protocolAdministrativeData/{protocolAdministrativeDataId}/animalProceduresPersonals/{animalProceduresPersonalsId}", method = RequestMethod.PUT)
	public ProtocolAdministrativeData updateAnimalProceduresPersonals(@PathVariable("protocolId") Long protocolId,
			@PathVariable("protocolAdministrativeDataId") Long protocolAdministrativeDataId,
			@PathVariable("animalProceduresPersonalsId") Long animalProceduresPersonalsId, @RequestBody UpdateJson updateJson)
	{
		return protocolService.updateAnimalProceduresPersonals(updateJson, protocolId, protocolAdministrativeDataId, animalProceduresPersonalsId);
	}

	@RequestMapping(value = "/{protocolId}/protocolAdministrativeData/{protocolAdministrativeDataId}/animalProceduresPersonals/{animalProceduresPersonalsId}", method = RequestMethod.DELETE)
	public ProtocolAdministrativeData deleteAnimalProceduresPersonals(@PathVariable("protocolId") Long protocolId,
			@PathVariable("protocolAdministrativeDataId") Long protocolAdministrativeDataId,
			@PathVariable("animalProceduresPersonalsId") Long animalProceduresPersonalsId)
	{
		return protocolService.deleteAnimalProceduresPersonals(protocolId, protocolAdministrativeDataId, animalProceduresPersonalsId);
	}

	/**
	 * PUT method to update Co-Investigator values of Protocol Administrative Data
	 * 
	 * @param protocolId
	 * @param protocolAdministrativeDataId
	 * @param coInvestigatorsId
	 * @param {@link UpdateJson}
	 * @return Returns updated {@link ProtocolAdministrativeData}
	 */
	@RequestMapping(value = "/{protocolId}/protocolAdministrativeData/{protocolAdministrativeDataId}/coInvestigators/{coInvestigatorsId}", method = RequestMethod.PUT)
	public ProtocolAdministrativeData updateCoInvestigators(@PathVariable("protocolId") Long protocolId,
			@PathVariable("protocolAdministrativeDataId") Long protocolAdministrativeDataId,
			@PathVariable("coInvestigatorsId") Long coInvestigatorsId, @RequestBody UpdateJson updateJson)
	{
		return protocolService.updateCoInvestigators(updateJson, protocolId, protocolAdministrativeDataId, coInvestigatorsId);
	}

	@RequestMapping(value = "/{protocolId}/protocolAdministrativeData/{protocolAdministrativeDataId}/coInvestigators/{coInvestigatorsId}", method = RequestMethod.DELETE)
	public ProtocolAdministrativeData deleteCoInvestigators(@PathVariable("protocolId") Long protocolId,
			@PathVariable("protocolAdministrativeDataId") Long protocolAdministrativeDataId, @PathVariable("coInvestigatorsId") Long coInvestigatorsId)
	{
		return protocolService.deleteCoInvestigators(protocolId, protocolAdministrativeDataId, coInvestigatorsId);
	}

	/**
	 * PUT method to update Other Personal information of Protocol Administrative Data
	 * 
	 * @param protocolId
	 * @param protocolAdministrativeDataId
	 * @param otherPersonalsId
	 * @param {@link UpdateJson}
	 * @return Returns updated {@link ProtocolAdministrativeData}
	 */
	@RequestMapping(value = "/{protocolId}/protocolAdministrativeData/{protocolAdministrativeDataId}/otherPersonals/{otherPersonalsId}", method = RequestMethod.PUT)
	public ProtocolAdministrativeData updateOtherPersonals(@PathVariable("protocolId") Long protocolId,
			@PathVariable("protocolAdministrativeDataId") Long protocolAdministrativeDataId, @PathVariable("otherPersonalsId") Long otherPersonalsId,
			@RequestBody UpdateJson updateJson)
	{
		return protocolService.updateOtherPersonals(updateJson, protocolId, protocolAdministrativeDataId, otherPersonalsId);
	}

	@RequestMapping(value = "/{protocolId}/protocolAdministrativeData/{protocolAdministrativeDataId}/otherPersonals/{otherPersonalsId}", method = RequestMethod.DELETE)
	public ProtocolAdministrativeData deleteOtherPersonals(@PathVariable("protocolId") Long protocolId,
			@PathVariable("protocolAdministrativeDataId") Long protocolAdministrativeDataId, @PathVariable("otherPersonalsId") Long otherPersonalsId)
	{
		return protocolService.deleteOtherPersonals(protocolId, protocolAdministrativeDataId, otherPersonalsId);
	}

	/**
	 * PUT method to update the Protocol Transportation
	 * 
	 * @param protocolId
	 * @param transportationId
	 * @param {@link UpdateJson}
	 * @return Returns updated {@link Transportation}
	 */
	@RequestMapping(value = "/{protocolId}/transportation/{transportationId}", method = RequestMethod.PUT)
	public Transportation updateTransportation(@PathVariable("protocolId") Long protocolId, @PathVariable("transportationId") Long transportationId,
			@RequestBody UpdateJson updateJson)
	{
		return protocolService.updateTransportation(updateJson, protocolId, transportationId);
	}

	/**
	 * PUT method to update the RationalForUseOfAnimals
	 * 
	 * @param protocolId
	 * @param rationalForUseOfAnimalsId
	 * @param userInputsId
	 * @param {@link UpdateJson}
	 * @return Returns updated {@link RationalUseOfAnimals}
	 */
	@RequestMapping(value = "/{protocolId}/rationalForUseOfAnimals/{rationalForUseOfAnimalsId}/userInputs/{userInputsId}", method = RequestMethod.PUT)
	public RationalUseOfAnimals updateRationalForUseOfAnimals(@PathVariable("protocolId") Long protocolId,
			@PathVariable("rationalForUseOfAnimalsId") Long rationalForUseOfAnimalsId, @PathVariable("userInputsId") Long userInputsId,
			@RequestBody UpdateJson updateJson)
	{
		return protocolService.updateRationalForUseOfAnimals(updateJson, protocolId, rationalForUseOfAnimalsId, userInputsId);
	}

	/**
	 * PUT method to update the Study objective
	 * 
	 * @param protocolId
	 * @param studyObjectiveId
	 * @param userInputsId
	 * @param {@link UpdateJson}
	 * @return Returns updated {@link StudyObjective}
	 */
	@RequestMapping(value = "/{protocolId}/studyObjective/{studyObjectiveId}/userInputs/{userInputsId}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateStudyObjective(@PathVariable("protocolId") Long protocolId,
			@PathVariable("studyObjectiveId") Long studyObjectiveId, @PathVariable("userInputsId") Long userInputsId,
			@RequestBody UpdateJson updateJson)
	{
		try
		{

			StudyObjective studyObjective = protocolService.updateStudyObjective(updateJson, protocolId, studyObjectiveId, userInputsId);
			return new ResponseEntity<Object>(studyObjective, HttpStatus.OK);
		} catch (Exception exception)
		{

			if (exception instanceof ARDXXAppException)
			{
				ARDXXAppException ardxxAppException = (ARDXXAppException) exception;
				return new ResponseEntity<Object>(new ErrorPayload(ardxxAppException.getErrorCodes().toArray(
						new String[ardxxAppException.getErrorCodes().size()])), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				logger.error(" Exception occured. Message:" + exception.getMessage());
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}

		}

	}

	/**
	 * PUT method to update the MajorSurvialSurgery
	 * 
	 * @param protocolId
	 * @param majorSurvialSurgeryId
	 * @param userInputsId
	 * @param {@link UpdateJson}
	 * @return Returns {@link MajorSurvialSurgery}
	 */
	@RequestMapping(value = "/{protocolId}/majorSurvialSurgery/{majorSurvialSurgeryId}/userInputs/{userInputsId}", method = RequestMethod.PUT)
	public MajorSurvialSurgery updateMajorSurvialSurgery(@PathVariable("protocolId") Long protocolId,
			@PathVariable("majorSurvialSurgeryId") Long majorSurvialSurgeryId, @PathVariable("userInputsId") Long userInputsId,
			@RequestBody UpdateJson updateJson)
	{
		return protocolService.updateMajorSurvialSurgery(updateJson, protocolId, majorSurvialSurgeryId, userInputsId);
	}

	@RequestMapping(value = "/{protocolId}/majorSurvialSurgery/{majorSurvialSurgeryId}", method = RequestMethod.PUT)
	public MajorSurvialSurgery updateMajorSurvialSurgeryValue(@PathVariable("protocolId") Long protocolId,
			@PathVariable("majorSurvialSurgeryId") Long majorSurvialSurgeryId, @RequestBody UpdateJson updateJson)
	{
		try
		{
			return protocolService.updateMajorSurvialSurgery(updateJson, protocolId, majorSurvialSurgeryId);
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * PUT method to update the DescriptionOfExperiment
	 * 
	 * @param protocolId
	 * @param descriptionOfExperimentId
	 * @param userInputsId
	 * @param {@link UpdateJson}
	 * @return Returns updated {@link DescriptionOfExperiment}
	 */
	@RequestMapping(value = "/{protocolId}/descriptionOfExperiment/{descriptionOfExperimentId}/userInputs/{userInputsId}", method = RequestMethod.PUT)
	public DescriptionOfExperiment updateDescriptionOfExperiment(@PathVariable("protocolId") Long protocolId,
			@PathVariable("descriptionOfExperimentId") Long descriptionOfExperimentId, @PathVariable("userInputsId") Long userInputsId,
			@RequestBody UpdateJson updateJson)
	{
		return protocolService.updateDescriptionOfExperiment(updateJson, protocolId, descriptionOfExperimentId, userInputsId);
	}

	/**
	 * Method to add/update Comments for a Protocol
	 * 
	 * @param protocolId
	 * @param commentsId
	 * @param {@link UpdateJson}
	 * @return Returns {@link Comment}
	 */
	@RequestMapping(value = "/{protocolId}/comments/{commentsId}", method = RequestMethod.PUT)
	public Comment addComment(@PathVariable("protocolId") Long protocolId, @PathVariable("commentsId") Long commentsId,
			@RequestBody UpdateJson updateJson, @AuthenticationPrincipal CurrentUser activeUser)
	{
		return protocolService.addComment(updateJson, protocolId, commentsId, activeUser.getUser().getId());
	}

	/*	*//**
	 * Method to update/add value for RECORDING PAIN OR DISTRESS CATEGORY
	 * 
	 * @param protocolId
	 * @param recordPainDistressCategoryId
	 * @param numberOfAnimalsEachYearId
	 * @param {@link UpdateJson}
	 * @return Returns updated {@link RecordPainDistressCategory}
	 */
	/*
	 * @RequestMapping(value =
	 * "/{protocolId}/recordPainDistressCategory/{recordPainDistressCategoryId}/numberOfAnimalsEachYear/{numberOfAnimalsEachYearId}", method =
	 * RequestMethod.PUT) public RecordPainDistressCategory updateRecordPainDistressCategory(
	 * 
	 * @PathVariable("protocolId") Long protocolId,
	 * 
	 * @PathVariable("recordPainDistressCategoryId") Long recordPainDistressCategoryId,
	 * 
	 * @PathVariable("numberOfAnimalsEachYearId") Long numberOfAnimalsEachYearId,
	 * 
	 * @RequestBody UpdateJson updateJson) { return protocolService.updateRecordPainDistressCategory(updateJson, protocolId,
	 * recordPainDistressCategoryId, numberOfAnimalsEachYearId); }
	 */

	/**
	 * updateSpecialRequirementOfStudy method updates the SpecialRequirementOfStudy entity for given id
	 * 
	 * @param protocolId
	 * @param specialRequirementOfStudyId
	 * @param userinputId
	 * @param updateJson
	 * @return
	 */
	@RequestMapping(value = "/{protocolid}/specialrequirementofstudy/{specialrequirementofstudyid}/userinput/{userinputid}", method = RequestMethod.PUT)
	public SpecialRequirementOfStudy updateSpecialRequirementOfStudyUserInput(@PathVariable("protocolid") Long protocolId,
			@PathVariable("specialrequirementofstudyid") Long specialRequirementOfStudyId, @PathVariable("userinputid") Long userinputId,
			@RequestBody UpdateJson updateJson)
	{
		return protocolService.updateSpecialRequirementOfStudy(updateJson, protocolId, specialRequirementOfStudyId, userinputId);
	}

	@RequestMapping(value = "/{protocolid}/specialrequirementofstudy/{specialrequirementofstudyid}", method = RequestMethod.PUT)
	public SpecialRequirementOfStudy updateSpecialRequirementOfStudy(@PathVariable("protocolid") Long protocolId,
			@PathVariable("specialrequirementofstudyid") Long specialRequirementOfStudyId, @RequestBody UpdateJson updateJson)
	{
		try
		{
			return protocolService.updateSpecialRequirementOfStudy(updateJson, protocolId, specialRequirementOfStudyId);
		} catch (Exception e)
		{

			e.printStackTrace();
		}

		return null;
	}

	/**
	 * updateAnesthesiaMethod updates the AnesthesiaMethod entity for given id
	 * 
	 * @param protocolId
	 * @param anesthesiamethodId
	 * @param userinputId
	 * @param updateJson
	 * @return
	 */
	@RequestMapping(value = "/{protocolid}/anesthesiamethod/{anesthesiamethodid}/userinput/{userinputid}", method = RequestMethod.PUT)
	public AnesthesiaMethod updateAnesthesiaMethod(@PathVariable("protocolid") Long protocolId,
			@PathVariable("anesthesiamethodid") Long anesthesiamethodId, @PathVariable("userinputid") Long userinputId,
			@RequestBody UpdateJson updateJson)
	{
		return protocolService.updateAnesthesiaMethod(updateJson, protocolId, anesthesiamethodId, userinputId);
	}

	@RequestMapping(value = "/{protocolid}/anesthesiamethod/{anesthesiamethodid}", method = RequestMethod.PUT)
	public AnesthesiaMethod updateAnesthesiaMethodValue(@PathVariable("protocolid") Long protocolId,
			@PathVariable("anesthesiamethodid") Long anesthesiamethodId, @RequestBody UpdateJson updateJson)
	{
		try
		{
			return protocolService.updateAnesthesiaMethod(updateJson, protocolId, anesthesiamethodId);
		} catch (Exception e)
		{

			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping(value = "/{protocolid}/euthanasiamethod/{euthanasiamethodid}/userinput/{userinputid}", method = RequestMethod.PUT)
	public EuthanasiaMethod updateEuthanasiaMethodUserInput(@PathVariable("protocolid") Long protocolId,
			@PathVariable("euthanasiamethodid") Long euthanasiamethodId, @PathVariable("userinputid") Long userinputId,
			@RequestBody UpdateJson updateJson)
	{
		return protocolService.updateEuthanasiaMethodUserInput(updateJson, protocolId, euthanasiamethodId, userinputId);
	}

	@RequestMapping(value = "/{protocolid}/euthanasiamethod/{euthanasiamethodid}", method = RequestMethod.PUT)
	public EuthanasiaMethod updateEuthanasiaMethod(@PathVariable("protocolid") Long protocolId,
			@PathVariable("euthanasiamethodid") Long euthanasiamethodId, @RequestBody UpdateJson updateJson)
	{
		try
		{
			return protocolService.updateEuthanasiaMethod(updateJson, protocolId, euthanasiamethodId);
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 
	 * @param protocolId
	 * @param recordingPainOrDistressCategoryId
	 * @param usdacolumnid
	 * @param updateJson
	 * @return
	 */
	@RequestMapping(value = "/{protocolid}/recordpainordistresscategory/{recordpainordistresscategoryid}/usdacolumn/{usdacolumnid}", method = RequestMethod.PUT)
	public RecordPainDistressCategory updateRecordingPainOrDistressCategoryUsdaColumn(@PathVariable("protocolid") Long protocolId,
			@PathVariable("recordpainordistresscategoryid") Long recordPainOrDistressCategoryId, @PathVariable("usdacolumnid") Long usdacolumnid,
			@RequestBody UpdateJson updateJson)
	{
		try
		{
			return protocolService.updateRecordPainOrDistressCategory(updateJson, protocolId, recordPainOrDistressCategoryId, usdacolumnid);
		} catch (Exception e)
		{

			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 
	 * @param protocolId
	 * @param recordingPainOrDistressCategoryId
	 * @param userInputId
	 * @param updateJson
	 * @return
	 */
	@RequestMapping(value = "/{protocolid}/recordpainordistresscategory/{recordpainordistresscategoryid}/userinput/{userinputid}", method = RequestMethod.PUT)
	public RecordPainDistressCategory updateRecordingPainOrDistressCategoryUserInput(@PathVariable("protocolid") Long protocolId,
			@PathVariable("recordpainordistresscategoryid") Long recordPainOrDistressCategoryId, @PathVariable("userinputid") Long userInputId,
			@RequestBody UpdateJson updateJson)
	{
		try
		{
			return protocolService.updateRecordPainOrDistressCategoryUserInput(updateJson, protocolId, recordPainOrDistressCategoryId, userInputId);
		} catch (Exception e)
		{

			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 
	 * @param protocolId
	 * @param PICertificationId
	 * @param updateJson
	 * @return
	 */
	@RequestMapping(value = "/{protocolid}/picertification/{picertificationid}", method = RequestMethod.PUT)
	public PICertification updatePICertification(@PathVariable("protocolid") Long protocolId,
			@PathVariable("picertificationid") Long PICertificationId, @RequestBody UpdateJson updateJson)
	{

		try
		{
			return protocolService.updatePICertification(updateJson, protocolId, PICertificationId);
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 
	 * @param protocolId
	 * @param PICertificationId
	 * @param updateJson
	 * @return
	 */
	@RequestMapping(value = "/{protocolid}/picertification/{picertificationid}/userinput/{userinputid}", method = RequestMethod.PUT)
	public PICertification updatePICertificationUserInputs(@PathVariable("protocolid") Long protocolId,
			@PathVariable("picertificationid") Long PICertificationId, @PathVariable("userinputid") Long userInputId,
			@RequestBody UpdateJson updateJson)
	{

		try
		{
			return protocolService.updatePICertification(updateJson, protocolId, PICertificationId, userInputId);
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 
	 * @param protocolId
	 * @param bioAnimalMaterialId
	 * @param bioMaterialsUsedId
	 * @param updateJson
	 * @return
	 */
	@RequestMapping(value = "/{protocolid}/bioanimalmaterial/{bioanimalmaterialid}/biomaterialsused/{biomaterialsusedid}", method = RequestMethod.PUT)
	public BioAnimalMaterial updateBioAnimalMaterialBiomaterialsUsed(@PathVariable("protocolid") Long protocolId,
			@PathVariable("bioanimalmaterialid") Long bioAnimalMaterialId, @PathVariable("biomaterialsusedid") Long bioMaterialsUsedId,
			@RequestBody UpdateJson updateJson)
	{
		try
		{
			return protocolService.updateBioAnimalMaterialBiomaterialsUsed(updateJson, protocolId, bioAnimalMaterialId, bioMaterialsUsedId);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param protocolId
	 * @param bioAnimalMaterialId
	 * @param updateJson
	 * @return
	 */
	@RequestMapping(value = "/{protocolid}/bioanimalmaterial/{bioanimalmaterialid}", method = RequestMethod.PUT)
	public BioAnimalMaterial updateBioAnimalMaterial(@PathVariable("protocolid") Long protocolId,
			@PathVariable("bioanimalmaterialid") Long bioAnimalMaterialId, @RequestBody UpdateJson updateJson)
	{
		try
		{
			return protocolService.updateBioAnimalMaterial(updateJson, protocolId, bioAnimalMaterialId);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * updateHazardousAgents method update the HazardousAgents entity
	 * 
	 * @param protocolId
	 * @param hazardousAgentsId
	 * @param userInputId
	 * @param updateJson
	 * @return
	 */
	@RequestMapping(value = "/{protocolid}/hazardousagents/{hazardousagentsid}/userinput/{userinputid}", method = RequestMethod.PUT)
	public HazardousAgents updateHazardousAgentsUserInputs(@PathVariable("protocolid") Long protocolId,
			@PathVariable("hazardousagentsid") Long hazardousAgentsId, @PathVariable("userinputid") Long userInputId,
			@RequestBody UpdateJson updateJson)
	{

		try
		{
			return protocolService.updateHazardousAgents(updateJson, protocolId, hazardousAgentsId, userInputId);
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping(value = "/{protocolid}/hazardousagents/{hazardousagentsid}", method = RequestMethod.PUT)
	public HazardousAgents updateHazardousAgents(@PathVariable("protocolid") Long protocolId,
			@PathVariable("hazardousagentsid") Long hazardousAgentsId, @RequestBody UpdateJson updateJson)
	{

		try
		{
			return protocolService.updateHazardousAgents(updateJson, protocolId, hazardousAgentsId);
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping(value = "/{protocolid}/review/{reviewId}", method = RequestMethod.PUT)
	public Review updateReview(@PathVariable("protocolid") Long protocolId, @PathVariable("reviewId") Long reviewId,
			@RequestBody UpdateJson updateJson)
	{

		try
		{
			Review review = protocolService.updateReview(updateJson, protocolId, reviewId);

			if (updateJson.getFieldName().equalsIgnoreCase("isDesignatedMemberReview"))
			{
				if (Boolean.parseBoolean(updateJson.getFieldValue()))
				{
					try
					{

						String[] userEmails = new String[review.getUserNames().size()];

						for (int i = 0; i < review.getUserNames().size(); i++)
						{
							userEmails[i] = review.getUserNames().get(i).getEmail();
						}
						if (userEmails != null && userEmails.length > 0)
						{
							emailUtil.createEmailTemplate(userEmails, protocolId);

						}
					} catch (Exception e)
					{
						logger.error("Error while sending email. Error message : " + e.getMessage());
					}

				}
			}
			return review;
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping(value = "/{protocolid}/review/{reviewId}/user", method = RequestMethod.PUT)
	public Review addReviewUser(@PathVariable("protocolid") Long protocolId, @PathVariable("reviewId") Long reviewId,
			@RequestBody UpdateJson updateJson)
	{

		try
		{
			return protocolService.addReviewUser(updateJson, protocolId, reviewId);
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping(value = "/{protocolid}/piuser/{piuserid}", method = RequestMethod.PUT)
	public Protocol addPIUser(@PathVariable("protocolid") Long protocolId, @PathVariable("piuserid") Long piuserId, @RequestBody UpdateJson updateJson)
	{

		try
		{
			return protocolService.updatePIUser(updateJson, protocolId, piuserId);
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping(value = "/piuser/{piuserid}/status/{status}", method = RequestMethod.GET)
	public ResponseEntity<Object> getProtocolsByUserAndStatus(@PathVariable("piuserid") Long piuserId, @PathVariable("status") String status)
	{
		try
		{
			List<ProtocolDTO> protocols = protocolService.getProtocolsByUserAndStatus(piuserId, status);
			return new ResponseEntity<Object>(protocols, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

}
