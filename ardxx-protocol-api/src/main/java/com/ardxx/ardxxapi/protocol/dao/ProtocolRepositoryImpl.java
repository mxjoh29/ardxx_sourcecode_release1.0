package com.ardxx.ardxxapi.protocol.dao;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.stereotype.Repository;

import com.ardxx.ardxxapi.common.ARDXXUtils;

@Repository
public class ProtocolRepositoryImpl implements ProtocolRepositoryCustom
{

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional(value = TxType.REQUIRED)
	public void updateEntity(String field, Object value, long id, String table)
	{

		StringBuilder updateQuery = new StringBuilder(ARDXXUtils.SQL_UPDATE).append(ARDXXUtils.EMPTY_STRING).append(table)
				.append(ARDXXUtils.EMPTY_STRING).append(ARDXXUtils.SQL_SET).append(ARDXXUtils.EMPTY_STRING).append(field)
				.append(ARDXXUtils.SQL_EQUAL);

		if (value instanceof Boolean)
		{
			boolean booleanValue = (Boolean) value;
			updateQuery.append(booleanValue);
		} else if (value instanceof Integer)
		{
			int intValue = (Integer) value;
			updateQuery.append(intValue);
		} else if (value instanceof Date)
		{
			Date dateValue = (Date) value;
			updateQuery.append("'" + dateValue + "'");
		} else
		{
			updateQuery.append(ARDXXUtils.SQL_SINGLE_QUATE)
					.append(value.toString().replace(ARDXXUtils.SQL_SINGLE_QUATE, ARDXXUtils.SQL_SINGLE_QUATE_REPLACE))
					.append(ARDXXUtils.SQL_SINGLE_QUATE);
		}
		updateQuery.append(ARDXXUtils.EMPTY_STRING).append(ARDXXUtils.SQL_WHERE).append(ARDXXUtils.EMPTY_STRING).append("id")
				.append(ARDXXUtils.SQL_EQUAL).append(id);
		System.out.println(">>>>>>>>>>>>>>>>>>>." + updateQuery.toString());
		this.entityManager.createQuery(updateQuery.toString()).executeUpdate();
	}

	@Override
	@Transactional(value = TxType.REQUIRED)
	public Object getEntity(long id, String table)
	{
		StringBuilder selectQuery = new StringBuilder(ARDXXUtils.SQL_FROM).append(ARDXXUtils.EMPTY_STRING).append(table)
				.append(ARDXXUtils.EMPTY_STRING).append(ARDXXUtils.SQL_WHERE).append(ARDXXUtils.EMPTY_STRING).append("id")
				.append(ARDXXUtils.SQL_EQUAL).append(id);

		return this.entityManager.createQuery(selectQuery.toString()).getSingleResult();

	}

}
