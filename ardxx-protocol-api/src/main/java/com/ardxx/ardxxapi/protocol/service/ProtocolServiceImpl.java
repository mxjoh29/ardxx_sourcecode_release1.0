package com.ardxx.ardxxapi.protocol.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ardxx.ardxxapi.common.Messages;
import com.ardxx.ardxxapi.exception.ARDXXAppException;
import com.ardxx.ardxxapi.protocol.controller.validator.ProtocolValidator;
import com.ardxx.ardxxapi.protocol.dao.AnimalRequirementsRepository;
import com.ardxx.ardxxapi.protocol.dao.BioMaterialDataRepository;
import com.ardxx.ardxxapi.protocol.dao.CommentRepository;
import com.ardxx.ardxxapi.protocol.dao.ProtocolAdministrativeDataRepository;
import com.ardxx.ardxxapi.protocol.dao.ProtocolRepository;
import com.ardxx.ardxxapi.protocol.dao.UserInputRepository;
import com.ardxx.ardxxapi.protocol.domain.AnesthesiaMethod;
import com.ardxx.ardxxapi.protocol.domain.AnimalRequirements;
import com.ardxx.ardxxapi.protocol.domain.BioAnimalMaterial;
import com.ardxx.ardxxapi.protocol.domain.BioMaterialData;
import com.ardxx.ardxxapi.protocol.domain.Comment;
import com.ardxx.ardxxapi.protocol.domain.DescriptionOfExperiment;
import com.ardxx.ardxxapi.protocol.domain.EuthanasiaMethod;
import com.ardxx.ardxxapi.protocol.domain.HazardousAgents;
import com.ardxx.ardxxapi.protocol.domain.MajorSurvialSurgery;
import com.ardxx.ardxxapi.protocol.domain.NumberOfAnimals;
import com.ardxx.ardxxapi.protocol.domain.NumberOfAnimalsEachYear;
import com.ardxx.ardxxapi.protocol.domain.PICertification;
import com.ardxx.ardxxapi.protocol.domain.Protocol;
import com.ardxx.ardxxapi.protocol.domain.ProtocolAdministrativeData;
import com.ardxx.ardxxapi.protocol.domain.ProtocolStatus;
import com.ardxx.ardxxapi.protocol.domain.RationalUseOfAnimals;
import com.ardxx.ardxxapi.protocol.domain.RecordPainDistressCategory;
import com.ardxx.ardxxapi.protocol.domain.Review;
import com.ardxx.ardxxapi.protocol.domain.SpecialRequirementOfStudy;
import com.ardxx.ardxxapi.protocol.domain.StudyObjective;
import com.ardxx.ardxxapi.protocol.domain.Transportation;
import com.ardxx.ardxxapi.protocol.domain.UserInput;
import com.ardxx.ardxxapi.protocol.dto.ProtocolDTO;
import com.ardxx.ardxxapi.protocol.dto.UpdateJson;
import com.tn.security.basic.domain.User;
import com.tn.security.basic.repository.UserRepository;
import com.tn.security.exception.ErrorPayload;
import com.tn.security.service.user.UserService;

/**
 * @author naikraj
 *
 */
@Service
public class ProtocolServiceImpl implements ProtocolService
{

	@Autowired
	ProtocolRepository protocolRepository;

	@Autowired
	UserInputRepository userInputRepository;

	@Autowired
	AnimalRequirementsRepository animalRequirementsRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	ProtocolAdministrativeDataRepository protocolAdministrativeDataRepository;

	@Autowired
	CommentRepository commentRepository;

	@Autowired
	BioMaterialDataRepository bioMaterialDataRepository;

	@Override
	public Protocol createProtocol(UpdateJson updateJson)
	{

		Protocol protocol = new Protocol();

		protocol.setAnesthesiaMethod(new AnesthesiaMethod());
		AnimalRequirements animalRequirements = new AnimalRequirements();
		animalRequirements.setNumberOfAnimals(new NumberOfAnimals());
		animalRequirements = animalRequirementsRepository.save(animalRequirements);
		Set<AnimalRequirements> animalRequirementsList = new HashSet<AnimalRequirements>();
		animalRequirementsList.add(animalRequirements);
		protocol.setAnimalRequirements(animalRequirementsList);
		protocol.setBioAnimalMaterial(new BioAnimalMaterial());
		protocol.setDescriptionOfExperiment(new DescriptionOfExperiment());
		protocol.setEuthanasiaMethod(new EuthanasiaMethod());
		protocol.setHazardousAgents(new HazardousAgents());
		protocol.setMajorSurvialSurgery(new MajorSurvialSurgery());
		protocol.setpICertification(new PICertification());

		protocol.setRationalUseOfAnimals(new RationalUseOfAnimals());

		RecordPainDistressCategory recordPainDistressCategory = new RecordPainDistressCategory();
		recordPainDistressCategory.setUSDAColumnB(new NumberOfAnimalsEachYear());
		recordPainDistressCategory.setUSDAColumnC(new NumberOfAnimalsEachYear());
		recordPainDistressCategory.setUSDAColumnD(new NumberOfAnimalsEachYear());
		recordPainDistressCategory.setUSDAColumnE(new NumberOfAnimalsEachYear());

		protocol.setRecordPainDistressCategory(recordPainDistressCategory);
		protocol.setSpecialRequirementOfStudy(new SpecialRequirementOfStudy());
		protocol.setStudyObjective(new StudyObjective());
		protocol.setTransportation(new Transportation());
		protocol.setReview(new Review());
		protocol.setCreatedDate(new Date());

		User user = userRepository.findOne(Long.valueOf(updateJson.getUserId()));
		protocol.setCreatedBy(user);
		protocol.setPiUser(user);

		ProtocolAdministrativeData protocolAdministrativeData = new ProtocolAdministrativeData();
		protocolAdministrativeData.setPrincipalInvestigator(user);
		protocol.setProtocolAdministrativeData(protocolAdministrativeData);

		return protocolRepository.save(protocol);
	}

	@Override
	public Protocol getProtocol(Long id)
	{
		return protocolRepository.findOne(id);
	}

	@Override
	public List<ProtocolDTO> getProtocolList(int page, int size)
	{

		Pageable pageable = new PageRequest(page, size);

		Iterable<Protocol> protocols = protocolRepository.findAll(pageable);
		if (protocols != null)
		{
			ProtocolDTO protocolDTO = null;
			List<ProtocolDTO> protocolDTOs = new ArrayList<ProtocolDTO>();
			for (Protocol protocol : protocols)
			{

				protocolDTO = new ProtocolDTO();
				protocolDTO.setId(protocol.getId());
				protocolDTO.setTitle(protocol.getTitle());
				protocolDTO.setProposalNumber(protocol.getProposalNumber());
				protocolDTO.setApprovalDate(protocol.getApprovalDate());
				protocolDTO.setExpirationDate(protocol.getExpirationDate());
				protocolDTO.setAnnualReviewDueFirstYear(protocol.getAnnualReviewDueFirstYear());
				protocolDTO.setAnnualReviewDueSecondYear(protocol.getAnnualReviewDueSecondYear());
				protocolDTO.setStatus(protocol.getStatus().name());
				protocolDTO.setCreatedDate(protocol.getCreatedDate());
				protocolDTO.setPiUser(protocol.getPiUser());
				protocolDTOs.add(protocolDTO);
			}
			return protocolDTOs;
		} else
		{
			return null;
		}

	}

	@Override
	public List<ProtocolDTO> getProtocolList(Long userId) throws Exception
	{

		Iterable<Protocol> protocols = protocolRepository.findByCreatedById(userId);
		return protocolToDto(protocols);

	}

	private List<ProtocolDTO> protocolToDto(Iterable<Protocol> protocols) throws ARDXXAppException
	{
		if (protocols != null && protocols.iterator().hasNext())
		{
			ProtocolDTO protocolDTO = null;
			List<ProtocolDTO> protocolDTOs = new ArrayList<ProtocolDTO>();
			for (Protocol protocol : protocols)
			{

				protocolDTO = new ProtocolDTO();
				protocolDTO.setId(protocol.getId());
				protocolDTO.setTitle(protocol.getTitle());
				protocolDTO.setProposalNumber(protocol.getProposalNumber());
				protocolDTO.setApprovalDate(protocol.getApprovalDate());
				protocolDTO.setExpirationDate(protocol.getExpirationDate());
				protocolDTO.setAnnualReviewDueFirstYear(protocol.getAnnualReviewDueFirstYear());
				protocolDTO.setAnnualReviewDueSecondYear(protocol.getAnnualReviewDueSecondYear());
				protocolDTO.setStatus(protocol.getStatus().name());
				protocolDTOs.add(protocolDTO);
			}
			return protocolDTOs;
		} else
		{
			// throw new ARDXXAppException(Messages.properties.getProperty("no.protocol"));
			return null;
		}
	}

	@Override
	public List<ProtocolDTO> getProtocolListByPIUser(Long userId) throws Exception
	{

		Iterable<Protocol> protocols = protocolRepository.findByPiUserId(userId);
		return protocolToDto(protocols);

	}

	@Override
	public String deleteProtocol(Long id) throws Exception
	{
		try
		{
			protocolRepository.delete(id);
			return "Successfully deleted protocol :" + id;
		} catch (Exception e)
		{
			// return "Error while deleting protocol :" + id;
			throw new ARDXXAppException(Messages.properties.getProperty("no.protocol"));

		}
	}

	@Override
	public Protocol updateProtocol(UpdateJson updateJson, Long protocolId) throws Exception
	{

		validateProtocol(updateJson, protocolId);

		Class<?> protocolClass = Class.forName(Protocol.class.getName());
		Boolean booleanValue = null;

		if (protocolClass.getDeclaredField(updateJson.getFieldName()).getType().getName().equals(boolean.class.getName()))
		{
			booleanValue = Boolean.valueOf(updateJson.getFieldValue());
			protocolRepository.updateEntity(updateJson.getFieldName(), booleanValue, protocolId, Protocol.class.getSimpleName());
			return (Protocol) protocolRepository.getEntity(protocolId, Protocol.class.getSimpleName());
		} else
		{
			protocolRepository.updateEntity(updateJson.getFieldName(), updateJson.getFieldValue(), protocolId, Protocol.class.getSimpleName());

			if (updateJson.getFieldName().equals("status") && updateJson.getFieldValue().equals(ProtocolStatus.APPROVED.name()))
			{

				Calendar cal = Calendar.getInstance();
				Date approvalDate = cal.getTime();
				cal.add(Calendar.YEAR, 1);
				Date annualReviewDueFirstYear = cal.getTime();
				cal.add(Calendar.YEAR, 1);
				Date annualReviewDueSecondYear = cal.getTime();
				cal.add(Calendar.YEAR, 1);
				Date expirationDate = cal.getTime();

				java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				protocolRepository.updateEntity("approvalDate", sdf.format(approvalDate), protocolId, Protocol.class.getSimpleName());
				protocolRepository.updateEntity("expirationDate", sdf.format(expirationDate), protocolId, Protocol.class.getSimpleName());
				protocolRepository.updateEntity("annualReviewDueFirstYear", sdf.format(annualReviewDueFirstYear), protocolId,
						Protocol.class.getSimpleName());
				protocolRepository.updateEntity("annualReviewDueSecondYear", sdf.format(annualReviewDueSecondYear), protocolId,
						Protocol.class.getSimpleName());
			}

			return (Protocol) protocolRepository.getEntity(protocolId, Protocol.class.getSimpleName());
		}
	}

	private void validateProtocol(UpdateJson updateJson, Long protocolId) throws Exception
	{
		ProtocolValidator protocolValidator = null;
		Protocol protocol = null;
		if (updateJson.getFieldName().equalsIgnoreCase("status"))
		{
			if (updateJson.getFieldValue().equalsIgnoreCase(ProtocolStatus.REVIEW.name())
					|| updateJson.getFieldValue().equalsIgnoreCase(ProtocolStatus.PRE_REVIEW.name()))
			{
				protocolValidator = new ProtocolValidator();
				protocol = protocolRepository.findOne(protocolId);
				List<String> errorCodes = protocolValidator.validateStatusUpdate(protocol);
				if (errorCodes != null && !errorCodes.isEmpty())
				{
					ARDXXAppException ardxxAppException = new ARDXXAppException();
					ardxxAppException.setErrorCodes(errorCodes);
					throw ardxxAppException;
				}

			}

			if (updateJson.getFieldValue().equalsIgnoreCase(ProtocolStatus.APPROVED.name()))
			{
				protocolValidator = new ProtocolValidator();
				protocol = protocolRepository.findOne(protocolId);
				List<String> errorCodes = protocolValidator.validateStatusApproval(protocol);
				if (errorCodes != null && !errorCodes.isEmpty())
				{
					ARDXXAppException ardxxAppException = new ARDXXAppException();
					ardxxAppException.setErrorCodes(errorCodes);
					throw ardxxAppException;
				}
			}
		}
	}

	@Override
	public AnimalRequirements updateAnimalRequirements(UpdateJson updateJson, Long pid, Long aid) throws Exception
	{

		Protocol protocol = null;
		Set<AnimalRequirements> animalRequirementsList = null;
		if (aid == -1)
		{
			protocol = protocolRepository.findOne(pid);

			AnimalRequirements animalRequirements = new AnimalRequirements();
			animalRequirements.setNumberOfAnimals(new NumberOfAnimals());

			animalRequirements = animalRequirementsRepository.save(animalRequirements);

			animalRequirementsList = protocol.getAnimalRequirements();

			animalRequirementsList.add(animalRequirements);

			protocol.setAnimalRequirements(animalRequirementsList);

			protocolRepository.save(protocol);
			return (AnimalRequirements) protocolRepository.getEntity(animalRequirements.getId(), AnimalRequirements.class.getSimpleName());

		} else
		{
			Class<?> animalRequirement = Class.forName(AnimalRequirements.class.getName());
			Boolean booleanValue = null;
			if (animalRequirement.getDeclaredField(updateJson.getFieldName()).getType().getName().equals(boolean.class.getName()))
			{
				booleanValue = Boolean.valueOf(updateJson.getFieldValue());
				protocolRepository.updateEntity(updateJson.getFieldName(), booleanValue, aid, AnimalRequirements.class.getSimpleName());
				return (AnimalRequirements) protocolRepository.getEntity(aid, AnimalRequirements.class.getSimpleName());
			} else
			{
				protocolRepository.updateEntity(updateJson.getFieldName(), updateJson.getFieldValue(), aid, AnimalRequirements.class.getSimpleName());
				return (AnimalRequirements) protocolRepository.getEntity(aid, AnimalRequirements.class.getSimpleName());
			}

		}

	}

	@Override
	public NumberOfAnimals updateNumberOfAnimals(UpdateJson updateJson, Long pid, Long paid, Long opid) throws Exception
	{

		Class<?> numberOfAnimalsClass = Class.forName(NumberOfAnimals.class.getName());
		Boolean booleanValue = null;
		Integer intValue = null;
		if (numberOfAnimalsClass.getDeclaredField(updateJson.getFieldName()).getType().getName().equals(boolean.class.getName()))
		{
			booleanValue = Boolean.valueOf(updateJson.getFieldValue());
			protocolRepository.updateEntity(updateJson.getFieldName(), booleanValue, opid, NumberOfAnimals.class.getSimpleName());
			return (NumberOfAnimals) protocolRepository.getEntity(opid, NumberOfAnimals.class.getSimpleName());
		} else if (numberOfAnimalsClass.getDeclaredField(updateJson.getFieldName()).getType().getName().equals(int.class.getName()))
		{
			intValue = Integer.valueOf(updateJson.getFieldValue());
			protocolRepository.updateEntity(updateJson.getFieldName(), intValue, opid, NumberOfAnimals.class.getSimpleName());
			return (NumberOfAnimals) protocolRepository.getEntity(opid, NumberOfAnimals.class.getSimpleName());
		} else
		{
			protocolRepository.updateEntity(updateJson.getFieldName(), updateJson.getFieldValue(), opid, NumberOfAnimals.class.getSimpleName());
			return (NumberOfAnimals) protocolRepository.getEntity(opid, NumberOfAnimals.class.getSimpleName());
		}

	}

	@Override
	public ProtocolAdministrativeData updateProtocolAdministrativeData(UpdateJson updateJson, Long pid, Long paid) throws Exception
	{

		Class<?> protocolAdministrativeDataClass = Class.forName(ProtocolAdministrativeData.class.getName());
		Boolean booleanValue = null;
		if (protocolAdministrativeDataClass.getDeclaredField(updateJson.getFieldName()).getType().getName().equals(boolean.class.getName()))
		{
			booleanValue = Boolean.valueOf(updateJson.getFieldValue());
			protocolRepository.updateEntity(updateJson.getFieldName(), booleanValue, paid, ProtocolAdministrativeData.class.getSimpleName());
			return (ProtocolAdministrativeData) protocolRepository.getEntity(paid, ProtocolAdministrativeData.class.getSimpleName());
		} else
		{
			protocolRepository.updateEntity(updateJson.getFieldName(), updateJson.getFieldValue(), paid,
					ProtocolAdministrativeData.class.getSimpleName());
			return (ProtocolAdministrativeData) protocolRepository.getEntity(paid, ProtocolAdministrativeData.class.getSimpleName());
		}

	}

	@Override
	public ProtocolAdministrativeData updateAnimalProceduresPersonals(UpdateJson updateJson, Long pid, Long paid, Long aapid)
	{

		ProtocolAdministrativeData protocolAdministrativeData = null;
		Protocol protocol = null;
		User user = null;
		if (aapid == -1)
		{
			protocol = protocolRepository.findByProtocolAdministrativeDataId(paid);

			protocolAdministrativeData = protocol.getProtocolAdministrativeData();

			user = userRepository.findOne(Long.valueOf(updateJson.getFieldValue()));

			// List<User> users = new ArrayList<User>();
			List<User> users = protocolAdministrativeData.getAnimalProceduresPersonals();
			if (users != null && !users.isEmpty())
			{
				users.add(user);
			} else
			{
				users = new ArrayList<User>();
				users.add(user);
			}
			// users.add(user);

			protocolAdministrativeData.setAnimalProceduresPersonals(users);

			protocolAdministrativeDataRepository.save(protocolAdministrativeData);
			return (ProtocolAdministrativeData) protocolRepository.getEntity(paid, ProtocolAdministrativeData.class.getSimpleName());
		}

		else
		{
			protocol = protocolRepository.findByProtocolAdministrativeDataId(paid);

			protocolAdministrativeData = protocol.getProtocolAdministrativeData();

			List<User> users = protocolAdministrativeData.getAnimalProceduresPersonals();
			User userToRemove = null;
			for (User userInList : users)
			{

				if (userInList.getId() == aapid)
				{
					userToRemove = userInList;
				}
			}
			users.remove(userToRemove);
			user = userRepository.findOne(Long.valueOf(updateJson.getFieldValue()));
			users.add(user);
			protocolAdministrativeData.setAnimalProceduresPersonals(users);
			protocolAdministrativeDataRepository.save(protocolAdministrativeData);
			return (ProtocolAdministrativeData) protocolRepository.getEntity(paid, ProtocolAdministrativeData.class.getSimpleName());
		}
	}

	@Override
	public ProtocolAdministrativeData deleteAnimalProceduresPersonals(Long protocolId, Long protocolAdministrativeDataId,
			Long animalProceduresPersonalsId)
	{
		ProtocolAdministrativeData protocolAdministrativeData = null;
		Protocol protocol = null;
		protocol = protocolRepository.findByProtocolAdministrativeDataId(protocolAdministrativeDataId);
		protocolAdministrativeData = protocol.getProtocolAdministrativeData();

		List<User> users = protocolAdministrativeData.getAnimalProceduresPersonals();
		for (User user : users)
		{
			if (user.getId() == animalProceduresPersonalsId)
			{
				users.remove(user);
				break;
			}
		}

		protocolAdministrativeData.setAnimalProceduresPersonals(users);
		protocolAdministrativeDataRepository.save(protocolAdministrativeData);
		return (ProtocolAdministrativeData) protocolRepository.getEntity(protocolAdministrativeDataId,
				ProtocolAdministrativeData.class.getSimpleName());
	}

	@Override
	public ProtocolAdministrativeData updateCoInvestigators(UpdateJson updateJson, Long pid, Long paid, Long coid)
	{

		ProtocolAdministrativeData protocolAdministrativeData = null;
		Protocol protocol = null;
		User user = null;
		if (coid == -1)
		{
			protocol = protocolRepository.findByProtocolAdministrativeDataId(paid);

			protocolAdministrativeData = protocol.getProtocolAdministrativeData();

			user = userRepository.findOne(Long.valueOf(updateJson.getFieldValue()));
			List<User> users = protocolAdministrativeData.getCoInvestigators();
			if (users == null)
			{
				users = new ArrayList<User>();
			}

			users.add(user);

			protocolAdministrativeData.setCoInvestigators(users);

			protocolAdministrativeDataRepository.save(protocolAdministrativeData);
			return (ProtocolAdministrativeData) protocolRepository.getEntity(paid, ProtocolAdministrativeData.class.getSimpleName());
		}

		else
		{
			protocol = protocolRepository.findByProtocolAdministrativeDataId(paid);

			protocolAdministrativeData = protocol.getProtocolAdministrativeData();

			List<User> users = protocolAdministrativeData.getCoInvestigators();
			User userToRemove = null;
			for (User userInList : users)
			{

				if (userInList.getId() == coid)
				{
					userToRemove = userInList;
				}
			}
			users.remove(userToRemove);
			user = userRepository.findOne(Long.valueOf(updateJson.getFieldValue()));
			users.add(user);
			protocolAdministrativeData.setCoInvestigators(users);
			protocolAdministrativeDataRepository.save(protocolAdministrativeData);
			return (ProtocolAdministrativeData) protocolRepository.getEntity(paid, ProtocolAdministrativeData.class.getSimpleName());
		}

	}

	@Override
	public ProtocolAdministrativeData updateOtherPersonals(UpdateJson updateJson, Long pid, Long paid, Long opid)
	{

		ProtocolAdministrativeData protocolAdministrativeData = null;
		Protocol protocol = null;
		User user = null;
		if (opid == -1)
		{
			protocol = protocolRepository.findByProtocolAdministrativeDataId(paid);

			protocolAdministrativeData = protocol.getProtocolAdministrativeData();

			user = userRepository.findOne(Long.valueOf(updateJson.getFieldValue()));

			// List<User> users = new ArrayList<User>();
			List<User> users = protocolAdministrativeData.getOtherPersonals();
			if (users == null)
			{
				users = new ArrayList<User>();
			}

			users.add(user);

			protocolAdministrativeData.setOtherPersonals(users);

			protocolAdministrativeDataRepository.save(protocolAdministrativeData);
			return (ProtocolAdministrativeData) protocolRepository.getEntity(paid, ProtocolAdministrativeData.class.getSimpleName());
		} else
		{
			protocol = protocolRepository.findByProtocolAdministrativeDataId(paid);

			protocolAdministrativeData = protocol.getProtocolAdministrativeData();

			List<User> users = protocolAdministrativeData.getOtherPersonals();
			User userToRemove = null;
			for (User userInList : users)
			{

				if (userInList.getId() == opid)
				{
					userToRemove = userInList;
				}
			}
			users.remove(userToRemove);
			user = userRepository.findOne(Long.valueOf(updateJson.getFieldValue()));
			users.add(user);
			protocolAdministrativeData.setOtherPersonals(users);
			protocolAdministrativeDataRepository.save(protocolAdministrativeData);
			return (ProtocolAdministrativeData) protocolRepository.getEntity(paid, ProtocolAdministrativeData.class.getSimpleName());

		}

	}

	@Override
	public ProtocolAdministrativeData deleteOtherPersonals(Long protocolId, Long protocolAdministrativeDataId, Long otherPersonalsId)
	{

		ProtocolAdministrativeData protocolAdministrativeData = null;
		Protocol protocol = null;
		protocol = protocolRepository.findByProtocolAdministrativeDataId(protocolAdministrativeDataId);
		protocolAdministrativeData = protocol.getProtocolAdministrativeData();

		List<User> users = protocolAdministrativeData.getOtherPersonals();
		for (User user : users)
		{
			if (user.getId() == otherPersonalsId)
			{
				users.remove(user);
				break;
			}
		}

		protocolAdministrativeData.setOtherPersonals(users);
		protocolAdministrativeDataRepository.save(protocolAdministrativeData);
		return (ProtocolAdministrativeData) protocolRepository.getEntity(protocolAdministrativeDataId,
				ProtocolAdministrativeData.class.getSimpleName());
	}

	@Override
	public Transportation updateTransportation(UpdateJson updateJson, Long pid, Long tid)
	{
		protocolRepository.updateEntity(updateJson.getFieldName(), updateJson.getFieldValue(), tid, Transportation.class.getSimpleName());
		return (Transportation) protocolRepository.getEntity(tid, Transportation.class.getSimpleName());
	}

	@Override
	public StudyObjective updateStudyObjective(UpdateJson updateJson, Long pid, Long sid, Long uid) throws Exception
	{
		UserInput userInput = null;
		Protocol protocol = null;
		ProtocolValidator protocolValidator = new ProtocolValidator();
		List<String> errorCodes = protocolValidator.validateStudyObjective(updateJson);

		if (errorCodes != null && !errorCodes.isEmpty())
		{
			ARDXXAppException ardxxAppException = new ARDXXAppException();
			ardxxAppException.setErrorCodes(errorCodes);
			throw ardxxAppException;

		}
		if (uid == -1)
		{
			protocol = protocolRepository.findByStudyObjectiveId(sid);
			userInput = setUserInputValues(new UserInput(), updateJson);

			userInput = userInputRepository.save(userInput);

			List<UserInput> userInputs = protocol.getStudyObjective().getUserInputs();
			if (userInputs == null)
			{
				userInputs = new ArrayList<UserInput>();
			}

			userInputs = setUserInputList(userInputs, userInput);

			protocol.getStudyObjective().setUserInputs(userInputs);

			protocolRepository.save(protocol);
			return (StudyObjective) protocolRepository.getEntity(sid, StudyObjective.class.getSimpleName());
		} else
		{
			userInput = userInputRepository.findOne(uid);
			userInput = setUserInputValues(userInput, updateJson);
			userInputRepository.save(userInput);
			return (StudyObjective) protocolRepository.getEntity(sid, StudyObjective.class.getSimpleName());
		}
	}

	@Override
	public RationalUseOfAnimals updateRationalForUseOfAnimals(UpdateJson updateJson, Long rid, Long sid, Long uid)
	{
		UserInput userInput = null;
		Protocol protocol = null;
		if (uid == -1)
		{
			protocol = protocolRepository.findByRationalUseOfAnimalsId(rid);

			userInput = setUserInputValues(new UserInput(), updateJson);

			userInput = userInputRepository.save(userInput);

			List<UserInput> userInputs = protocol.getRationalUseOfAnimals().getUserInputs();
			if (userInputs == null)
			{
				userInputs = new ArrayList<UserInput>();
			}

			userInputs = setUserInputList(userInputs, userInput);

			protocol.getRationalUseOfAnimals().setUserInputs(userInputs);

			protocolRepository.save(protocol);
			return (RationalUseOfAnimals) protocolRepository.getEntity(rid, RationalUseOfAnimals.class.getSimpleName());

		} else
		{
			userInput = userInputRepository.findOne(uid);
			userInput = setUserInputValues(userInput, updateJson);
			userInputRepository.save(userInput);
			return (RationalUseOfAnimals) protocolRepository.getEntity(rid, RationalUseOfAnimals.class.getSimpleName());
		}

	}

	@Override
	public MajorSurvialSurgery updateMajorSurvialSurgery(UpdateJson updateJson, Long pid, Long mid, Long uid)
	{
		UserInput userInput = null;
		Protocol protocol = null;
		if (uid == -1)
		{
			protocol = protocolRepository.findByMajorSurvialSurgeryId(mid);

			userInput = setUserInputValues(new UserInput(), updateJson);

			userInput = userInputRepository.save(userInput);

			List<UserInput> userInputs = protocol.getMajorSurvialSurgery().getUserInputs();
			if (userInputs == null)
			{
				userInputs = new ArrayList<UserInput>();
			}

			userInputs = setUserInputList(userInputs, userInput);

			protocol.getMajorSurvialSurgery().setUserInputs(userInputs);

			protocolRepository.save(protocol);
			return (MajorSurvialSurgery) protocolRepository.getEntity(mid, MajorSurvialSurgery.class.getSimpleName());

		} else
		{
			userInput = userInputRepository.findOne(uid);
			userInput = setUserInputValues(userInput, updateJson);
			userInputRepository.save(userInput);
			return (MajorSurvialSurgery) protocolRepository.getEntity(mid, MajorSurvialSurgery.class.getSimpleName());

		}

	}

	@Override
	public DescriptionOfExperiment updateDescriptionOfExperiment(UpdateJson updateJson, Long pid, Long did, Long uid)
	{
		UserInput userInput = null;
		Protocol protocol = null;
		if (uid == -1)
		{
			protocol = protocolRepository.findByDescriptionOfExperimentId(did);

			userInput = setUserInputValues(new UserInput(), updateJson);

			userInput = userInputRepository.save(userInput);

			List<UserInput> userInputs = protocol.getDescriptionOfExperiment().getUserInputs();
			if (userInputs == null)
			{
				userInputs = new ArrayList<UserInput>();
			}

			userInputs = setUserInputList(userInputs, userInput);

			protocol.getDescriptionOfExperiment().setUserInputs(userInputs);

			protocolRepository.save(protocol);
			return (DescriptionOfExperiment) protocolRepository.getEntity(did, DescriptionOfExperiment.class.getSimpleName());

		} else
		{
			userInput = userInputRepository.findOne(uid);
			userInput = setUserInputValues(userInput, updateJson);
			userInputRepository.save(userInput);
			return (DescriptionOfExperiment) protocolRepository.getEntity(did, DescriptionOfExperiment.class.getSimpleName());
		}

	}

	private UserInput setUserInputValues(UserInput userInput, UpdateJson updateJson)
	{
		userInput.setLabel(updateJson.getFieldName());
		userInput.setValue(updateJson.getFieldValue());
		return userInput;
	}

	private List<UserInput> setUserInputList(List<UserInput> userInputList, UserInput... userInputs)
	{

		for (UserInput userInput : userInputs)
		{
			userInputList.add(userInput);
		}

		return userInputList;
	}

	@Override
	public Comment addComment(UpdateJson updateJson, Long pid, Long cid, Long userId)
	{

		Protocol protocol = protocolRepository.findOne(pid);
		Comment comment = null;
		List<Comment> comments = null;
		User user = userRepository.findOne(userId);
		if (cid == -1)
		{
			comment = new Comment();
			comment.setAddedDate(new Date());
			comment.setModifiedDate(new Date());
			comment.setComment(updateJson.getFieldValue());
			comment.setAddedBy(user);
			comment = commentRepository.save(comment);

			comments = protocol.getComments();
			if (comments == null || (comments != null && comments.isEmpty()))
			{
				comments = new ArrayList<Comment>();
				comments.add(comment);
			} else
			{
				comments.add(comment);
			}

			protocol.setComments(comments);

			protocolRepository.save(protocol);
			return (Comment) protocolRepository.getEntity(comment.getId(), Comment.class.getSimpleName());
		} else
		{
			comment = commentRepository.findOne(cid);
			comment.setComment(updateJson.getFieldValue());
			comment.setModifiedDate(new Date());
			comment.setAddedBy(user);
			commentRepository.save(comment);
			return (Comment) protocolRepository.getEntity(cid, Comment.class.getSimpleName());
		}

	}

	// TODO: Method not used : Remove
	@Override
	public RecordPainDistressCategory updateRecordPainDistressCategory(UpdateJson updateJson, Long protocolId, Long recordPainDistressCategoryId,
			Long numberOfAnimalsEachYearId)
	{

		Protocol protocol = null;
		RecordPainDistressCategory recordPainDistressCategory = null;

		if (numberOfAnimalsEachYearId == -1)
		{
			protocol = protocolRepository.findOne(protocolId);
			recordPainDistressCategory = protocol.getRecordPainDistressCategory();
			// recordPainDistressCategory.get
		}
		return null;
	}

	@Override
	public SpecialRequirementOfStudy updateSpecialRequirementOfStudy(UpdateJson updateJson, Long protocolId, Long specialRequirementOfStudyId,
			Long userinputsId)
	{

		Protocol protocol = protocolRepository.findOne(protocolId);

		SpecialRequirementOfStudy specialRequirementOfStudy = protocol.getSpecialRequirementOfStudy();
		UserInput userInput = null;
		if (userinputsId == -1)
		{
			userInput = new UserInput();
		} else
		{
			userInput = specialRequirementOfStudy.getSpecialRequirements();
		}

		userInput = setUserInputValues(userInput, updateJson);
		specialRequirementOfStudy.setSpecialRequirements(userInput);
		protocol.setSpecialRequirementOfStudy(specialRequirementOfStudy);
		protocolRepository.save(protocol);

		return (SpecialRequirementOfStudy) protocolRepository.getEntity(specialRequirementOfStudyId, SpecialRequirementOfStudy.class.getSimpleName());

	}

	@Override
	public AnesthesiaMethod updateAnesthesiaMethod(UpdateJson updateJson, Long protocolId, Long anesthesiamethodId, Long userinputId)
	{

		Protocol protocol = protocolRepository.findOne(protocolId);
		AnesthesiaMethod anesthesiaMethod = protocol.getAnesthesiaMethod();
		UserInput userInput = null;
		if (userinputId == -1)
		{
			userInput = new UserInput();
		} else
		{
			userInput = anesthesiaMethod.getAnesthesiaMethodDesc();
		}
		userInput = setUserInputValues(userInput, updateJson);
		anesthesiaMethod.setAnesthesiaMethodDesc(userInput);
		protocol.setAnesthesiaMethod(anesthesiaMethod);
		protocolRepository.save(protocol);

		return (AnesthesiaMethod) protocolRepository.getEntity(anesthesiamethodId, AnesthesiaMethod.class.getSimpleName());
	}

	@Override
	public RecordPainDistressCategory updateRecordPainOrDistressCategory(UpdateJson updateJson, Long protocolId, Long recordPainOrDistressCategoryId,
			Long usdacolumnbId) throws Exception
	{

		Class<?> numberOfAnimalsEachYearClass = Class.forName(NumberOfAnimalsEachYear.class.getName());
		Integer integerValue = null;
		if (numberOfAnimalsEachYearClass.getDeclaredField(updateJson.getFieldName()).getType().getName().equals(int.class.getName()))
		{
			integerValue = Integer.valueOf(updateJson.getFieldValue());
			protocolRepository.updateEntity(updateJson.getFieldName(), integerValue, usdacolumnbId, NumberOfAnimalsEachYear.class.getSimpleName());

		} else
		{
			protocolRepository.updateEntity(updateJson.getFieldName(), updateJson.getFieldValue(), protocolId, Protocol.class.getSimpleName());
		}

		return (RecordPainDistressCategory) protocolRepository.getEntity(recordPainOrDistressCategoryId,
				RecordPainDistressCategory.class.getSimpleName());

	}

	@Override
	public RecordPainDistressCategory updateRecordPainOrDistressCategoryUserInput(UpdateJson updateJson, Long protocolId,
			Long recordPainOrDistressCategoryId, Long userInputId)
	{

		Protocol protocol = protocolRepository.findOne(protocolId);
		RecordPainDistressCategory recordPainDistressCategory = protocol.getRecordPainDistressCategory();
		UserInput userInput = null;
		if (userInputId == -1)
		{
			userInput = new UserInput();
		} else
		{
			userInput = recordPainDistressCategory.getNarativeOfProcedures();
		}

		userInput = setUserInputValues(userInput, updateJson);
		recordPainDistressCategory.setNarativeOfProcedures(userInput);
		protocol.setRecordPainDistressCategory(recordPainDistressCategory);
		protocolRepository.save(protocol);

		return (RecordPainDistressCategory) protocolRepository.getEntity(recordPainOrDistressCategoryId,
				RecordPainDistressCategory.class.getSimpleName());
	}

	@Override
	public PICertification updatePICertification(UpdateJson updateJson, Long protocolId, Long pICertificationId) throws Exception
	{

		Class<?> PICertificationClass = Class.forName(PICertification.class.getName());
		Boolean booleanValue = null;
		if (PICertificationClass.getDeclaredField(updateJson.getFieldName()).getType().getName().equals(boolean.class.getName()))
		{
			booleanValue = Boolean.valueOf(updateJson.getFieldValue());
			protocolRepository.updateEntity(updateJson.getFieldName(), booleanValue, pICertificationId, Protocol.class.getSimpleName());
			return (PICertification) protocolRepository.getEntity(pICertificationId, PICertification.class.getSimpleName());
		} else
		{
			protocolRepository.updateEntity(updateJson.getFieldName(), updateJson.getFieldValue(), pICertificationId, Protocol.class.getSimpleName());
			return (PICertification) protocolRepository.getEntity(pICertificationId, PICertification.class.getSimpleName());
		}

	}

	@Override
	public PICertification updatePICertification(UpdateJson updateJson, Long protocolId, Long pICertificationId, Long userInputId)
	{

		UserInput userInput = null;
		Protocol protocol = null;

		if (userInputId == -1)
		{
			protocol = protocolRepository.findByPICertificationId(pICertificationId);
			userInput = setUserInputValues(new UserInput(), updateJson);
			userInput = userInputRepository.save(userInput);
			List<UserInput> userInputs = protocol.getpICertification().getUserInputs();
			if (userInputs == null)
			{
				userInputs = new ArrayList<UserInput>();
			}

			userInputs = setUserInputList(userInputs, userInput);

			protocol.getpICertification().setUserInputs(userInputs);

			protocolRepository.save(protocol);
			return (PICertification) protocolRepository.getEntity(pICertificationId, PICertification.class.getSimpleName());
		} else
		{
			userInput = userInputRepository.findOne(protocolId);
			userInput = setUserInputValues(userInput, updateJson);
			userInputRepository.save(userInput);
			return (PICertification) protocolRepository.getEntity(pICertificationId, PICertification.class.getSimpleName());
		}
	}

	@Override
	public BioAnimalMaterial updateBioAnimalMaterialBiomaterialsUsed(UpdateJson updateJson, Long protocolId, Long bioAnimalMaterialId,
			Long bioMaterialsUsedId) throws Exception
	{

		Protocol protocol = null;
		BioAnimalMaterial bioAnimalMaterial = null;
		BioMaterialData bioMaterialData = null;
		List<BioMaterialData> materialDatas = null;
		if (bioMaterialsUsedId == -1)
		{
			protocol = protocolRepository.findOne(protocolId);
			bioAnimalMaterial = protocol.getBioAnimalMaterial();
			materialDatas = bioAnimalMaterial.getBioMaterialsUsed();
			if (materialDatas == null)
			{
				materialDatas = new ArrayList<BioMaterialData>();
			}
			bioMaterialData = new BioMaterialData();

			bioMaterialData = bioMaterialDataRepository.save(bioMaterialData);
			materialDatas.add(bioMaterialData);
			bioAnimalMaterial.setBioMaterialsUsed(materialDatas);
			protocol.setBioAnimalMaterial(bioAnimalMaterial);
			protocolRepository.save(protocol);
			return (BioAnimalMaterial) protocolRepository.getEntity(bioAnimalMaterialId, BioAnimalMaterial.class.getSimpleName());
		} else
		{
			Class<?> bioMaterialDataClass = Class.forName(BioMaterialData.class.getName());
			Boolean booleanValue = null;
			if (bioMaterialDataClass.getDeclaredField(updateJson.getFieldName()).getType().getName().equals(boolean.class.getName()))
			{
				booleanValue = Boolean.valueOf(updateJson.getFieldValue());
				protocolRepository.updateEntity(updateJson.getFieldName(), booleanValue, bioMaterialsUsedId, BioMaterialData.class.getSimpleName());

			} else
			{
				protocolRepository.updateEntity(updateJson.getFieldName(), updateJson.getFieldValue(), bioMaterialsUsedId,
						BioMaterialData.class.getSimpleName());
			}

			return (BioAnimalMaterial) protocolRepository.getEntity(bioAnimalMaterialId, BioAnimalMaterial.class.getSimpleName());

		}

	}

	@Override
	public BioAnimalMaterial updateBioAnimalMaterial(UpdateJson updateJson, Long protocolId, Long bioAnimalMaterialId) throws Exception
	{

		Class<?> bioAnimalMaterialClass = Class.forName(BioAnimalMaterial.class.getName());
		Boolean booleanValue = null;
		if (bioAnimalMaterialClass.getDeclaredField(updateJson.getFieldName()).getType().getName().equals(boolean.class.getName()))
		{
			booleanValue = Boolean.valueOf(updateJson.getFieldValue());
			protocolRepository.updateEntity(updateJson.getFieldName(), booleanValue, bioAnimalMaterialId, BioAnimalMaterial.class.getSimpleName());

		} else
		{
			protocolRepository.updateEntity(updateJson.getFieldName(), updateJson.getFieldValue(), bioAnimalMaterialId,
					BioAnimalMaterial.class.getSimpleName());
		}

		return (BioAnimalMaterial) protocolRepository.getEntity(bioAnimalMaterialId, BioAnimalMaterial.class.getSimpleName());
	}

	@Override
	public HazardousAgents updateHazardousAgents(UpdateJson updateJson, Long protocolId, Long hazardousAgentsId, Long userInputId)
	{

		UserInput userInput = null;
		List<UserInput> userInputs = null;
		if (userInputId == -1)
		{
			Protocol protocol = protocolRepository.findOne(protocolId);
			userInput = setUserInputValues(new UserInput(), updateJson);
			userInput = userInputRepository.save(userInput);
			userInputs = protocol.getHazardousAgents().getUserInputs();
			if (userInputs == null)
			{
				userInputs = new ArrayList<UserInput>();
			}
			userInputs = setUserInputList(userInputs, userInput);
			protocol.getHazardousAgents().setUserInputs(userInputs);
			protocolRepository.save(protocol);
			return (HazardousAgents) protocolRepository.getEntity(hazardousAgentsId, HazardousAgents.class.getSimpleName());
		} else
		{
			userInput = userInputRepository.findOne(userInputId);
			userInput = setUserInputValues(userInput, updateJson);
			userInputRepository.save(userInput);
			return (HazardousAgents) protocolRepository.getEntity(hazardousAgentsId, HazardousAgents.class.getSimpleName());
		}

	}

	@Override
	public MajorSurvialSurgery updateMajorSurvialSurgery(UpdateJson updateJson, Long protocolId, Long majorSurvialSurgeryId) throws Exception
	{

		Class<?> majorSurvialSurgeryClass = Class.forName(MajorSurvialSurgery.class.getName());
		Boolean booleanValue = null;
		if (majorSurvialSurgeryClass.getDeclaredField(updateJson.getFieldName()).getType().getName().equals(boolean.class.getName()))
		{
			booleanValue = Boolean.valueOf(updateJson.getFieldValue());
			protocolRepository
					.updateEntity(updateJson.getFieldName(), booleanValue, majorSurvialSurgeryId, MajorSurvialSurgery.class.getSimpleName());

		} else
		{
			protocolRepository.updateEntity(updateJson.getFieldName(), updateJson.getFieldValue(), majorSurvialSurgeryId,
					MajorSurvialSurgery.class.getSimpleName());
		}

		return (MajorSurvialSurgery) protocolRepository.getEntity(majorSurvialSurgeryId, MajorSurvialSurgery.class.getSimpleName());
	}

	@Override
	public AnesthesiaMethod updateAnesthesiaMethod(UpdateJson updateJson, Long protocolId, Long anesthesiamethodId) throws Exception
	{

		Class<?> anesthesiaMethodClass = Class.forName(AnesthesiaMethod.class.getName());
		Boolean booleanValue = null;
		if (anesthesiaMethodClass.getDeclaredField(updateJson.getFieldName()).getType().getName().equals(boolean.class.getName()))
		{
			booleanValue = Boolean.valueOf(updateJson.getFieldValue());
			protocolRepository.updateEntity(updateJson.getFieldName(), booleanValue, anesthesiamethodId, AnesthesiaMethod.class.getSimpleName());

		} else
		{
			protocolRepository.updateEntity(updateJson.getFieldName(), updateJson.getFieldValue(), anesthesiamethodId,
					AnesthesiaMethod.class.getSimpleName());
		}

		return (AnesthesiaMethod) protocolRepository.getEntity(anesthesiamethodId, AnesthesiaMethod.class.getSimpleName());

	}

	@Override
	public EuthanasiaMethod updateEuthanasiaMethodUserInput(UpdateJson updateJson, Long protocolId, Long euthanasiamethodId, Long userinputId)
	{

		Protocol protocol = protocolRepository.findOne(protocolId);
		EuthanasiaMethod euthanasiaMethod = protocol.getEuthanasiaMethod();
		UserInput userInput = null;
		if (userinputId == -1)
		{
			userInput = new UserInput();
		} else
		{
			userInput = euthanasiaMethod.getEuthanasiaMethodDesc();
		}
		userInput = setUserInputValues(userInput, updateJson);
		euthanasiaMethod.setEuthanasiaMethodDesc(userInput);
		protocol.setEuthanasiaMethod(euthanasiaMethod);
		protocolRepository.save(protocol);

		return (EuthanasiaMethod) protocolRepository.getEntity(euthanasiamethodId, EuthanasiaMethod.class.getSimpleName());

	}

	@Override
	public EuthanasiaMethod updateEuthanasiaMethod(UpdateJson updateJson, Long protocolId, Long euthanasiamethodId) throws Exception
	{

		Class<?> euthanasiaMethodClass = Class.forName(EuthanasiaMethod.class.getName());
		Boolean booleanValue = null;
		if (euthanasiaMethodClass.getDeclaredField(updateJson.getFieldName()).getType().getName().equals(boolean.class.getName()))
		{
			booleanValue = Boolean.valueOf(updateJson.getFieldValue());
			protocolRepository.updateEntity(updateJson.getFieldName(), booleanValue, euthanasiamethodId, EuthanasiaMethod.class.getSimpleName());

		} else
		{
			protocolRepository.updateEntity(updateJson.getFieldName(), updateJson.getFieldValue(), euthanasiamethodId,
					EuthanasiaMethod.class.getSimpleName());
		}

		return (EuthanasiaMethod) protocolRepository.getEntity(euthanasiamethodId, EuthanasiaMethod.class.getSimpleName());

	}

	@Override
	public HazardousAgents updateHazardousAgents(UpdateJson updateJson, Long protocolId, Long hazardousAgentsId) throws Exception
	{

		Class<?> hazardousAgentsClass = Class.forName(HazardousAgents.class.getName());
		Boolean booleanValue = null;
		if (hazardousAgentsClass.getDeclaredField(updateJson.getFieldName()).getType().getName().equals(boolean.class.getName()))
		{
			booleanValue = Boolean.valueOf(updateJson.getFieldValue());
			protocolRepository.updateEntity(updateJson.getFieldName(), booleanValue, hazardousAgentsId, HazardousAgents.class.getSimpleName());

		} else
		{
			protocolRepository.updateEntity(updateJson.getFieldName(), updateJson.getFieldValue(), hazardousAgentsId,
					HazardousAgents.class.getSimpleName());
		}

		return (HazardousAgents) protocolRepository.getEntity(hazardousAgentsId, HazardousAgents.class.getSimpleName());

	}

	@Override
	public SpecialRequirementOfStudy updateSpecialRequirementOfStudy(UpdateJson updateJson, Long protocolId, Long specialRequirementOfStudyId)
			throws Exception
	{

		Class<?> specialRequirementOfStudyClass = Class.forName(SpecialRequirementOfStudy.class.getName());
		Boolean booleanValue = null;
		if (specialRequirementOfStudyClass.getDeclaredField(updateJson.getFieldName()).getType().getName().equals(boolean.class.getName()))
		{
			booleanValue = Boolean.valueOf(updateJson.getFieldValue());
			protocolRepository.updateEntity(updateJson.getFieldName(), booleanValue, specialRequirementOfStudyId,
					SpecialRequirementOfStudy.class.getSimpleName());

		} else
		{
			protocolRepository.updateEntity(updateJson.getFieldName(), updateJson.getFieldValue(), specialRequirementOfStudyId,
					SpecialRequirementOfStudy.class.getSimpleName());
		}

		return (SpecialRequirementOfStudy) protocolRepository.getEntity(specialRequirementOfStudyId, SpecialRequirementOfStudy.class.getSimpleName());

	}

	@Override
	public Review updateReview(UpdateJson updateJson, Long protocolId, Long reviewId) throws Exception
	{
		Class<?> reviewClass = Class.forName(Review.class.getName());
		Boolean booleanValue = null;
		if (reviewClass.getDeclaredField(updateJson.getFieldName()).getType().getName().equals(boolean.class.getName()))
		{
			booleanValue = Boolean.valueOf(updateJson.getFieldValue());
			protocolRepository.updateEntity(updateJson.getFieldName(), booleanValue, reviewId, Review.class.getSimpleName());

		} else
		{
			protocolRepository.updateEntity(updateJson.getFieldName(), updateJson.getFieldValue(), reviewId, Review.class.getSimpleName());
		}

		return (Review) protocolRepository.getEntity(reviewId, Review.class.getSimpleName());

	}

	@Override
	public Review addReviewUser(UpdateJson updateJson, Long protocolId, Long reviewId) throws Exception
	{

		Protocol protocol = protocolRepository.findOne(protocolId);
		List<User> userNames = null;
		User user = null;
		Review review = protocol.getReview();
		userNames = review.getUserNames();
		if (userNames == null)
		{
			userNames = new ArrayList<User>();
			user = userRepository.findOne(Long.parseLong(updateJson.getFieldValue()));
			userNames.add(user);
			review.setUserNames(userNames);
		} else
		{
			user = userRepository.findOne(Long.parseLong(updateJson.getFieldValue()));
			userNames.add(user);
		}

		protocolRepository.save(protocol);

		return (Review) protocolRepository.getEntity(reviewId, Review.class.getSimpleName());

	}

	@Override
	public Protocol updatePIUser(UpdateJson updateJson, Long protocolId, Long piuserId) throws Exception
	{

		Protocol protocol = null;
		User user = null;

		protocol = protocolRepository.findOne(protocolId);

		user = userRepository.findOne(piuserId);
		protocol.setPiUser(user);

		protocolRepository.save(protocol);
		return (Protocol) protocolRepository.getEntity(protocolId, Protocol.class.getSimpleName());

	}

	@Override
	public AnimalRequirements deleteAnimalRequirements(Long protocolId, Long animalRequirementsId) throws Exception
	{
		Protocol protocol = protocolRepository.findOne(protocolId);
		Set<AnimalRequirements> animalRequirements = protocol.getAnimalRequirements();
		AnimalRequirements animalRequirementsToRemove = animalRequirementsRepository.findOne(animalRequirementsId);
		if (animalRequirementsToRemove != null)
		{
			animalRequirements.remove(animalRequirementsToRemove);
			protocol.setAnimalRequirements(animalRequirements);
			protocolRepository.save(protocol);
			animalRequirementsRepository.delete(animalRequirementsId);
		} else
		{
			throw new ARDXXAppException(Messages.properties.getProperty("no.animalrequirements"));
		}

		return null;
	}

	@Override
	public ProtocolAdministrativeData deleteCoInvestigators(Long protocolId, Long protocolAdministrativeDataId, Long coInvestigatorsId)
	{

		Protocol protocol = protocolRepository.findByProtocolAdministrativeDataId(protocolId);

		ProtocolAdministrativeData protocolAdministrativeData = protocol.getProtocolAdministrativeData();

		List<User> users = protocolAdministrativeData.getCoInvestigators();
		User userToRemove = null;
		for (User userInList : users)
		{

			if (userInList.getId() == coInvestigatorsId)
			{
				userToRemove = userInList;
			}
		}
		users.remove(userToRemove);

		protocolAdministrativeData.setCoInvestigators(users);
		protocolAdministrativeDataRepository.save(protocolAdministrativeData);
		return (ProtocolAdministrativeData) protocolRepository.getEntity(protocolId, ProtocolAdministrativeData.class.getSimpleName());

	}

	@Override
	public List<ProtocolDTO> getProtocolsByUserAndStatus(Long piuserId, String status) throws Exception
	{
		List<Protocol> protocols = protocolRepository.findByPiUserIdAndStatus(piuserId, ProtocolStatus.APPROVED);
		return protocolToDto(protocols);
	}
}
