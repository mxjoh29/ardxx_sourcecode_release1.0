/**
 * 
 */
package com.ardxx.ardxxapi.protocol.domain;

/**
 * @author gopikrishnappa
 *
 */
public enum LocationType
{
	HOLDING, PROCEDURE
}
