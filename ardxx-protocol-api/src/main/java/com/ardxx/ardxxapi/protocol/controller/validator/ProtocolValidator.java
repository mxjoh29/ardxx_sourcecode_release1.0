package com.ardxx.ardxxapi.protocol.controller.validator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.ardxx.ardxxapi.protocol.domain.AnimalRequirements;
import com.ardxx.ardxxapi.protocol.domain.Protocol;
import com.ardxx.ardxxapi.protocol.domain.UserInput;
import com.ardxx.ardxxapi.protocol.dto.UpdateJson;
import com.tn.security.common.Messages;
import com.tn.security.common.TNUtils;

public class ProtocolValidator
{
	public List<String> validateProtocolCreate()
	{
		List<String> errorCodes = new ArrayList<String>();

		return errorCodes;
	}

	public List<String> validateGetProtocol(Long userId)
	{
		List<String> errorCodes = new ArrayList<String>();

		return errorCodes;
	}

	public List<String> validateStatusApproval(Protocol protocol)
	{
		List<String> errorCode = new ArrayList<String>();

		if (TNUtils.isEmpty(protocol.getProposalNumber()))
		{
			errorCode.add(Messages.properties.getProperty("proposal.number.empty"));
		}

		return errorCode;
	}

	public List<String> validateStudyObjective(UpdateJson updateJson)
	{
		List<String> errorCode = new ArrayList<String>();

		if (updateJson.getFieldValue() != null && updateJson.getFieldValue().length() > 300)
		{
			errorCode.add(Messages.properties.getProperty("studyobjetive.Q.max.length"));
		}
		if ((updateJson.getFieldValue() == null || updateJson.getFieldValue().trim().isEmpty()) && "3".equals(updateJson.getFieldName()))
		{
			errorCode.add(Messages.properties.getProperty("studyobjetive.Q3.empty"));
		}
		return errorCode;
	}

	public List<String> validateStatusUpdate(Protocol protocol)
	{
		List<String> errorCode = new ArrayList<String>();

		if (protocol.getProtocolAdministrativeData() == null)
		{
			errorCode.add(Messages.properties.getProperty("administrativedata.empty"));
		} else
		{
			if (protocol.getProtocolAdministrativeData().getPrincipalInvestigator() == null)
			{
				errorCode.add(Messages.properties.getProperty("administrativedata.pi.empty"));
			}

			if (TNUtils.isEmpty(protocol.getProtocolAdministrativeData().getClient()))
			{
				errorCode.add(Messages.properties.getProperty("administrativedata.client.empty"));
			}

			if (TNUtils.isEmpty(protocol.getProtocolAdministrativeData().getProjectTitle()))
			{
				errorCode.add(Messages.properties.getProperty("administrativedata.title.empty"));
			}
		}

		if (protocol.getAnimalRequirements() != null)
		{
			Iterator<AnimalRequirements> animalRequirements = protocol.getAnimalRequirements().iterator();

			if (animalRequirements != null)
			{
				while (animalRequirements.hasNext())
				{
					AnimalRequirements animalRequirement = animalRequirements.next();
					if (animalRequirement.getSex() == null || animalRequirement.getSex().isEmpty())
					{
						errorCode.add(Messages.properties.getProperty("sex.not.empty"));
					}
					if (animalRequirement.getSpecies() == null || animalRequirement.getSpecies().isEmpty())
					{
						errorCode.add(Messages.properties.getProperty("species.not.empty"));
					}
				}

			}
		}

		if (protocol.getTransportation() == null
				|| (protocol.getTransportation() != null && TNUtils.isEmpty(protocol.getTransportation().getDescription())))
		{
			errorCode.add(Messages.properties.getProperty("transportation.empty"));
		}

		if (protocol.getStudyObjective() == null || (protocol.getStudyObjective() != null && protocol.getStudyObjective().getUserInputs() == null))
		{
			errorCode.add(Messages.properties.getProperty("studyobjetive.Q3.empty"));
		} else
		{
			List<UserInput> userInputs = protocol.getStudyObjective().getUserInputs();
			boolean found = false;
			for (UserInput userInput : userInputs)
			{
				if (userInput.getLabel().equalsIgnoreCase("3"))
				{
					found = true;
				}
			}

			if (!found)
			{
				errorCode.add(Messages.properties.getProperty("studyobjetive.Q3.empty"));
			}
		}

		if (protocol.getRationalUseOfAnimals() == null
				|| (protocol.getRationalUseOfAnimals() != null && protocol.getRationalUseOfAnimals().getUserInputs() == null))
		{
			errorCode.add(Messages.properties.getProperty("rationaluseofanimals.Q3.empty"));
		} else
		{
			List<UserInput> userInputs = protocol.getRationalUseOfAnimals().getUserInputs();
			boolean found = false;
			for (UserInput userInput : userInputs)
			{
				if (userInput.getLabel().equalsIgnoreCase("3"))
				{
					found = true;
				}
			}

			if (!found)
			{
				errorCode.add(Messages.properties.getProperty("rationaluseofanimals.Q3.empty"));
			}
		}

		if (protocol.getDescriptionOfExperiment() == null
				|| (protocol.getDescriptionOfExperiment() != null && protocol.getDescriptionOfExperiment().getUserInputs() == null))
		{
			errorCode.add(Messages.properties.getProperty("experimentaldesign.Q1.empty"));
			errorCode.add(Messages.properties.getProperty("experimentaldesign.Q12.empty"));
			errorCode.add(Messages.properties.getProperty("experimentaldesign.Q13.empty"));
			errorCode.add(Messages.properties.getProperty("experimentaldesign.Q14.empty"));
		} else
		{
			List<UserInput> userInputs = protocol.getDescriptionOfExperiment().getUserInputs();
			boolean foundQ1 = false;
			boolean foundQ12 = false;
			boolean foundQ13 = false;
			boolean foundQ14 = false;
			for (UserInput userInput : userInputs)
			{
				if (userInput.getLabel().equalsIgnoreCase("1"))
				{
					foundQ1 = true;
				}
				if (userInput.getLabel().equalsIgnoreCase("12"))
				{
					foundQ12 = true;
				}
				if (userInput.getLabel().equalsIgnoreCase("13"))
				{
					foundQ13 = true;
				}
				if (userInput.getLabel().equalsIgnoreCase("14"))
				{
					foundQ14 = true;
				}
			}

			if (!foundQ1)
			{
				errorCode.add(Messages.properties.getProperty("experimentaldesign.Q1.empty"));
			}
			if (!foundQ12)
			{
				errorCode.add(Messages.properties.getProperty("experimentaldesign.Q12.empty"));
			}
			if (!foundQ13)
			{
				errorCode.add(Messages.properties.getProperty("experimentaldesign.Q13.empty"));
			}
			if (!foundQ14)
			{
				errorCode.add(Messages.properties.getProperty("experimentaldesign.Q14.empty"));
			}
		}

		return errorCode;
	}
}
