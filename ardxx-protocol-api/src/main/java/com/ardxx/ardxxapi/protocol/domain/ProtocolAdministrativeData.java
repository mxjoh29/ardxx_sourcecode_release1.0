package com.ardxx.ardxxapi.protocol.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.tn.security.basic.domain.User;

@Entity
public class ProtocolAdministrativeData implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	private User principalInvestigator;
	
	private String building;
	private String room;
	private String phone;
	private String fax;
	private String email;
	private String client;
	private String projectTitle;
	private boolean initialSubmission = false;
	private boolean modification = false;
	private String renewalProposalNumber;
	@ElementCollection
	private List<User> animalProceduresPersonals = new ArrayList<User>();
	private boolean piHandsOnAnimalWork = false;
	@ElementCollection
	private List<User> coInvestigators = new ArrayList<User>();
	@ElementCollection
	List<User> otherPersonals = new ArrayList<User>();
	private String comments;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public User getPrincipalInvestigator()
	{
		return principalInvestigator;
	}

	public void setPrincipalInvestigator(User principalInvestigator)
	{
		this.principalInvestigator = principalInvestigator;
	}

	public String getBuilding()
	{
		return building;
	}

	public void setBuilding(String building)
	{
		this.building = building;
	}

	public String getRoom()
	{
		return room;
	}

	public void setRoom(String room)
	{
		this.room = room;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public String getFax()
	{
		return fax;
	}

	public void setFax(String fax)
	{
		this.fax = fax;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getClient()
	{
		return client;
	}

	public void setClient(String client)
	{
		this.client = client;
	}

	public String getProjectTitle()
	{
		return projectTitle;
	}

	public void setProjectTitle(String projectTitle)
	{
		this.projectTitle = projectTitle;
	}

	public boolean isInitialSubmission()
	{
		return initialSubmission;
	}

	public void setInitialSubmission(boolean initialSubmission)
	{
		this.initialSubmission = initialSubmission;
	}

	public boolean isModification()
	{
		return modification;
	}

	public void setModification(boolean modification)
	{
		this.modification = modification;
	}

	public String getRenewalProposalNumber()
	{
		return renewalProposalNumber;
	}

	public void setRenewalProposalNumber(String renewalProposalNumber)
	{
		this.renewalProposalNumber = renewalProposalNumber;
	}

	public List<User> getAnimalProceduresPersonals()
	{
		return animalProceduresPersonals;
	}

	public void setAnimalProceduresPersonals(List<User> animalProceduresPersonals)
	{
		this.animalProceduresPersonals = animalProceduresPersonals;
	}

	public List<User> getCoInvestigators()
	{
		return coInvestigators;
	}

	public void setCoInvestigators(List<User> coInvestigators)
	{
		this.coInvestigators = coInvestigators;
	}

	public List<User> getOtherPersonals()
	{
		return otherPersonals;
	}

	public void setOtherPersonals(List<User> otherPersonals)
	{
		this.otherPersonals = otherPersonals;
	}

	public String getComments()
	{
		return comments;
	}

	public void setComments(String comments)
	{
		this.comments = comments;
	}

	public boolean isPiHandsOnAnimalWork()
	{
		return piHandsOnAnimalWork;
	}

	public void setPiHandsOnAnimalWork(boolean piHandsOnAnimalWork)
	{
		this.piHandsOnAnimalWork = piHandsOnAnimalWork;
	}

}
