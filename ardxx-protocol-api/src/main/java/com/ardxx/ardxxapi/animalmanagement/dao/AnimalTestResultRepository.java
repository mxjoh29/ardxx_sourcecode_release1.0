package com.ardxx.ardxxapi.animalmanagement.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.ardxx.ardxxapi.animalmanagement.domain.AnimalTestResults;

public interface AnimalTestResultRepository extends PagingAndSortingRepository<AnimalTestResults, Long>
{
	@Query("FROM AnimalTestResults where collectedDate between ?1 and ?2")
	Iterable<AnimalTestResults> findByCollectedDate(Date startDate,Date endDate);
	
	Iterable<AnimalTestResults> findByAnimalIdId(@Param("id") Long id);
}
