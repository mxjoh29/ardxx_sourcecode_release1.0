package com.ardxx.ardxxapi.animalmanagement.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ardxx.ardxxapi.animalmanagement.dao.AnimalGroupRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.AnimalHistoryRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.AnimalsRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.ProcedureRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.ProcedureScheduleRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.ProcedureTestRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.StudyRepository;
import com.ardxx.ardxxapi.animalmanagement.domain.AnimalGroup;
import com.ardxx.ardxxapi.animalmanagement.domain.AnimalHistory;
import com.ardxx.ardxxapi.animalmanagement.domain.Animals;
import com.ardxx.ardxxapi.animalmanagement.domain.BloodSample;
import com.ardxx.ardxxapi.animalmanagement.domain.Inoculation;
import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureSchedule;
import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureTest;
import com.ardxx.ardxxapi.animalmanagement.domain.Study;
import com.ardxx.ardxxapi.animalmanagement.domain.StudyProcedure;
import com.ardxx.ardxxapi.animalmanagement.domain.Treatment;
import com.ardxx.ardxxapi.animalmanagement.dto.AnimalsTPRWDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.ScheduleAnimalDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.ScheduleCloneDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.StudyDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.StudyProcedureDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.VolumeDTO;
import com.ardxx.ardxxapi.common.ARDXXUtils;
import com.ardxx.ardxxapi.common.ConstantsMap;
import com.ardxx.ardxxapi.common.Messages;
import com.ardxx.ardxxapi.common.dao.ConstantsRepository;
import com.ardxx.ardxxapi.common.domain.Constants;
import com.ardxx.ardxxapi.exception.ARDXXAppException;
import com.tn.security.basic.domain.User;
import com.tn.security.basic.repository.UserRepository;

@Service
public class ProcedureServiceImpl implements ProcedureService
{

	@Autowired
	ProcedureRepository procedureRepository;

	@Autowired
	StudyRepository studyRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	ConstantsRepository constantsRepository;

	@Autowired
	AnimalsRepository animalsRepository;

	@Autowired
	ProcedureScheduleRepository procedureScheduleRepository;

	@Autowired
	ProcedureTestRepository procedureTestRepository;

	@Autowired
	AnimalGroupRepository animalGroupRepository;

	@Autowired
	AnimalHistoryRepository animalHistoryRepository;

	@Autowired
	ConstantsMap constantsMap;

	@Override
	public StudyProcedure createProcedure(Long studyId, StudyProcedure procedure)
	{
		List<StudyProcedure> procedures = null;
		procedure.setStatus("CREATED");
		procedure = procedureRepository.save(procedure);
		Study study = studyRepository.findOne(studyId);
		procedures = study.getProcedures();

		if (procedures != null)
		{
			procedures.add(procedure);
		} else
		{
			procedures = new ArrayList<StudyProcedure>();
			procedures.add(procedure);
		}
		study.setProcedures(procedures);
		studyRepository.save(study);
		return procedure;
	}

	@Override
	public StudyProcedure createProcedureTests(Long studyId, Long procedureId, ProcedureTest procedureTest) throws Exception
	{
		StudyProcedure procedure = procedureRepository.findOne(procedureId);

		if (procedure == null)
		{
			throw new ARDXXAppException(Messages.properties.getProperty("no.procedure"));
		} else
		{
			if (!procedure.getProcedureType().equalsIgnoreCase(ARDXXUtils.BLOOD_SAMPLE))
			{
				throw new ARDXXAppException(Messages.properties.getProperty("procedure.type.not.bloodsample"));
			}
			BloodSample bloodSample = procedure.getBloodSample();

			if (bloodSample == null)
			{
				throw new ARDXXAppException(Messages.properties.getProperty("no.bloodsample"));
			} else
			{
				procedureTest = procedureTestRepository.save(procedureTest);

				List<ProcedureTest> procedureTests = bloodSample.getProcedureTests();
				if (procedureTests == null)
				{
					procedureTests = new ArrayList<ProcedureTest>();
				} else
				{
					procedureTests.add(procedureTest);
				}

				bloodSample.setProcedureTests(procedureTests);

				procedureRepository.save(procedure);
			}

			return procedure;
		}

	}

	@Override
	public StudyProcedure getProcedure(Long procedureId)
	{
		StudyProcedure studyProcedure = procedureRepository.findOne(procedureId);
		List<Animals> animals = studyProcedure.getAnimals();
		List<Animals> animalsList = new ArrayList<Animals>();
		for (Animals animal : animals)
		{
			AnimalGroup animalGroup = animalGroupRepository.findOneByAnimalsId(animal.getId());
			if (animalGroup != null)
			{
				animal.setGroupName(animalGroup.getGroupName());
			}

			animalsList.add(animal);
		}
		studyProcedure.setAnimals(animalsList);
		return studyProcedure;
	}

	@Override
	public Page<StudyProcedure> getProcedureList(int page, int size)
	{
		Pageable pageable = new PageRequest(page, size);
		return procedureRepository.findAll(pageable);
	}

	@Override
	public StudyProcedure assignTechnician(Long studyId, Long procedureId, String type, Long userId)
	{
		StudyProcedure procedure = procedureRepository.findOne(procedureId);
		User user = userRepository.findOne(userId);
		if (type.equalsIgnoreCase("VET_TEC"))
		{
			procedure.setVetTechnician(user);
		} else if (type.equalsIgnoreCase("LAB_TEC"))
		{
			procedure.setLabTechnician(user);
		}
		procedure = procedureRepository.save(procedure);
		return procedure;
	}

	@Override
	public List<Constants> getAllProcedureValues()
	{
		return constantsRepository.findByNameStartingWith("procedure.");
	}

	@Override
	public StudyProcedure assignAnimalsToProcedure(Long studyId, Long procedureId, StudyDTO studyDTO)
	{
		StudyProcedure procedure = procedureRepository.findOne(procedureId);
		
		List<Animals> animalsInProcedure = procedure.getAnimals();
		List<Long> eixistingAnimalIds = new ArrayList<Long>();
		if (animalsInProcedure == null)
		{
			animalsInProcedure = new ArrayList<Animals>();
		}
		else
		{
			for (Animals animals : animalsInProcedure)
			{
				for (Long animalIds : studyDTO.getAnimalIds())
				{
					if(animalIds == animals.getId())
					{
						eixistingAnimalIds.add(animalIds);
					}
				}
			}
		}
		studyDTO.getAnimalIds().removeAll(eixistingAnimalIds);
		Iterable<Animals> animals = animalsRepository.findAll(studyDTO.getAnimalIds());
		
		for (Animals animal : animals)
		{
			animalsInProcedure.add(animal);
		}

		procedure.setAnimals(animalsInProcedure);

		procedure = procedureRepository.save(procedure);
		return procedure;
	}

	@Override
	public StudyProcedure deleteAnimalsFromProcedure(Long studyId, Long procedureId, Long animalId)
	{

		List<Animals> animalsToRemove = new ArrayList<Animals>();
		StudyProcedure procedure = procedureRepository.findOne(procedureId);

		List<Animals> animalsInProcedure = procedure.getAnimals();
		if (animalsInProcedure != null)
		{
			for (Animals animals : animalsInProcedure)
			{
				if (animals.getId() == animalId)
				{
					animalsToRemove.add(animals);
					break;
				}

			}
		}

		if (animalsToRemove != null && !animalsToRemove.isEmpty())
		{
			procedure.getAnimals().removeAll(animalsToRemove);
		}

		return procedureRepository.save(procedure);
	}

	@Override
	public StudyProcedure scheduleProcedure(Long studyId, Long procedureId, ProcedureSchedule procedureSchedule)
	{
		StudyProcedure studyProcedure = procedureRepository.findOne(procedureId);
		List<ProcedureSchedule> procedureSchedules = studyProcedure.getProcedureSchedules();

		if (procedureSchedules == null)
		{
			procedureSchedules = new ArrayList<ProcedureSchedule>();
		}

		procedureSchedule = procedureScheduleRepository.save(procedureSchedule);
		procedureSchedules.add(procedureSchedule);
		studyProcedure = procedureRepository.save(studyProcedure);
		return studyProcedure;
	}

	@Override
	public StudyProcedure changeProcedureStatus(Long studyId, Long procedureId, String status)
	{
		StudyProcedure studyProcedure = procedureRepository.findOne(procedureId);
		studyProcedure.setStatus(status);

		if (status.equalsIgnoreCase("ACTIVE"))
		{
			List<ProcedureSchedule> procedureSchedules = studyProcedure.getProcedureSchedules();

			for (ProcedureSchedule procedureSchedule : procedureSchedules)
			{
				procedureSchedule.setStatus("ACTIVE");
				procedureScheduleRepository.save(procedureSchedule);
			}
		}
		studyProcedure = procedureRepository.save(studyProcedure);
		return studyProcedure;
	}

	@Override
	public ProcedureSchedule updateScheduleProcedure(Long studyId, Long procedureId, Long scheduleId, ProcedureSchedule procedureSchedule)
			throws Exception
	{
		ProcedureSchedule procedureScheduleFromDB = procedureScheduleRepository.findOne(scheduleId);
		if (procedureScheduleFromDB == null)
		{
			throw new ARDXXAppException(Messages.properties.getProperty("no.procedure.schedule"));
		}
		procedureScheduleFromDB.setScheduleDate(procedureSchedule.getScheduleDate());
		procedureScheduleFromDB.setStatus(procedureSchedule.getStatus());
		procedureScheduleFromDB = procedureScheduleRepository.save(procedureScheduleFromDB);
		return procedureScheduleFromDB;
	}

	@Override
	public void deleteScheduleProcedure(Long studyId, Long procedureId, Long scheduleId) throws Exception
	{
		ProcedureSchedule scheduleExist = procedureScheduleRepository.findOne(scheduleId);
		if (scheduleExist != null)
		{
			StudyProcedure studyProcedure = procedureRepository.findOne(procedureId);
			studyProcedure.getProcedureSchedules().remove(scheduleExist);
			procedureRepository.save(studyProcedure);
			procedureScheduleRepository.delete(scheduleId);
		} else
		{
			throw new ARDXXAppException(Messages.properties.getProperty("no.procedure.schedule"));
		}
	}

	@Override
	public Iterable<StudyProcedureDTO> getProcedureSchedules(Long userid)
	{
		List<StudyProcedureDTO> studyProcedureDTOs = new ArrayList<StudyProcedureDTO>();
		Iterable<StudyProcedure> procedureSchedules = procedureRepository.findByVetTechnicianId(userid);
		for (StudyProcedure studyProcedure : procedureSchedules)
		{

			Study study = studyRepository.findByProceduresId(studyProcedure.getId());
			for (ProcedureSchedule procedureSchedule : studyProcedure.getProcedureSchedules())
			{
				setStudyProcedureDTO(studyProcedureDTOs, studyProcedure, study, procedureSchedule);
			}

		}

		return studyProcedureDTOs;
	}

	@Override
	public ProcedureSchedule updateAnimalsInSchedule(Long studyId, Long procedureId, Long scheduleId, Long animalId,
			ScheduleAnimalDTO scheduleAnimalDTO)
	{
		Animals animals = animalsRepository.findOne(animalId);
		animals.setSedationRequired(scheduleAnimalDTO.isSedationRequired());
		animals.setSubstance(scheduleAnimalDTO.getSubstance());
		animals.setDosage(scheduleAnimalDTO.getDosage());
		animals.setTemperature(scheduleAnimalDTO.getTemperature());
		animals.setPulse(scheduleAnimalDTO.getPulse());
		animals.setRespiration(scheduleAnimalDTO.getRespiration());
		animals.setNotes(scheduleAnimalDTO.getNotes());
		animals.setWeight(scheduleAnimalDTO.getWeight());
		animals.setStudyId(studyId);
		animals.setProcedureId(procedureId);
		animals.setScheduleId(scheduleId);
		animalsRepository.save(animals);

		return procedureScheduleRepository.findOne(scheduleId);
	}

	@Override
	public StudyProcedure getProcedures(Long scheduleId)
	{
		StudyProcedure studyProcedure = procedureRepository.findByProcedureSchedulesId(scheduleId);

		Study study = studyRepository.findByProceduresId(studyProcedure.getId());

		studyProcedure.setStudyName(study.getName());
		studyProcedure.setStudyId(study.getId());

		return studyProcedure;
	}

	@Override
	public Iterable<StudyProcedureDTO> getProcedureSchedulesForLabTec(Long userid)
	{
		List<StudyProcedureDTO> studyProcedureDTOs = new ArrayList<StudyProcedureDTO>();
		Iterable<StudyProcedure> procedureSchedules = procedureRepository.findByLabTechnicianId(userid);
		for (StudyProcedure studyProcedure : procedureSchedules)
		{

			Study study = studyRepository.findByProceduresId(studyProcedure.getId());
			for (ProcedureSchedule procedureSchedule : studyProcedure.getProcedureSchedules())
			{
				setStudyProcedureDTO(studyProcedureDTOs, studyProcedure, study, procedureSchedule);
			}

		}

		return studyProcedureDTOs;
	}

	@Override
	public Iterable<StudyProcedureDTO> getProcedureSchedulesForPM()
	{
		List<StudyProcedureDTO> studyProcedureDTOs = new ArrayList<StudyProcedureDTO>();

		String status = "COMPLETED";

		Iterable<StudyProcedure> procedureSchedules = procedureRepository.findByStatusNot(status);
		for (StudyProcedure studyProcedure : procedureSchedules)
		{

			Study study = studyRepository.findByProceduresId(studyProcedure.getId());
			for (ProcedureSchedule procedureSchedule : studyProcedure.getProcedureSchedules())
			{
				setStudyProcedureDTO(studyProcedureDTOs, studyProcedure, study, procedureSchedule);
			}

		}

		return studyProcedureDTOs;
	}

	private void setStudyProcedureDTO(List<StudyProcedureDTO> studyProcedureDTOs, StudyProcedure studyProcedure, Study study,
			ProcedureSchedule procedureSchedule)
	{
		StudyProcedureDTO studyProcedureDTO = new StudyProcedureDTO();
		studyProcedureDTO.setScheduleDate(procedureSchedule.getScheduleDate());
		studyProcedureDTO.setStatus(procedureSchedule.getStatus());
		studyProcedureDTO.setProcedureId(studyProcedure.getId());
		studyProcedureDTO.setStudyName(study.getName());
		studyProcedureDTO.setScheduleId(procedureSchedule.getId());
		BloodSample bloodSample = studyProcedure.getBloodSample();
		Inoculation inoculation = studyProcedure.getInoculation();
		Treatment treatment = studyProcedure.getTreatment();

		if (bloodSample != null)
		{
			studyProcedureDTO.setTubeType(bloodSample.getTubeType());
			studyProcedureDTO.setQuantity(bloodSample.getQuantity());
			studyProcedureDTO.setMultipleTubes(bloodSample.getMultipleTubes());
			studyProcedureDTO.setUnit(bloodSample.getUnit());
			studyProcedureDTO.setNotes(bloodSample.getNotes());
		}

		if (inoculation != null)
		{
			studyProcedureDTO.setMaterial(inoculation.getMaterial());
			studyProcedureDTO.setMaterialPrep(inoculation.getMaterialPrep());
			studyProcedureDTO.setDeliveryDevice(inoculation.getDeliveryDevice());
			studyProcedureDTO.setRoute(inoculation.getRoute());
			studyProcedureDTO.setSite(inoculation.getSite());
			studyProcedureDTO.setSitePrep(inoculation.getSitePrep());
			studyProcedureDTO.setDosage(inoculation.getDosage());
			studyProcedureDTO.setDosageUnit(inoculation.getDosageUnit());
			studyProcedureDTO.setVolume(inoculation.getVolume());
			studyProcedureDTO.setVolumeUnit(inoculation.getVolumeUnit());
			studyProcedureDTO.setStudyName("");
		}

		if (treatment != null)
		{
			studyProcedureDTO.setGenericName(treatment.getGenericName());
			studyProcedureDTO.setConcentration(treatment.getConcentration());
			studyProcedureDTO.setConcentrationUnit(treatment.getConcentrationUnit());
			studyProcedureDTO.setDosage(treatment.getDosage());
			studyProcedureDTO.setDosageUnit(treatment.getDosageUnit());
			studyProcedureDTO.setRoute(treatment.getRoute());
			studyProcedureDTO.setFrequency(treatment.getFrequency());
			studyProcedureDTO.setNotes(treatment.getNotes());
			studyProcedureDTO.setStudyName("");
		}

		studyProcedureDTOs.add(studyProcedureDTO);
	}

	@Override
	public List<AnimalHistory> getAnimalListForSchedule(Long studyId, Long procedureId, Long scheduleId, ProcedureSchedule procedureSchedule)
	{
		return animalHistoryRepository.findDistinctAnimalTableIdByScheduleIdOrderByAddedDateDesc(scheduleId);
	}

	@Override
	public List<Animals> findAnimalsByProcedureId(Long procedureId)
	{
		List<Animals> animals = procedureRepository.findAnimalsByProcedureSchedulesId(procedureId);
		List<Animals> newAnimals = new ArrayList<Animals>();
		for (Animals animal : animals)
		{
			animal.setTemperature("");
			animal.setPulse("");
			animal.setRespiration("");
			animal.setVolume(0);
			newAnimals.add(animal);
		}

		return newAnimals;
	}

	@Override
	public Iterable<StudyProcedureDTO> getProcedureSchedulesForStudy(Long studyId) throws Exception
	{
		List<StudyProcedureDTO> studyProcedureDTOs = new ArrayList<StudyProcedureDTO>();
		Study study = studyRepository.findOne(studyId);

		if (study == null)
		{
			throw new ARDXXAppException(Messages.properties.getProperty("no.study"));
		}

		List<StudyProcedure> studyProcedures = study.getProcedures();

		if (studyProcedures != null && !studyProcedures.isEmpty())
		{
			for (StudyProcedure studyProcedure : studyProcedures)
			{
				for (ProcedureSchedule procedureSchedule : studyProcedure.getProcedureSchedules())
				{
					setStudyProcedureDTO(studyProcedureDTOs, studyProcedure, study, procedureSchedule);
				}
			}
		}

		return studyProcedureDTOs;
	}

	@Override
	public Iterable<StudyProcedureDTO> cloneProcedureSchedules(Long studyId, ScheduleCloneDTO scheduleCloneDTO)
	{
		List<BigInteger> procedureIds = studyRepository.findById(studyId);
		List<ProcedureSchedule> procedureScheduleNewList = null;
		// List<ProcedureSchedule> procedureScheduleResponse = new ArrayList<ProcedureSchedule>();
		List<StudyProcedureDTO> procedureScheduleResponse = new ArrayList<StudyProcedureDTO>();

		for (BigInteger procedureId : procedureIds)
		{
			String multipleTubes = null;
			String notes = null;
			String quantity = null;
			String tubeType = null;
			String unit = null;

			String material = null;
			String materialPrep = null;
			String deliveryDevice = null;
			String route = null;
			String site = null;
			String sitePrep = null;
			Float dosage = null;
			String dosageUnit = null;
			Float volume = null;
			String volumeUnit = null;
			
			String genericName = null;
			Float concentration = null;
			String concentrationUnit = null;
			String frequency = null;
			String procedureType = null;
			
			
			List<BigInteger> scheduleIds = procedureScheduleRepository.findById(procedureId.longValue());

			List<Object[]> procedureValues = procedureRepository.findByBloodSample(procedureId.longValue());

			if (procedureValues != null && !procedureValues.isEmpty())
			{
				procedureType = ARDXXUtils.BLOOD_SAMPLE;
				for (Object[] value : procedureValues)
				{
					if (value[0] != null)
					{
						multipleTubes = value[0].toString();
					}
					if (value[1] != null)
					{
						notes = value[1].toString();
					}
					if (value[2] != null)
					{
						quantity = value[2].toString();
					}
					if (value[3] != null)
					{
						tubeType = value[3].toString();
					}
					if (value[4] != null)
					{
						unit = value[4].toString();
					}

				}
			}

			procedureValues = procedureRepository.findByInoculation(procedureId.longValue());
			
			if (procedureValues != null && !procedureValues.isEmpty())
			{
				procedureType = ARDXXUtils.INOCULATION;
				for (Object[] value : procedureValues)
				{
					if (value[0] != null)
					{
						material = value[0].toString();
					}
					if (value[1] != null)
					{
						materialPrep = value[1].toString();
					}
					if (value[2] != null)
					{
						deliveryDevice = value[2].toString();
					}
					if (value[3] != null)
					{
						route = value[3].toString();
					}
					if (value[4] != null)
					{
						site = value[4].toString();
					}
					if (value[5] != null)
					{
						sitePrep = value[5].toString();
					}
					if (value[6] != null)
					{
						dosage = Float.valueOf(value[6].toString());
					}
					if (value[7] != null)
					{
						dosageUnit = value[7].toString();
					}
					if (value[8] != null)
					{
						volume = Float.valueOf(value[8].toString());
					}
					if (value[9] != null)
					{
						volumeUnit = value[9].toString();
					}
					if (value[10] != null)
					{
						notes = value[10].toString();
					}

				}
			}
			
			
			procedureValues = procedureRepository.findByTreatement(procedureId.longValue());
			
			if (procedureValues != null && !procedureValues.isEmpty())
			{
				procedureType = ARDXXUtils.TREATMENT;
				for (Object[] value : procedureValues)
				{
					if (value[0] != null)
					{
						genericName = value[0].toString();
					}
					if (value[1] != null)
					{
						concentration = Float.parseFloat(value[1].toString());
					}
					if (value[2] != null)
					{
						concentrationUnit = value[2].toString();
					}
					if (value[3] != null)
					{
						dosage = Float.parseFloat(value[3].toString());
					}
					if (value[4] != null)
					{
						dosageUnit = value[4].toString();
					}
					if (value[5] != null)
					{
						route = value[5].toString();
					}
					if (value[6] != null)
					{
						frequency = value[6].toString();
					}
					if (value[7] != null)
					{
						notes = value[7].toString();
					}
				}
			}
			
			procedureScheduleNewList = new ArrayList<ProcedureSchedule>();
			for (BigInteger scheduleId : scheduleIds)
			{
				ProcedureSchedule procedureSchedules = procedureScheduleRepository.findByScheduleDateAndId(scheduleCloneDTO.getFromDate(),
						scheduleId.longValue());
				if (procedureSchedules != null)
				{
					ProcedureSchedule procedureSchedule = new ProcedureSchedule();
					StudyProcedureDTO studyProcedureDTO = new StudyProcedureDTO();

					procedureSchedule.setScheduleDate(scheduleCloneDTO.getToDate());
					procedureSchedule.setStatus(procedureSchedules.getStatus());

					procedureSchedule = procedureScheduleRepository.save(procedureSchedule);

					studyProcedureDTO.setScheduleDate(scheduleCloneDTO.getToDate());
					studyProcedureDTO.setStatus(procedureSchedules.getStatus());
					studyProcedureDTO.setMultipleTubes(multipleTubes);
					studyProcedureDTO.setNotes(notes);
					studyProcedureDTO.setQuantity(quantity);
					studyProcedureDTO.setTubeType(tubeType);
					studyProcedureDTO.setUnit(unit);

					studyProcedureDTO.setMaterial(material);
					studyProcedureDTO.setMaterialPrep(materialPrep);
					studyProcedureDTO.setDeliveryDevice(deliveryDevice);
					studyProcedureDTO.setRoute(route);
					studyProcedureDTO.setSite(site);
					studyProcedureDTO.setSitePrep(sitePrep);
					// studyProcedureDTO.setDosage(dosage);
					studyProcedureDTO.setDosageUnit(dosageUnit);
					// studyProcedureDTO.setVolume(volume);
					studyProcedureDTO.setVolumeUnit(volumeUnit);
					
//					studyProcedureDTO.setConcentration(concentration);
					studyProcedureDTO.setConcentrationUnit(concentrationUnit);
					studyProcedureDTO.setFrequency(frequency);
					studyProcedureDTO.setGenericName(genericName);
					
					studyProcedureDTO.setProcedureId(procedureId.longValue());
					studyProcedureDTO.setProcedureType(procedureType);
					procedureScheduleNewList.add(procedureSchedule);

					procedureScheduleResponse.add(studyProcedureDTO);
				}

			}

			StudyProcedure procedure = procedureRepository.findOne(procedureId.longValue());
			procedure.getProcedureSchedules().addAll(procedureScheduleNewList);
			procedureRepository.save(procedure);
		}

		return procedureScheduleResponse;
	}

	@Override
	public Iterable<Animals> calculateVolume(Long studyId, Long scheduleId, VolumeDTO volumeDTO)
	{
		String key = "substance.name." + volumeDTO.getSubstanceName().toLowerCase() + ".concentration";
		Map<String, List<String>> map = constantsMap.loadConstants();
		List<String> concentrationValue = map.get(key);
		key = "substance.name." + volumeDTO.getSubstanceName().toLowerCase() + ".dosage";
		List<String> dosageValue = map.get(key);

		Long procedureId = procedureRepository.findById(scheduleId);
		float volume = 0;
		int weight = 0;
		List<Animals> animals = procedureRepository.findAnimalsByProcedureSchedulesId(procedureId);
		for (Animals animal : animals)
		{
			if (animal.getWeight() != null && !animal.getWeight().isEmpty())
			{
				weight = Integer.parseInt(animal.getWeight());
				volume = (weight * Float.parseFloat(dosageValue.get(0))) / (Float.parseFloat(concentrationValue.get(0)));
				animal.setVolume(volume);
				animal.setSubstance(volumeDTO.getSubstanceName());
				animal.setDosage(dosageValue.get(0));
				animal.setScheduleId(scheduleId);
				animalsRepository.save(animal);
			}
		}
		return animals;
	}

	@Override
	public Iterable<Animals> updateTPRWvalue(Long studyId, Long procedureId, Long scheduleId, List<AnimalsTPRWDTO> animalsTPRWDTO)
	{
		Animals animal = null;
		List<Animals> animals = new ArrayList<Animals>();

		for (AnimalsTPRWDTO animalsTPRWDTO2 : animalsTPRWDTO)
		{
			animal = animalsRepository.findOne(animalsTPRWDTO2.getId());
			animal.setTemperature(animalsTPRWDTO2.getTemperature());
			animal.setPulse(animalsTPRWDTO2.getPulse());
			animal.setRespiration(animalsTPRWDTO2.getRespiration());
			animal.setWeight(animalsTPRWDTO2.getWeight());
			animal.setScheduleId(scheduleId);
			animal.setProcedureId(procedureId);
			animal.setStudyId(studyId);
			animal.setVolume(animalsTPRWDTO2.getVolume());
			animalsRepository.save(animal);
			animals.add(animal);
		}
		return animals;
	}

	@Override
	public Page<StudyProcedure> getClinicalProcedures(int page, int size)
	{
		Pageable pageable = new PageRequest(page, size);
		return procedureRepository.findByProcedureType(pageable,"TREATMENT");
	}

}
