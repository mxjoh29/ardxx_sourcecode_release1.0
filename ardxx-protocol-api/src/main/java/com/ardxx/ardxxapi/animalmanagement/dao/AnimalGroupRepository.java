package com.ardxx.ardxxapi.animalmanagement.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ardxx.ardxxapi.animalmanagement.domain.AnimalGroup;

public interface AnimalGroupRepository extends PagingAndSortingRepository<AnimalGroup, Long>
{
	AnimalGroup findOneByAnimalsId(Long id);
}
