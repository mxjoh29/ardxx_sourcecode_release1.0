package com.ardxx.ardxxapi.animalmanagement.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Biopsy implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String specimen;
	private String sizeInCm;
	private String quanitiy;
	private String preservative;
	private String destination;
	private String storageRequirements;
	private String packagingRequirements;
	private String timeShipppedBy;
	private String shippedTo;
	private String building;
	private String freezer;
	private String shelf;
	private String rack;
	private String position;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getSpecimen()
	{
		return specimen;
	}

	public void setSpecimen(String specimen)
	{
		this.specimen = specimen;
	}

	public String getSizeInCm()
	{
		return sizeInCm;
	}

	public void setSizeInCm(String sizeInCm)
	{
		this.sizeInCm = sizeInCm;
	}

	public String getQuanitiy()
	{
		return quanitiy;
	}

	public void setQuanitiy(String quanitiy)
	{
		this.quanitiy = quanitiy;
	}

	public String getPreservative()
	{
		return preservative;
	}

	public void setPreservative(String preservative)
	{
		this.preservative = preservative;
	}

	public String getDestination()
	{
		return destination;
	}

	public void setDestination(String destination)
	{
		this.destination = destination;
	}

	public String getStorageRequirements()
	{
		return storageRequirements;
	}

	public void setStorageRequirements(String storageRequirements)
	{
		this.storageRequirements = storageRequirements;
	}

	public String getPackagingRequirements()
	{
		return packagingRequirements;
	}

	public void setPackagingRequirements(String packagingRequirements)
	{
		this.packagingRequirements = packagingRequirements;
	}

	public String getTimeShipppedBy()
	{
		return timeShipppedBy;
	}

	public void setTimeShipppedBy(String timeShipppedBy)
	{
		this.timeShipppedBy = timeShipppedBy;
	}

	public String getShippedTo()
	{
		return shippedTo;
	}

	public void setShippedTo(String shippedTo)
	{
		this.shippedTo = shippedTo;
	}

	public String getBuilding()
	{
		return building;
	}

	public void setBuilding(String building)
	{
		this.building = building;
	}

	public String getFreezer()
	{
		return freezer;
	}

	public void setFreezer(String freezer)
	{
		this.freezer = freezer;
	}

	public String getShelf()
	{
		return shelf;
	}

	public void setShelf(String shelf)
	{
		this.shelf = shelf;
	}

	public String getRack()
	{
		return rack;
	}

	public void setRack(String rack)
	{
		this.rack = rack;
	}

	public String getPosition()
	{
		return position;
	}

	public void setPosition(String position)
	{
		this.position = position;
	}

}
