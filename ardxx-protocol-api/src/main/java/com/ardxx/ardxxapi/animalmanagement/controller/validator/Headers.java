package com.ardxx.ardxxapi.animalmanagement.controller.validator;

public class Headers
{


	private int ID, OLDID, SPECIES, SEX, AGECATEGORY, CLIENT, DOB, SOURCE, RECEIVEDDATE, DEPARTEDDATE, ROOMNUM, VIRUS, WEIGHT, ORIGIN,REMARKS ;

	private int Accession_Number, Group_Number, Animal_Id, Age, Breed, Collected_Date, A_G_RATIO, ABSOLUTE_BASOPHIL, ABSOLUTE_EOSINOPHIL,
			ABSOLUTE_LYMPHOCYTE, ABSOLUTE_METAMYELOCYTES, ABSOLUTE_MONOCYTE, ABSOLUTE_NEUTROPHIL_BAND, ABSOLUTE_NEUTROPHIL_SEG,
			ABSOLUTE_PROMYELOCYTES, ALBUMIN, ALKALINE_PHOSPHATASE, ANISOCYTOSIS, B_C_RATIO, BASOPHIL, BICARBONATE, BUN, CALCIUM, CHLORIDE,
			CHOLESTEROL, CPK, CREATININE, DIRECT_BILIRUBIN, EOSINOPHIL, GLOBULIN, GLUCOSE, HCT, HEINZ_BODIES, HEMOLYSIS_INDEX, HGB,
			INDIRECT_BILIRUBIN, LIPEMIA_INDEX, LYMPHOCYTE, MCH, MCHC, MCV, METAMYELOCYTE, MONOCYTE, MYELOCYTE, NA_K_RATIO, NEUTROPHIL_BAND,
			NEUTROPHIL_SEG, NRBC, PHOSPHORUS, PLATELET_COUNT, PLATELET_ESTIMATE, POIKILOCYTOSIS, POLYCHROMASIA, POTASSIUM, PROMYELOCYTE, RBC,
			SGOT_AST, SGPT_ALT, SODIUM, TOTAL_BILIRUBIN, TOTAL_PROTEIN, WBC ;

	public Headers(String header)
	{
		ID = OLDID = SPECIES = SEX = AGECATEGORY = CLIENT = DOB = SOURCE = RECEIVEDDATE = DEPARTEDDATE = ROOMNUM = VIRUS = WEIGHT = ORIGIN = REMARKS = Accession_Number 
				= Group_Number = Animal_Id = Age = Breed = Collected_Date = A_G_RATIO = ABSOLUTE_BASOPHIL = ABSOLUTE_EOSINOPHIL = ABSOLUTE_LYMPHOCYTE = 
				ABSOLUTE_METAMYELOCYTES = ABSOLUTE_MONOCYTE = ABSOLUTE_NEUTROPHIL_BAND = ABSOLUTE_NEUTROPHIL_SEG = ABSOLUTE_PROMYELOCYTES = ALBUMIN =
				ALKALINE_PHOSPHATASE = ANISOCYTOSIS = B_C_RATIO = BASOPHIL = BICARBONATE = BUN = CALCIUM = CHLORIDE = CHOLESTEROL = CPK = CREATININE = 
				DIRECT_BILIRUBIN = EOSINOPHIL = GLOBULIN = GLUCOSE = HCT = HEINZ_BODIES = HEMOLYSIS_INDEX = HGB = INDIRECT_BILIRUBIN = LIPEMIA_INDEX = LYMPHOCYTE 
				= MCH = MCHC = MCV = METAMYELOCYTE = MONOCYTE = MYELOCYTE = NA_K_RATIO = NEUTROPHIL_BAND = NEUTROPHIL_SEG = NRBC = PHOSPHORUS = PLATELET_COUNT 
				= PLATELET_ESTIMATE = POIKILOCYTOSIS = POLYCHROMASIA = POTASSIUM = PROMYELOCYTE = RBC = SGOT_AST = SGPT_ALT = SODIUM = TOTAL_BILIRUBIN =
				TOTAL_PROTEIN = WBC = -1;
		String[] headers = header.split(",");
		int i = 0;
		for (String string : headers)
		{
			if (string.equalsIgnoreCase("ID"))
			{
				setID(i);
			} else if (string.equalsIgnoreCase("OLDID"))
			{
				setOLDID(i);
			} else if (string.equalsIgnoreCase("SPECIES"))
			{
				setSPECIES(i);
			} else if (string.equalsIgnoreCase("SEX"))
			{
				setSEX(i);
			} else if (string.equalsIgnoreCase("AGECATEGORY"))
			{
				setAGECATEGORY(i);
			} else if (string.equalsIgnoreCase("CLIENT"))
			{
				setCLIENT(i);
			} else if (string.equalsIgnoreCase("DOB"))
			{
				setDOB(i);
			} else if (string.equalsIgnoreCase("SOURCE"))
			{
				setSOURCE(i);
			} else if (string.equalsIgnoreCase("RECEIVEDDATE"))
			{
				setRECEIVEDDATE(i);
			} else if (string.equalsIgnoreCase("DEPARTEDDATE"))
			{
				setDEPARTEDDATE(i);
			} else if (string.equalsIgnoreCase("ROOMNUM"))
			{
				setROOMNUM(i);
			} else if (string.equalsIgnoreCase("VIRUS"))
			{
				setVIRUS(i);
			} else if (string.equalsIgnoreCase("WEIGHT"))
			{
				setWEIGHT(i);
			} else if (string.equalsIgnoreCase("REMARKS"))
			{
				setREMARKS(i);
			} else if (string.equalsIgnoreCase("ORIGIN"))
			{
				setORIGIN(i);
			}

			if (string.equalsIgnoreCase("Accession Number"))
			{
				setAccession_Number(i);
			}
			if (string.equalsIgnoreCase("Group Number"))
			{
				setGroup_Number(i);
			}
			if (string.equalsIgnoreCase("Animal Id"))
			{
				setAnimal_Id(i);
			}
			if (string.equalsIgnoreCase("Age"))
			{
				setAge(i);
			}
			if (string.equalsIgnoreCase("Breed"))
			{
				setBreed(i);
			}
			if (string.equalsIgnoreCase("Collected Date"))
			{
				setCollected_Date(i);
			}
			if (string.equalsIgnoreCase("A/G RATIO"))
			{
				setA_G_RATIO(i);
			}
			if (string.equalsIgnoreCase("ABSOLUTE BASOPHIL"))
			{
				setABSOLUTE_BASOPHIL(i);
			}
			if (string.equalsIgnoreCase("ABSOLUTE EOSINOPHIL"))
			{
				setABSOLUTE_EOSINOPHIL(i);
			}
			if (string.equalsIgnoreCase("ABSOLUTE LYMPHOCYTE"))
			{
				setABSOLUTE_LYMPHOCYTE(i);
			}
			if (string.equalsIgnoreCase("ABSOLUTE METAMYELOCYTES"))
			{
				setABSOLUTE_METAMYELOCYTES(i);
			}
			if (string.equalsIgnoreCase("ABSOLUTE MONOCYTE"))
			{
				setABSOLUTE_MONOCYTE(i);
			}
			if (string.equalsIgnoreCase("ABSOLUTE NEUTROPHIL BAND"))
			{
				setABSOLUTE_NEUTROPHIL_BAND(i);
			}
			if (string.equalsIgnoreCase("ABSOLUTE NEUTROPHIL SEG"))
			{
				setABSOLUTE_NEUTROPHIL_SEG(i);
			}
			if (string.equalsIgnoreCase("ABSOLUTE PROMYELOCYTES"))
			{
				setABSOLUTE_PROMYELOCYTES(i);
			}
			if (string.equalsIgnoreCase("ALBUMIN"))
			{
				setALBUMIN(i);
			}
			if (string.equalsIgnoreCase("ALKALINE PHOSPHATASE"))
			{
				setALKALINE_PHOSPHATASE(i);
			}
			if (string.equalsIgnoreCase("ANISOCYTOSIS"))
			{
				setANISOCYTOSIS(i);
			}
			if (string.equalsIgnoreCase("B/C RATIO"))
			{
				setB_C_RATIO(i);
			}
			if (string.equalsIgnoreCase("BASOPHIL"))
			{
				setBASOPHIL(i);
			}
			if (string.equalsIgnoreCase("BICARBONATE"))
			{
				setBICARBONATE(i);
			}
			if (string.equalsIgnoreCase("BUN"))
			{
				setBUN(i);
			}
			if (string.equalsIgnoreCase("CALCIUM"))
			{
				setCALCIUM(i);
			}
			if (string.equalsIgnoreCase("CHLORIDE"))
			{
				setCHLORIDE(i);
			}
			if (string.equalsIgnoreCase("CHOLESTEROL"))
			{
				setCHOLESTEROL(i);
			}
			if (string.equalsIgnoreCase("CPK"))
			{
				setCPK(i);
			}
			if (string.equalsIgnoreCase("CREATININE"))
			{
				setCREATININE(i);
			}
			if (string.equalsIgnoreCase("DIRECT BILIRUBIN"))
			{
				setDIRECT_BILIRUBIN(i);
			}
			if (string.equalsIgnoreCase("EOSINOPHIL"))
			{
				setEOSINOPHIL(i);
			}
			if (string.equalsIgnoreCase("GLOBULIN"))
			{
				setGLOBULIN(i);
			}
			if (string.equalsIgnoreCase("GLUCOSE"))
			{
				setGLUCOSE(i);
			}
			if (string.equalsIgnoreCase("HCT"))
			{
				setHCT(i);
			}
			if (string.equalsIgnoreCase("HEINZ BODIES"))
			{
				setHEINZ_BODIES(i);
			}
			if (string.equalsIgnoreCase("HEMOLYSIS INDEX"))
			{
				setHEMOLYSIS_INDEX(i);
			}
			if (string.equalsIgnoreCase("HGB"))
			{
				setHGB(i);
			}
			if (string.equalsIgnoreCase("INDIRECT BILIRUBIN"))
			{
				setINDIRECT_BILIRUBIN(i);
			}
			if (string.equalsIgnoreCase("LIPEMIA INDEX"))
			{
				setLIPEMIA_INDEX(i);
			}
			if (string.equalsIgnoreCase("LYMPHOCYTE"))
			{
				setLYMPHOCYTE(i);
			}

			if (string.equalsIgnoreCase("MCH"))
			{
				setMCH(i);
			}
			if (string.equalsIgnoreCase("MCHC"))
			{
				setMCHC(i);
			}
			if (string.equalsIgnoreCase("MCV"))
			{
				setMCV(i);
			}
			if (string.equalsIgnoreCase("METAMYELOCYTE"))
			{
				setMETAMYELOCYTE(i);
			}
			if (string.equalsIgnoreCase("MONOCYTE"))
			{
				setMONOCYTE(i);
			}
			if (string.equalsIgnoreCase("MYELOCYTE"))
			{
				setMYELOCYTE(i);
			}
			if (string.equalsIgnoreCase("NA/K RATIO"))
			{
				setNA_K_RATIO(i);
			}
			if (string.equalsIgnoreCase("NEUTROPHIL BAND"))
			{
				setNEUTROPHIL_BAND(i);
			}
			if (string.equalsIgnoreCase("NEUTROPHIL SEG"))
			{
				setNEUTROPHIL_SEG(i);
			}
			if (string.equalsIgnoreCase("NRBC"))
			{
				setNRBC(i);
			}

			if (string.equalsIgnoreCase("PHOSPHORUS"))
			{
				setPHOSPHORUS(i);
			}

			if (string.equalsIgnoreCase("PLATELET COUNT"))
			{
				setPLATELET_COUNT(i);
			}
			if (string.equalsIgnoreCase("PLATELET ESTIMATE"))
			{
				setPLATELET_ESTIMATE(i);
			}
			if (string.equalsIgnoreCase("POIKILOCYTOSIS"))
			{
				setPOIKILOCYTOSIS(i);
			}
			if (string.equalsIgnoreCase("POLYCHROMASIA"))
			{
				setPOLYCHROMASIA(i);
			}
			if (string.equalsIgnoreCase("POTASSIUM"))
			{
				setPOTASSIUM(i);
			}
			if (string.equalsIgnoreCase("PROMYELOCYTE"))
			{
				setPROMYELOCYTE(i);
			}
			if (string.equalsIgnoreCase("RBC"))
			{
				setRBC(i);
			}
			if (string.equalsIgnoreCase("REMARKS"))
			{
				REMARKS = i;
			}
			if (string.equalsIgnoreCase("SGOT (AST)"))
			{
				setSGOT_AST(i);
			}
			if (string.equalsIgnoreCase("SGPT (ALT)"))
			{
				setSGPT_ALT(i);
			}
			if (string.equalsIgnoreCase("SODIUM"))
			{
				setSODIUM(i);
			}
			if (string.equalsIgnoreCase("TOTAL BILIRUBIN"))
			{
				setTOTAL_BILIRUBIN(i);
			}
			if (string.equalsIgnoreCase("TOTAL PROTEIN"))
			{
				setTOTAL_PROTEIN(i);
			}
			if (string.equalsIgnoreCase("WBC"))
			{
				setWBC(i);
			}
			if (string.equalsIgnoreCase("REMARKS"))
			{
				REMARKS = i;
			}

			i++;
		}
	}

	public int getID()
	{
		return ID;
	}

	public void setID(int iD)
	{
		ID = iD;
	}

	public int getOLDID()
	{
		return OLDID;
	}

	public void setOLDID(int oLDID)
	{
		OLDID = oLDID;
	}

	public int getSPECIES()
	{
		return SPECIES;
	}

	public void setSPECIES(int sPECIES)
	{
		SPECIES = sPECIES;
	}

	public int getSEX()
	{
		return SEX;
	}

	public void setSEX(int sEX)
	{
		SEX = sEX;
	}

	public int getAGECATEGORY()
	{
		return AGECATEGORY;
	}

	public void setAGECATEGORY(int aGECATEGORY)
	{
		AGECATEGORY = aGECATEGORY;
	}

	public int getCLIENT()
	{
		return CLIENT;
	}

	public void setCLIENT(int cLIENT)
	{
		CLIENT = cLIENT;
	}

	public int getDOB()
	{
		return DOB;
	}

	public void setDOB(int dOB)
	{
		DOB = dOB;
	}

	public int getSOURCE()
	{
		return SOURCE;
	}

	public void setSOURCE(int sOURCE)
	{
		SOURCE = sOURCE;
	}

	public int getRECEIVEDDATE()
	{
		return RECEIVEDDATE;
	}

	public void setRECEIVEDDATE(int rECEIVEDDATE)
	{
		RECEIVEDDATE = rECEIVEDDATE;
	}

	public int getDEPARTEDDATE()
	{
		return DEPARTEDDATE;
	}

	public void setDEPARTEDDATE(int dEPARTEDDATE)
	{
		DEPARTEDDATE = dEPARTEDDATE;
	}

	public int getROOMNUM()
	{
		return ROOMNUM;
	}

	public void setROOMNUM(int rOOMNUM)
	{
		ROOMNUM = rOOMNUM;
	}

	public int getVIRUS()
	{
		return VIRUS;
	}

	public void setVIRUS(int vIRUS)
	{
		VIRUS = vIRUS;
	}

	public int getWEIGHT()
	{
		return WEIGHT;
	}

	public void setWEIGHT(int wEIGHT)
	{
		WEIGHT = wEIGHT;
	}

	public int getREMARKS()
	{
		return REMARKS;
	}

	public void setREMARKS(int rEMARKS)
	{
		REMARKS = rEMARKS;
	}

	public int getAccession_Number()
	{
		return Accession_Number;
	}

	public void setAccession_Number(int accession_Number)
	{
		Accession_Number = accession_Number;
	}

	public int getGroup_Number()
	{
		return Group_Number;
	}

	public void setGroup_Number(int group_Number)
	{
		Group_Number = group_Number;
	}

	public int getABSOLUTE_LYMPHOCYTE()
	{
		return ABSOLUTE_LYMPHOCYTE;
	}

	public void setABSOLUTE_LYMPHOCYTE(int aBSOLUTE_LYMPHOCYTE)
	{
		ABSOLUTE_LYMPHOCYTE = aBSOLUTE_LYMPHOCYTE;
	}

	public int getAnimal_Id()
	{
		return Animal_Id;
	}

	public void setAnimal_Id(int animal_Id)
	{
		Animal_Id = animal_Id;
	}

	public int getAge()
	{
		return Age;
	}

	public void setAge(int age)
	{
		Age = age;
	}

	public int getBreed()
	{
		return Breed;
	}

	public void setBreed(int breed)
	{
		Breed = breed;
	}

	public int getCollected_Date()
	{
		return Collected_Date;
	}

	public void setCollected_Date(int collected_Date)
	{
		Collected_Date = collected_Date;
	}

	public int getA_G_RATIO()
	{
		return A_G_RATIO;
	}

	public void setA_G_RATIO(int a_G_RATIO)
	{
		A_G_RATIO = a_G_RATIO;
	}

	public int getABSOLUTE_BASOPHIL()
	{
		return ABSOLUTE_BASOPHIL;
	}

	public void setABSOLUTE_BASOPHIL(int aBSOLUTE_BASOPHIL)
	{
		ABSOLUTE_BASOPHIL = aBSOLUTE_BASOPHIL;
	}

	public int getABSOLUTE_EOSINOPHIL()
	{
		return ABSOLUTE_EOSINOPHIL;
	}

	public void setABSOLUTE_EOSINOPHIL(int aBSOLUTE_EOSINOPHIL)
	{
		ABSOLUTE_EOSINOPHIL = aBSOLUTE_EOSINOPHIL;
	}

	public int getABSOLUTE_PROMYELOCYTES()
	{
		return ABSOLUTE_PROMYELOCYTES;
	}

	public void setABSOLUTE_PROMYELOCYTES(int aBSOLUTE_PROMYELOCYTES)
	{
		ABSOLUTE_PROMYELOCYTES = aBSOLUTE_PROMYELOCYTES;
	}

	public int getABSOLUTE_METAMYELOCYTES()
	{
		return ABSOLUTE_METAMYELOCYTES;
	}

	public void setABSOLUTE_METAMYELOCYTES(int aBSOLUTE_METAMYELOCYTES)
	{
		ABSOLUTE_METAMYELOCYTES = aBSOLUTE_METAMYELOCYTES;
	}

	public int getABSOLUTE_MONOCYTE()
	{
		return ABSOLUTE_MONOCYTE;
	}

	public void setABSOLUTE_MONOCYTE(int aBSOLUTE_MONOCYTE)
	{
		ABSOLUTE_MONOCYTE = aBSOLUTE_MONOCYTE;
	}

	public int getABSOLUTE_NEUTROPHIL_BAND()
	{
		return ABSOLUTE_NEUTROPHIL_BAND;
	}

	public void setABSOLUTE_NEUTROPHIL_BAND(int aBSOLUTE_NEUTROPHIL_BAND)
	{
		ABSOLUTE_NEUTROPHIL_BAND = aBSOLUTE_NEUTROPHIL_BAND;
	}

	public int getABSOLUTE_NEUTROPHIL_SEG()
	{
		return ABSOLUTE_NEUTROPHIL_SEG;
	}

	public void setABSOLUTE_NEUTROPHIL_SEG(int aBSOLUTE_NEUTROPHIL_SEG)
	{
		ABSOLUTE_NEUTROPHIL_SEG = aBSOLUTE_NEUTROPHIL_SEG;
	}

	public int getALBUMIN()
	{
		return ALBUMIN;
	}

	public void setALBUMIN(int aLBUMIN)
	{
		ALBUMIN = aLBUMIN;
	}

	public int getDIRECT_BILIRUBIN()
	{
		return DIRECT_BILIRUBIN;
	}

	public void setDIRECT_BILIRUBIN(int dIRECT_BILIRUBIN)
	{
		DIRECT_BILIRUBIN = dIRECT_BILIRUBIN;
	}

	public int getALKALINE_PHOSPHATASE()
	{
		return ALKALINE_PHOSPHATASE;
	}

	public void setALKALINE_PHOSPHATASE(int aLKALINE_PHOSPHATASE)
	{
		ALKALINE_PHOSPHATASE = aLKALINE_PHOSPHATASE;
	}

	public int getANISOCYTOSIS()
	{
		return ANISOCYTOSIS;
	}

	public void setANISOCYTOSIS(int aNISOCYTOSIS)
	{
		ANISOCYTOSIS = aNISOCYTOSIS;
	}

	public int getB_C_RATIO()
	{
		return B_C_RATIO;
	}

	public void setB_C_RATIO(int b_C_RATIO)
	{
		B_C_RATIO = b_C_RATIO;
	}

	public int getBASOPHIL()
	{
		return BASOPHIL;
	}

	public void setBASOPHIL(int bASOPHIL)
	{
		BASOPHIL = bASOPHIL;
	}

	public int getBICARBONATE()
	{
		return BICARBONATE;
	}

	public void setBICARBONATE(int bICARBONATE)
	{
		BICARBONATE = bICARBONATE;
	}

	public int getBUN()
	{
		return BUN;
	}

	public void setBUN(int bUN)
	{
		BUN = bUN;
	}

	public int getCALCIUM()
	{
		return CALCIUM;
	}

	public void setCALCIUM(int cALCIUM)
	{
		CALCIUM = cALCIUM;
	}

	public int getCHLORIDE()
	{
		return CHLORIDE;
	}

	public void setCHLORIDE(int cHLORIDE)
	{
		CHLORIDE = cHLORIDE;
	}

	public int getCHOLESTEROL()
	{
		return CHOLESTEROL;
	}

	public void setCHOLESTEROL(int cHOLESTEROL)
	{
		CHOLESTEROL = cHOLESTEROL;
	}

	public int getCPK()
	{
		return CPK;
	}

	public void setCPK(int cPK)
	{
		CPK = cPK;
	}

	public int getCREATININE()
	{
		return CREATININE;
	}

	public void setCREATININE(int cREATININE)
	{
		CREATININE = cREATININE;
	}

	public int getEOSINOPHIL()
	{
		return EOSINOPHIL;
	}

	public void setEOSINOPHIL(int eOSINOPHIL)
	{
		EOSINOPHIL = eOSINOPHIL;
	}

	public int getGLUCOSE()
	{
		return GLUCOSE;
	}

	public void setGLUCOSE(int gLUCOSE)
	{
		GLUCOSE = gLUCOSE;
	}

	public int getGLOBULIN()
	{
		return GLOBULIN;
	}

	public void setGLOBULIN(int gLOBULIN)
	{
		GLOBULIN = gLOBULIN;
	}

	public int getMETAMYELOCYTE()
	{
		return METAMYELOCYTE;
	}

	public void setMETAMYELOCYTE(int mETAMYELOCYTE)
	{
		METAMYELOCYTE = mETAMYELOCYTE;
	}

	public int getINDIRECT_BILIRUBIN()
	{
		return INDIRECT_BILIRUBIN;
	}

	public void setINDIRECT_BILIRUBIN(int iNDIRECT_BILIRUBIN)
	{
		INDIRECT_BILIRUBIN = iNDIRECT_BILIRUBIN;
	}

	public int getSGOT_AST()
	{
		return SGOT_AST;
	}

	public void setSGOT_AST(int sGOT_AST)
	{
		SGOT_AST = sGOT_AST;
	}

	public int getPHOSPHORUS()
	{
		return PHOSPHORUS;
	}

	public void setPHOSPHORUS(int pHOSPHORUS)
	{
		PHOSPHORUS = pHOSPHORUS;
	}

	public int getTOTAL_BILIRUBIN()
	{
		return TOTAL_BILIRUBIN;
	}

	public void setTOTAL_BILIRUBIN(int tOTAL_BILIRUBIN)
	{
		TOTAL_BILIRUBIN = tOTAL_BILIRUBIN;
	}

	public int getHEINZ_BODIES()
	{
		return HEINZ_BODIES;
	}

	public void setHEINZ_BODIES(int hEINZ_BODIES)
	{
		HEINZ_BODIES = hEINZ_BODIES;
	}

	public int getHCT()
	{
		return HCT;
	}

	public void setHCT(int hCT)
	{
		HCT = hCT;
	}

	public int getNEUTROPHIL_BAND()
	{
		return NEUTROPHIL_BAND;
	}

	public void setNEUTROPHIL_BAND(int nEUTROPHIL_BAND)
	{
		NEUTROPHIL_BAND = nEUTROPHIL_BAND;
	}

	public int getPROMYELOCYTE()
	{
		return PROMYELOCYTE;
	}

	public void setPROMYELOCYTE(int pROMYELOCYTE)
	{
		PROMYELOCYTE = pROMYELOCYTE;
	}

	public int getRBC()
	{
		return RBC;
	}

	public void setRBC(int rBC)
	{
		RBC = rBC;
	}

	public int getSGPT_ALT()
	{
		return SGPT_ALT;
	}

	public void setSGPT_ALT(int sGPT_ALT)
	{
		SGPT_ALT = sGPT_ALT;
	}

	public int getSODIUM()
	{
		return SODIUM;
	}

	public void setSODIUM(int sODIUM)
	{
		SODIUM = sODIUM;
	}

	public int getTOTAL_PROTEIN()
	{
		return TOTAL_PROTEIN;
	}

	public void setTOTAL_PROTEIN(int tOTAL_PROTEIN)
	{
		TOTAL_PROTEIN = tOTAL_PROTEIN;
	}

	public int getLIPEMIA_INDEX()
	{
		return LIPEMIA_INDEX;
	}

	public void setLIPEMIA_INDEX(int lIPEMIA_INDEX)
	{
		LIPEMIA_INDEX = lIPEMIA_INDEX;
	}

	public int getLYMPHOCYTE()
	{
		return LYMPHOCYTE;
	}

	public void setLYMPHOCYTE(int lYMPHOCYTE)
	{
		LYMPHOCYTE = lYMPHOCYTE;
	}

	public int getMCH()
	{
		return MCH;
	}

	public void setMCH(int mCH)
	{
		MCH = mCH;
	}

	public int getPLATELET_ESTIMATE()
	{
		return PLATELET_ESTIMATE;
	}

	public void setPLATELET_ESTIMATE(int pLATELET_ESTIMATE)
	{
		PLATELET_ESTIMATE = pLATELET_ESTIMATE;
	}

	public int getPOIKILOCYTOSIS()
	{
		return POIKILOCYTOSIS;
	}

	public void setPOIKILOCYTOSIS(int pOIKILOCYTOSIS)
	{
		POIKILOCYTOSIS = pOIKILOCYTOSIS;
	}

	public int getMCHC()
	{
		return MCHC;
	}

	public void setMCHC(int mCHC)
	{
		MCHC = mCHC;
	}

	public int getMONOCYTE()
	{
		return MONOCYTE;
	}

	public void setMONOCYTE(int mONOCYTE)
	{
		MONOCYTE = mONOCYTE;
	}

	public int getMYELOCYTE()
	{
		return MYELOCYTE;
	}

	public void setMYELOCYTE(int mYELOCYTE)
	{
		MYELOCYTE = mYELOCYTE;
	}

	public int getHEMOLYSIS_INDEX()
	{
		return HEMOLYSIS_INDEX;
	}

	public void setHEMOLYSIS_INDEX(int hEMOLYSIS_INDEX)
	{
		HEMOLYSIS_INDEX = hEMOLYSIS_INDEX;
	}

	public int getHGB()
	{
		return HGB;
	}

	public void setHGB(int hGB)
	{
		HGB = hGB;
	}

	public int getNEUTROPHIL_SEG()
	{
		return NEUTROPHIL_SEG;
	}

	public void setNEUTROPHIL_SEG(int nEUTROPHIL_SEG)
	{
		NEUTROPHIL_SEG = nEUTROPHIL_SEG;
	}

	public int getPLATELET_COUNT()
	{
		return PLATELET_COUNT;
	}

	public void setPLATELET_COUNT(int pLATELET_COUNT)
	{
		PLATELET_COUNT = pLATELET_COUNT;
	}

	public int getNRBC()
	{
		return NRBC;
	}

	public void setNRBC(int nRBC)
	{
		NRBC = nRBC;
	}

	public int getMCV()
	{
		return MCV;
	}

	public void setMCV(int mCV)
	{
		MCV = mCV;
	}

	public int getPOLYCHROMASIA()
	{
		return POLYCHROMASIA;
	}

	public void setPOLYCHROMASIA(int pOLYCHROMASIA)
	{
		POLYCHROMASIA = pOLYCHROMASIA;
	}

	public int getNA_K_RATIO()
	{
		return NA_K_RATIO;
	}

	public void setNA_K_RATIO(int nA_K_RATIO)
	{
		NA_K_RATIO = nA_K_RATIO;
	}

	public int getPOTASSIUM()
	{
		return POTASSIUM;
	}

	public void setPOTASSIUM(int pOTASSIUM)
	{
		POTASSIUM = pOTASSIUM;
	}

	public int getWBC()
	{
		return WBC;
	}

	public void setWBC(int wBC)
	{
		WBC = wBC;
	}

	public int getORIGIN()
	{
		return ORIGIN;
	}

	public void setORIGIN(int oRIGIN)
	{
		ORIGIN = oRIGIN;
	}
}
