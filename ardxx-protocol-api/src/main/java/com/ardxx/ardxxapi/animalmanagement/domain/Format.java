package com.ardxx.ardxxapi.animalmanagement.domain;

public enum Format
{
	ANIMAL, MAMU, ELISA, CBC
}
