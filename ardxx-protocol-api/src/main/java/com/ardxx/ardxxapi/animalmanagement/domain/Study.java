package com.ardxx.ardxxapi.animalmanagement.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.ardxx.ardxxapi.protocol.domain.Protocol;

/**
 * 
 * @author Raj
 *
 */
@Entity
public class Study implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String serialNumber;

	private String name;

	private String chargeCode;

	private Date createDate;

	private Date closedDate;

	private String comment;
	
	private boolean isClinical = false;
	
	private boolean isDraft = false;
	
	@ManyToOne
	private Protocol protocol;

	@ElementCollection
	private List<Animals> animals;

	@ElementCollection
	private List<AnimalGroup> animalGroups;
	
	@ElementCollection
	private List<StudyProcedure> procedures;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getSerialNumber()
	{
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber)
	{
		this.serialNumber = serialNumber;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getChargeCode()
	{
		return chargeCode;
	}

	public void setChargeCode(String chargeCode)
	{
		this.chargeCode = chargeCode;
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}

	public Date getClosedDate()
	{
		return closedDate;
	}

	public void setClosedDate(Date closedDate)
	{
		this.closedDate = closedDate;
	}

	public String getComment()
	{
		return comment;
	}

	public void setComment(String comment)
	{
		this.comment = comment;
	}

	public Protocol getProtocol()
	{
		return protocol;
	}

	public void setProtocol(Protocol protocol)
	{
		this.protocol = protocol;
	}

	public List<Animals> getAnimals()
	{
		return animals;
	}

	public void setAnimals(List<Animals> animals)
	{
		this.animals = animals;
	}

	public List<AnimalGroup> getAnimalGroups()
	{
		return animalGroups;
	}

	public void setAnimalGroups(List<AnimalGroup> animalGroups)
	{
		this.animalGroups = animalGroups;
	}

	public List<StudyProcedure> getProcedures()
	{
		return procedures;
	}

	public void setProcedures(List<StudyProcedure> procedures)
	{
		this.procedures = procedures;
	}

	public boolean isClinical()
	{
		return isClinical;
	}

	public void setClinical(boolean isClinical)
	{
		this.isClinical = isClinical;
	}

	public boolean isDraft()
	{
		return isDraft;
	}

	public void setDraft(boolean isDraft)
	{
		this.isDraft = isDraft;
	}

}
