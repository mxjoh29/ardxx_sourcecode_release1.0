package com.ardxx.ardxxapi.animalmanagement.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ProcedureTest implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String test;
	private String destination;
	private String storageRequirements;
	private String packagingRequirements;
	private Date timeShipppedBy;
	private String shippedTo;
	private String notes;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getTest()
	{
		return test;
	}

	public void setTest(String test)
	{
		this.test = test;
	}

	public String getDestination()
	{
		return destination;
	}

	public void setDestination(String destination)
	{
		this.destination = destination;
	}

	public String getStorageRequirements()
	{
		return storageRequirements;
	}

	public void setStorageRequirements(String storageRequirements)
	{
		this.storageRequirements = storageRequirements;
	}

	public String getPackagingRequirements()
	{
		return packagingRequirements;
	}

	public void setPackagingRequirements(String packagingRequirements)
	{
		this.packagingRequirements = packagingRequirements;
	}

	public Date getTimeShipppedBy()
	{
		return timeShipppedBy;
	}

	public void setTimeShipppedBy(Date timeShipppedBy)
	{
		this.timeShipppedBy = timeShipppedBy;
	}

	public String getShippedTo()
	{
		return shippedTo;
	}

	public void setShippedTo(String shippedTo)
	{
		this.shippedTo = shippedTo;
	}

	public String getNotes()
	{
		return notes;
	}

	public void setNotes(String notes)
	{
		this.notes = notes;
	}

}
