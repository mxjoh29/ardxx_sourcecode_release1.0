package com.ardxx.ardxxapi.animalmanagement.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ardxx.ardxxapi.animalmanagement.domain.Virus;

/**
 * 
 * @author Raj
 *
 */
@Repository
public interface VirusRepository extends PagingAndSortingRepository<Virus, Long>
{

}
