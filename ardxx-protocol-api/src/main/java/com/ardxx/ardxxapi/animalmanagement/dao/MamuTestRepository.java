package com.ardxx.ardxxapi.animalmanagement.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.ardxx.ardxxapi.animalmanagement.domain.MamuTest;

public interface MamuTestRepository extends PagingAndSortingRepository<MamuTest, Long>
{
	@Query("from MamuTest AS mamu where mamu.animals.animalId=:animalId")
	Iterable<MamuTest> getMamuTestByAnimalId(@Param("animalId") String animalId);
}
