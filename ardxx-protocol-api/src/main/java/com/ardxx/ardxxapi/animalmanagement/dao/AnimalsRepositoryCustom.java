package com.ardxx.ardxxapi.animalmanagement.dao;

import org.springframework.stereotype.Repository;

@Repository
public interface AnimalsRepositoryCustom
{
	public Long getNextAnimalId();
}
