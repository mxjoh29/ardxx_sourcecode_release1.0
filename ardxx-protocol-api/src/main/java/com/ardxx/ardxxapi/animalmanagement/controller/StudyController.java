package com.ardxx.ardxxapi.animalmanagement.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ardxx.ardxxapi.animalmanagement.domain.AnimalGroup;
import com.ardxx.ardxxapi.animalmanagement.domain.AnimalHistory;
import com.ardxx.ardxxapi.animalmanagement.domain.Animals;
import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureSchedule;
import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureTest;
import com.ardxx.ardxxapi.animalmanagement.domain.Study;
import com.ardxx.ardxxapi.animalmanagement.domain.StudyProcedure;
import com.ardxx.ardxxapi.animalmanagement.dto.AnimalsTPRWDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.ScheduleAnimalDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.ScheduleCloneDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.StudyDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.StudyProcedureDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.VolumeDTO;
import com.ardxx.ardxxapi.animalmanagement.service.ProcedureService;
import com.ardxx.ardxxapi.animalmanagement.service.StudyService;
import com.ardxx.ardxxapi.common.ConstantsMap;
import com.ardxx.ardxxapi.common.domain.Constants;
import com.ardxx.ardxxapi.exception.ARDXXAppException;
import com.ardxx.ardxxapi.exception.ARDXXErrorPayload;

@RestController
@RequestMapping(value = "/study")
public class StudyController
{
	@Autowired
	StudyService studyService;

	@Autowired
	ProcedureService procedureService;

	@Autowired
	ConstantsMap constantsMap;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Object> createStudy(@RequestBody StudyDTO studyDTO)
	{
		try
		{
			Study study = studyService.createStudy(studyDTO);
			return new ResponseEntity<Object>(study, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

	}

	@RequestMapping(value = "/{studyid}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateStudy(@PathVariable("studyid") Long studyId, @RequestBody StudyDTO studyDTO)
	{
		try
		{
			Study study = studyService.updateStudy(studyId, studyDTO);
			return new ResponseEntity<Object>(study, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Object> getAllStudies(@RequestParam int page, @RequestParam int size)
	{
		try
		{
			Page<Study> studies = studyService.getStudies(page, size);
			return new ResponseEntity<Object>(studies, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

	}
	
	@RequestMapping(value = "/clinicalProcedure",method = RequestMethod.GET)
	public ResponseEntity<Object> getClinicalProcedures(@RequestParam int page, @RequestParam int size)
	{
		try
		{
			Page<StudyProcedure> studies = procedureService.getClinicalProcedures(page, size);
			return new ResponseEntity<Object>(studies, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}
	
	@RequestMapping(value = "/{studyid}", method = RequestMethod.GET)
	public ResponseEntity<Object> getStudy(@PathVariable("studyid") Long studyId)
	{
		try
		{
			Study study = studyService.getStudy(studyId);
			return new ResponseEntity<Object>(study, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

	}

	@RequestMapping(value = "/{studyid}/animals", method = RequestMethod.PUT)
	public ResponseEntity<Object> addAnimals(@PathVariable("studyid") Long studyId, @RequestBody StudyDTO studyDTO)
	{
		try
		{
			Study study = studyService.addAnimals(studyId, studyDTO);
			return new ResponseEntity<Object>(study, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

	}

	@RequestMapping(value = "/{studyid}/animals", method = RequestMethod.GET)
	public ResponseEntity<Object> getAnimals(@PathVariable("studyid") Long studyId)
	{
		try
		{
			List<Animals> animals = studyService.getAnimals(studyId);
			return new ResponseEntity<Object>(animals, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/{studyid}/group", method = RequestMethod.PUT)
	public ResponseEntity<Object> createAnimalsGroup(@PathVariable("studyid") Long studyId, @RequestBody AnimalGroup animalGroup)
	{
		try
		{
			List<AnimalGroup> animalGroups = studyService.createAnimalsGroup(studyId, animalGroup);
			return new ResponseEntity<Object>(animalGroups, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/{studyid}/group/{groupid}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deleteAnimalsGroup(@PathVariable("studyid") Long studyId, @PathVariable("groupid") Long groupId)
	{
		try
		{
			List<AnimalGroup> animalGroups = studyService.deleteAnimalsGroup(studyId, groupId);
			return new ResponseEntity<Object>(animalGroups, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/{studyid}/group/{groupid}/assign", method = RequestMethod.PUT)
	public ResponseEntity<Object> assignAnimalsGroup(@PathVariable("studyid") Long studyId, @PathVariable("groupid") Long groupId,
			@RequestBody StudyDTO studyDTO)
	{
		try
		{
			Study study = studyService.assignAnimalsGroup(studyId, groupId, studyDTO);
			return new ResponseEntity<Object>(study, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/{studyid}/group/{groupid}/animals", method = RequestMethod.GET)
	public ResponseEntity<Object> assignAnimalsGroup(@PathVariable("studyid") Long studyId, @PathVariable("groupid") Long groupId)
	{
		try
		{
			List<Animals> animals = studyService.getAnimalsInGroup(studyId, groupId);
			return new ResponseEntity<Object>(animals, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/{studyid}/group/{groupid}/unassign", method = RequestMethod.PUT)
	public ResponseEntity<Object> unAssignAnimalsGroup(@PathVariable("studyid") Long studyId, @PathVariable("groupid") Long groupId,
			@RequestBody StudyDTO studyDTO)
	{
		try
		{
			Study study = studyService.unassignAnimalsGroup(studyId, groupId, studyDTO);
			return new ResponseEntity<Object>(study, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/{studyid}/group", method = RequestMethod.GET)
	public ResponseEntity<Object> getAnimalsGroups(@PathVariable("studyid") Long studyId)
	{
		try
		{
			List<AnimalGroup> animalGroups = studyService.getAnimalsGroups(studyId);
			return new ResponseEntity<Object>(animalGroups, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/{studyid}/group/{groupid}", method = RequestMethod.GET)
	public ResponseEntity<Object> getAnimalsGroupById(@PathVariable("studyid") Long studyId, @PathVariable("groupid") Long groupId)
	{
		try
		{
			AnimalGroup animalGroups = studyService.getAnimalsGroupById(studyId, groupId);
			return new ResponseEntity<Object>(animalGroups, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/{studyid}/animals", method = RequestMethod.DELETE)
	public ResponseEntity<Object> removeAnimalsInStudy(@PathVariable("studyid") Long studyId, @RequestBody StudyDTO studyDTO)
	{
		try
		{
			Study study = studyService.removeAnimalsInStudy(studyId, studyDTO);

			return new ResponseEntity<Object>(study, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/procedure/{procedureid}", method = RequestMethod.GET)
	public ResponseEntity<Object> getProcedure(@PathVariable("procedureid") Long procedureId)
	{
		try
		{
			StudyProcedure procedure = procedureService.getProcedure(procedureId);
			return new ResponseEntity<Object>(procedure, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

	}

	@RequestMapping(value = "/procedure", method = RequestMethod.GET)
	public ResponseEntity<Object> getProcedureList(@RequestParam int page, @RequestParam int size)
	{
		try
		{
			Page<StudyProcedure> procedure = procedureService.getProcedureList(page, size);
			return new ResponseEntity<Object>(procedure, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

	}

	@RequestMapping(value = "/{studyid}/procedure", method = RequestMethod.PUT)
	public ResponseEntity<Object> createProcedure(@PathVariable("studyid") Long studyId, @RequestBody StudyProcedure procedure)
	{
		try
		{
			procedure = procedureService.createProcedure(studyId, procedure);
			return new ResponseEntity<Object>(procedure, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

	}

	@RequestMapping(value = "/{studyid}/procedure/{procedureid}/status/{status}", method = RequestMethod.PUT)
	public ResponseEntity<Object> changeProcedureStatus(@PathVariable("studyid") Long studyId, @PathVariable("procedureid") Long procedureId,
			@PathVariable("status") String status)
	{
		try
		{
			StudyProcedure procedure = procedureService.changeProcedureStatus(studyId, procedureId, status);
			return new ResponseEntity<Object>(procedure, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

	}
//	@PreAuthorize("@animalManagementUserServiceImpl.canEditProcedureDetails(principal, #procedureTest)")
	@RequestMapping(value = "/{studyid}/procedure/{procedureid}/tests", method = RequestMethod.PUT)
	public ResponseEntity<Object> createProcedureTests(@PathVariable("studyid") Long studyId, @PathVariable("procedureid") Long procedureId,
			@RequestBody ProcedureTest procedureTest)
	{
		try
		{
			StudyProcedure procedure = procedureService.createProcedureTests(studyId, procedureId, procedureTest);
			return new ResponseEntity<Object>(procedure, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

	}

	@RequestMapping(value = "/procedure/type", method = RequestMethod.GET)
	public ResponseEntity<Object> getProcedureType()
	{
		Map<String, List<String>> map = constantsMap.loadConstants();
		List<String> procedureType = map.get("procedure.type");

		return new ResponseEntity<Object>(procedureType, HttpStatus.OK);
	}

	@RequestMapping(value = "/procedure/type/{type}", method = RequestMethod.GET)
	public ResponseEntity<Object> getProcedureSubType(@PathVariable("type") String type)
	{
		String key = "procedure." + type;
		Map<String, List<String>> map = constantsMap.loadConstants();
		List<String> subtypes = map.get(key);
		return new ResponseEntity<Object>(subtypes, HttpStatus.OK);
	}

	@RequestMapping(value = "/procedure/type/{type}/subtype/{subtype}/field/{field}", method = RequestMethod.GET)
	public ResponseEntity<Object> getProcedureDropDownValues(@PathVariable("type") String type, @PathVariable("subtype") String subtype,
			@PathVariable("field") String field)
	{
		String key = "procedure." + type + "." + subtype + "." + field;
		Map<String, List<String>> map = constantsMap.loadConstants();
		List<String> values = map.get(key);
		return new ResponseEntity<Object>(values, HttpStatus.OK);
	}

	@RequestMapping(value = "/{studyid}/procedure/{procedureid}/assign/{type}/user/{userid}", method = RequestMethod.PUT)
	public ResponseEntity<Object> assignTechnician(@PathVariable("studyid") Long studyId, @PathVariable("procedureid") Long procedureId,
			@PathVariable("type") String type, @PathVariable("userid") Long userId)
	{
		try
		{
			StudyProcedure procedure = procedureService.assignTechnician(studyId, procedureId, type, userId);
			return new ResponseEntity<Object>(procedure, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

	}

	@RequestMapping(value = "/procedurevalues", method = RequestMethod.GET)
	public ResponseEntity<Object> getAllProcedureValues()
	{
		try
		{
			List<Constants> constants = procedureService.getAllProcedureValues();
			return new ResponseEntity<Object>(constants, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

	}

	@RequestMapping(value = "/{studyid}/allanimals", method = RequestMethod.GET)
	public ResponseEntity<Object> getAllStudyAnimals(@PathVariable("studyid") Long studyId)
	{
		try
		{
			List<Animals> animals = studyService.getAllStudyAnimals(studyId);
			return new ResponseEntity<Object>(animals, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/{studyid}/procedure/{procedureid}/animals", method = RequestMethod.PUT)
	public ResponseEntity<Object> assignAnimalsToProcedure(@PathVariable("studyid") Long studyId, @PathVariable("procedureid") Long procedureId,
			@RequestBody StudyDTO studyDTO)
	{
		try
		{
			StudyProcedure procedure = procedureService.assignAnimalsToProcedure(studyId, procedureId, studyDTO);
			return new ResponseEntity<Object>(procedure, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/{studyid}/procedure/{procedureid}/animals/{animalid}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deleteAnimalsFromProcedure(@PathVariable("studyid") Long studyId, @PathVariable("procedureid") Long procedureId,
			 @PathVariable("animalid") Long animalId)
	{
		try
		{
			StudyProcedure procedure = procedureService.deleteAnimalsFromProcedure(studyId, procedureId, animalId);
			return new ResponseEntity<Object>(procedure, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/{studyid}/procedure/{procedureid}/schedule", method = RequestMethod.PUT)
	public ResponseEntity<Object> scheduleProcedure(@PathVariable("studyid") Long studyId, @PathVariable("procedureid") Long procedureId,
			@RequestBody ProcedureSchedule procedureSchedule)
	{
		try
		{
			StudyProcedure procedure = procedureService.scheduleProcedure(studyId, procedureId, procedureSchedule);
			return new ResponseEntity<Object>(procedure, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/{studyid}/procedure/{procedureid}/schedule/{scheduleid}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateScheduleProcedure(@PathVariable("studyid") Long studyId, @PathVariable("procedureid") Long procedureId,
			@PathVariable("scheduleid") Long scheduleId, @RequestBody ProcedureSchedule procedureSchedule)
	{
		try
		{
			procedureSchedule = procedureService.updateScheduleProcedure(studyId, procedureId, scheduleId, procedureSchedule);
			return new ResponseEntity<Object>(procedureSchedule, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/{studyid}/procedure/{procedureid}/schedule/{scheduleid}/animals", method = RequestMethod.GET)
	public ResponseEntity<Object> getAnimalListForSchedule(@PathVariable("studyid") Long studyId, @PathVariable("procedureid") Long procedureId,
			@PathVariable("scheduleid") Long scheduleId)
	{
		try
		{
			List<AnimalHistory> animalHistory = procedureService.getAnimalListForSchedule(studyId, procedureId, scheduleId, null);
			
			if(animalHistory != null && !animalHistory.isEmpty())
			{
				return new ResponseEntity<Object>(animalHistory, HttpStatus.OK);
			}
			else
			{
				List<Animals> animals = procedureService.findAnimalsByProcedureId(procedureId);
				return new ResponseEntity<Object>(animals, HttpStatus.OK);
			}
			
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/{studyid}/procedure/{procedureid}/schedule/{scheduleid}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deleteScheduleProcedure(@PathVariable("studyid") Long studyId, @PathVariable("procedureid") Long procedureId,
			@PathVariable("scheduleid") Long scheduleId)
	{
		try
		{
			procedureService.deleteScheduleProcedure(studyId, procedureId, scheduleId);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/getProcedureSchedules/vettec/{userid}", method = RequestMethod.GET)
	public ResponseEntity<Object> getProcedureSchedulesForVetTec(@PathVariable("userid") Long userid)
	{
		try
		{
			Iterable<StudyProcedureDTO> procedureSchedules = procedureService.getProcedureSchedules(userid);
			return new ResponseEntity<Object>(procedureSchedules, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/getProcedureSchedules/labtec/{userid}", method = RequestMethod.GET)
	public ResponseEntity<Object> getProcedureSchedulesForLabTec(@PathVariable("userid") Long userid)
	{
		try
		{
			Iterable<StudyProcedureDTO> procedureSchedules = procedureService.getProcedureSchedulesForLabTec(userid);
			return new ResponseEntity<Object>(procedureSchedules, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/getProcedureSchedules/pm", method = RequestMethod.GET)
	public ResponseEntity<Object> getProcedureSchedulesForPM()
	{
		try
		{
			Iterable<StudyProcedureDTO> procedureSchedules = procedureService.getProcedureSchedulesForPM();
			return new ResponseEntity<Object>(procedureSchedules, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/{studyid}/procedure/{procedureid}/schedule/{scheduleid}/animal/{animalid}", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateAnimalsInSchedule(@PathVariable("studyid") Long studyId, @PathVariable("procedureid") Long procedureId,
			@PathVariable("scheduleid") Long scheduleId, @PathVariable("animalid") Long animalId, @RequestBody ScheduleAnimalDTO scheduleAnimalDTO)
	{
		try
		{
			ProcedureSchedule procedureSchedule = procedureService.updateAnimalsInSchedule(studyId, procedureId, scheduleId, animalId,
					scheduleAnimalDTO);
			return new ResponseEntity<Object>(procedureSchedule, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/getProcedures/schedule/{scheduleid}", method = RequestMethod.GET)
	public ResponseEntity<Object> getProcedures(@PathVariable("scheduleid") Long scheduleId)
	{
		try
		{
			StudyProcedure procedure = procedureService.getProcedures(scheduleId);
			return new ResponseEntity<Object>(procedure, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/{studyid}/procedure", method = RequestMethod.GET)
	public ResponseEntity<Object> getProcedureSchedulesForStudy(@PathVariable("studyid") Long studyId)
	{
		try
		{
			Iterable<StudyProcedureDTO> procedureSchedules = procedureService.getProcedureSchedulesForStudy(studyId);
			return new ResponseEntity<Object>(procedureSchedules, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@RequestMapping(value = "/{studyid}/cloneschedule", method = RequestMethod.PUT)
	public ResponseEntity<Object> cloneProcedureSchedules(@PathVariable("studyid") Long studyId, @RequestBody ScheduleCloneDTO scheduleCloneDTO)
	{
		try
		{
			Iterable<StudyProcedureDTO> procedureSchedules = procedureService.cloneProcedureSchedules(studyId, scheduleCloneDTO);
			return new ResponseEntity<Object>(procedureSchedules, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}
	
	@RequestMapping(value = "/{studyid}/schedule/{scheduleid}/volume", method = RequestMethod.PUT)
	public ResponseEntity<Object> calculateVolume(@PathVariable("studyid") Long studyId, @PathVariable("scheduleid") Long scheduleid,@RequestBody VolumeDTO volumeDTO)
	{
		try
		{
			Iterable<Animals> procedureSchedules = procedureService.calculateVolume(studyId, scheduleid,volumeDTO);
			return new ResponseEntity<Object>(procedureSchedules, HttpStatus.OK);
		} catch (Exception exception)
		{
			exception.printStackTrace();
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}
	
	@RequestMapping(value = "/{studyid}/procedure/{procedureid}/schedule/{scheduleid}/tprw", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateTPRWvalue(@PathVariable("studyid") Long studyId, @PathVariable("procedureid") Long procedureId,@PathVariable("scheduleid") Long scheduleId,@RequestBody List<AnimalsTPRWDTO> animalsTPRWDTO)
	{
		try
		{
			Iterable<Animals> procedureSchedules = procedureService.updateTPRWvalue(studyId,procedureId, scheduleId, animalsTPRWDTO);
			
			return new ResponseEntity<Object>(procedureSchedules, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}
	
	@RequestMapping(value = "/staticdata", method = RequestMethod.GET)
	public ResponseEntity<Object> updateTPRWvalue(@RequestParam("key") String key)
	{
		try
		{
			Map<String, List<String>> map = constantsMap.loadConstants();
			List<String> values = map.get(key);
			return new ResponseEntity<Object>(values, HttpStatus.OK);
		} catch (Exception exception)
		{
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

}
