package com.ardxx.ardxxapi.animalmanagement.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.ardxx.ardxxapi.animalmanagement.domain.Study;

public class StudyDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long id;
	
	private String serialNumber;

	private String name;

	private String chargeCode;

	private Date createDate;

	private Date closedDate;

	private String comment;

	private Long protocolId;

	private List<Long> animalIds;
	
	
	public Study toStudy(Study study)
	{
		study.setSerialNumber(this.serialNumber);
		study.setName(this.name);
		study.setChargeCode(this.chargeCode);
		study.setCreateDate(this.createDate);
		study.setClosedDate(this.closedDate);
		study.setComment(this.comment);
		return study;
	}
	
	public String getSerialNumber()
	{
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber)
	{
		this.serialNumber = serialNumber;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getChargeCode()
	{
		return chargeCode;
	}

	public void setChargeCode(String chargeCode)
	{
		this.chargeCode = chargeCode;
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}

	public Date getClosedDate()
	{
		return closedDate;
	}

	public void setClosedDate(Date closedDate)
	{
		this.closedDate = closedDate;
	}

	public String getComment()
	{
		return comment;
	}

	public void setComment(String comment)
	{
		this.comment = comment;
	}

	public Long getProtocolId()
	{
		return protocolId;
	}

	public void setProtocolId(Long protocolId)
	{
		this.protocolId = protocolId;
	}

	public List<Long> getAnimalIds()
	{
		return animalIds;
	}

	public void setAnimalIds(List<Long> animalIds)
	{
		this.animalIds = animalIds;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

}
