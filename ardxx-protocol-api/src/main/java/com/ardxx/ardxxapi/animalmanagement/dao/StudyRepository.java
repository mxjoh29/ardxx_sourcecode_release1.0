package com.ardxx.ardxxapi.animalmanagement.dao;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.ardxx.ardxxapi.animalmanagement.domain.Study;

public interface StudyRepository extends PagingAndSortingRepository<Study, Long>
{

	public List<Study> findByAnimalsId(@Param("id") Long animalId);
	
	public Study findByProceduresId(@Param("id") Long animalId);
	
	public Study findByProceduresProcedureSchedulesId(@Param("id") Long animalId);
	
	@Query(nativeQuery=true,value="select procedures_id from study_procedures where study_id = :studyId")
	List<BigInteger> findById(@Param("studyId") Long id);
	
	public  Page<Study> findByIsClinicalAndIsDraft(Pageable pageable,@Param("isClinical") boolean isClinical,@Param("isDraft") boolean isDraft);
}
