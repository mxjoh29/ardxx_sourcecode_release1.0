package com.ardxx.ardxxapi.animalmanagement.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.stereotype.Repository;

import com.ardxx.ardxxapi.animalmanagement.domain.AnimalTestResults;
import com.ardxx.ardxxapi.animalmanagement.dto.AnimalTestResultDTO;
import com.ardxx.ardxxapi.common.ARDXXUtils;

@Repository
public class AnimalTestRepositoryImpl implements AnimalTestRepositoryCustom
{
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional(value = TxType.REQUIRED)
	public List<AnimalTestResults> getCBCTestResults(AnimalTestResultDTO animalTestResultDTO)
	{
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<AnimalTestResults> criteriaQuery = criteriaBuilder.createQuery(AnimalTestResults.class);

		Root<AnimalTestResults> animalTestResults = criteriaQuery.from(AnimalTestResults.class);

		List<Predicate> predicates = new ArrayList<Predicate>();

		if (!ARDXXUtils.isEmpty(animalTestResultDTO.getAnimalGroup()))
		{
			predicates.add(criteriaBuilder.equal(animalTestResults.get("groupNumber"), animalTestResultDTO.getAnimalGroup()));
		}
		if (animalTestResultDTO.getFromDate() != null)
		{
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(animalTestResults.<Date> get("collectedDate"), animalTestResultDTO.getFromDate()));
		}

		if (animalTestResultDTO.getToDate() != null)
		{
			predicates.add(criteriaBuilder.lessThanOrEqualTo(animalTestResults.<Date> get("collectedDate"), animalTestResultDTO.getToDate()));
		}

		if (ARDXXUtils.isEmpty(animalTestResultDTO.getStatus()))
		{
			predicates.add(criteriaBuilder.equal(animalTestResults.get("status"), animalTestResultDTO.getStatus()));
		}

		// query itself
		criteriaQuery.select(animalTestResults).where(predicates.toArray(new Predicate[] {}));

		// execute query and do something with result

		return entityManager.createQuery(criteriaQuery).getResultList();

		// return null;
	}

}
