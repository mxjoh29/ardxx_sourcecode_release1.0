package com.ardxx.ardxxapi.animalmanagement.dto;

import java.io.Serializable;
import java.util.List;

import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureSchedule;

public class ProcedureDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long procedureId;

	private List<ProcedureSchedule> procedureSchedules;

	public Long getProcedureId()
	{
		return procedureId;
	}

	public void setProcedureId(Long procedureId)
	{
		this.procedureId = procedureId;
	}

	public List<ProcedureSchedule> getProcedureSchedules()
	{
		return procedureSchedules;
	}

	public void setProcedureSchedules(List<ProcedureSchedule> procedureSchedules)
	{
		this.procedureSchedules = procedureSchedules;
	}

}
