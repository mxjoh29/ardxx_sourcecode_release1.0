package com.ardxx.ardxxapi.animalmanagement.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ardxx.ardxxapi.animalmanagement.dao.AnimalGroupRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.AnimalsRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.StudyRepository;
import com.ardxx.ardxxapi.animalmanagement.domain.AnimalGroup;
import com.ardxx.ardxxapi.animalmanagement.domain.AnimalStatus;
import com.ardxx.ardxxapi.animalmanagement.domain.Animals;
import com.ardxx.ardxxapi.animalmanagement.domain.Study;
import com.ardxx.ardxxapi.animalmanagement.dto.StudyDTO;
import com.ardxx.ardxxapi.common.Messages;
import com.ardxx.ardxxapi.exception.ARDXXAppException;
import com.ardxx.ardxxapi.protocol.dao.ProtocolRepository;
import com.ardxx.ardxxapi.protocol.domain.Protocol;

@Service
public class StudyServiceimpl implements StudyService
{

	@Autowired
	StudyRepository studyRepository;

	@Autowired
	AnimalsRepository animalsRepository;

	@Autowired
	ProtocolRepository protocolRepository;

	@Autowired
	AnimalGroupRepository animalGroupRepository;

	@Override
	public Study createStudy(StudyDTO studyDTO)
	{
		return persistStudy(studyDTO, new Study());
	}

	@Override
	public Study updateStudy(Long studyId, StudyDTO studyDTO)
	{
		Study study = studyRepository.findOne(studyId);

		return persistStudy(studyDTO, study);
	}

	private Study persistStudy(StudyDTO studyDTO, Study study)
	{
		study = studyDTO.toStudy(study);

		Protocol protocol = null;

		if (studyDTO.getProtocolId() != null)
		{
			protocol = protocolRepository.findOne(studyDTO.getProtocolId());
			study.setProtocol(protocol);
		}
		study.setCreateDate(new Date());
		return studyRepository.save(study);
	}

	@Override
	public Page<Study> getStudies(int page, int size)
	{
		Pageable pageable = new PageRequest(page, size);
		return studyRepository.findByIsClinicalAndIsDraft(pageable,false,false);
	}

	@Override
	public Study addAnimals(Long studyId, StudyDTO studyDTO)
	{
		Study study = studyRepository.findOne(studyId);
		List<Animals> animals = new ArrayList<Animals>();
		Iterable<Animals> iterable = animalsRepository.findAll(studyDTO.getAnimalIds());
		for (Animals animals2 : iterable)
		{
			animals2.setStatus(AnimalStatus.ACTIVE.name());
			animals.add(animals2);
			animalsRepository.save(animals2);
		}
		animals.addAll(study.getAnimals());
		study.setAnimals(animals);

		return studyRepository.save(study);
	}

	@Override
	public Study getStudy(Long studyId)
	{
		return studyRepository.findOne(studyId);
	}

	@Override
	public List<AnimalGroup> createAnimalsGroup(Long studyId, AnimalGroup animalGroup)
	{
		List<AnimalGroup> animalGroups = null;
		Study study = studyRepository.findOne(studyId);
		animalGroup = animalGroupRepository.save(animalGroup);
		if (study.getAnimalGroups() != null)
		{
			study.getAnimalGroups().add(animalGroup);
		} else
		{
			animalGroups = new ArrayList<AnimalGroup>();
			animalGroups.add(animalGroup);
			study.setAnimalGroups(animalGroups);
		}

		study = studyRepository.save(study);
		return study.getAnimalGroups();
	}

	@Override
	public List<Animals> getAnimals(Long studyId)
	{
		Study study = studyRepository.findOne(studyId);

		return study.getAnimals();
	}

	@Override
	public Study assignAnimalsGroup(Long studyId, Long groupId, StudyDTO studyDTO)
	{
		Study study = studyRepository.findOne(studyId);
		AnimalGroup animalGroup = animalGroupRepository.findOne(groupId);

		List<Animals> animalsInGroup = animalGroup.getAnimals();
		List<Animals> animalsInStudy = study.getAnimals();

		List<Animals> animalsRemoveInStudy = new ArrayList<Animals>();

		for (Animals animals : animalsInStudy)
		{
			for (Long animalsId : studyDTO.getAnimalIds())
			{
				if (animals.getId() == animalsId)
				{
					animalsRemoveInStudy.add(animals);
				}
			}

		}

		if (animalsInGroup != null)
		{
			animalsInGroup.addAll(animalsRemoveInStudy);
		} else
		{
			animalsInGroup = new ArrayList<Animals>();
			animalsInGroup.addAll(animalsRemoveInStudy);
		}

		animalGroup.setAnimals(animalsInGroup);

		study.getAnimals().removeAll(animalsRemoveInStudy);

		animalGroupRepository.save(animalGroup);

		return studyRepository.save(study);
	}

	@Override
	public List<Animals> getAnimalsInGroup(Long studyId, Long groupId)
	{
		AnimalGroup animalGroup = animalGroupRepository.findOne(groupId);
		return animalGroup.getAnimals();
	}

	@Override
	public Study unassignAnimalsGroup(Long studyId, Long groupId, StudyDTO studyDTO)
	{

		Study study = studyRepository.findOne(studyId);
		AnimalGroup animalGroup = animalGroupRepository.findOne(groupId);

		List<Animals> animalsInGroup = animalGroup.getAnimals();
		List<Animals> animalsInStudy = study.getAnimals();

		// List<Animals> animalsInGroup = animalGroup.getAnimals();
		List<Animals> animalsRemoveInGroup = new ArrayList<Animals>();

		for (Animals animals : animalsInGroup)
		{
			for (Long animalsId : studyDTO.getAnimalIds())
			{
				if (animals.getId() == animalsId)
				{
					animalsRemoveInGroup.add(animals);
				}
			}

		}

		if (animalsInStudy != null)
		{
			animalsInStudy.addAll(animalsRemoveInGroup);
		} else
		{
			animalsInStudy = new ArrayList<Animals>();
			animalsInStudy.addAll(animalsRemoveInGroup);
		}

		animalGroup.getAnimals().removeAll(animalsRemoveInGroup);

		study.setAnimals(animalsInStudy);

		animalGroupRepository.save(animalGroup);

		return studyRepository.save(study);

	}

	@Override
	public List<AnimalGroup> getAnimalsGroups(Long studyId)
	{
		List<AnimalGroup>  animalGroups = studyRepository.findOne(studyId).getAnimalGroups();
		for (AnimalGroup animalGroup : animalGroups)
		{
			List<Animals> animals = animalGroup.getAnimals();
			for (Animals animal : animals)
			{
				animal.setGroupName(animalGroup.getGroupName());
			}
		}
		return animalGroups;
	}

	@Override
	public AnimalGroup getAnimalsGroupById(Long studyId, Long groupId)
	{
		return animalGroupRepository.findOne(groupId);
	}

	@Override
	public Study removeAnimalsInStudy(Long studyId, StudyDTO studyDTO)
	{
		Study study = studyRepository.findOne(studyId);
		List<Animals> animalsToRemove = new ArrayList<Animals>();
		for (Animals animal : study.getAnimals())
		{
			for (Long animalId : studyDTO.getAnimalIds())
			{
				if (animalId == animal.getId())
				{
					animal.setStatus(AnimalStatus.HOLDING.name());
					animalsRepository.save(animal);
					animalsToRemove.add(animal);
				}
			}
		}

		if (!animalsToRemove.isEmpty())
		{
			study.getAnimals().removeAll(animalsToRemove);
		}

		return studyRepository.save(study);
	}

	@Override
	public List<AnimalGroup> deleteAnimalsGroup(Long studyId, Long groupId) throws Exception
	{
		Study study = studyRepository.findOne(studyId);
		if (study == null)
		{
			throw new ARDXXAppException(Messages.properties.getProperty("no.study"));
		} else
		{
			AnimalGroup animalGroup = animalGroupRepository.findOne(groupId);
			if (animalGroup == null)
			{
				throw new ARDXXAppException(Messages.properties.getProperty("study.no.group"));
			} else
			{
				study.getAnimalGroups().remove(animalGroup);
				study.getAnimals().addAll(animalGroup.getAnimals());
				study = studyRepository.save(study);
				animalGroupRepository.delete(groupId);
				return study.getAnimalGroups();
			}
		}

	}

	@Override
	public List<Animals> getAllStudyAnimals(Long studyId)
	{
		List<Animals> animals = new ArrayList<Animals>();
		Study study = studyRepository.findOne(studyId);
		List<Animals> studyPool = study.getAnimals();
		if (studyPool != null)
		{
			animals.addAll(studyPool);
		}

		List<AnimalGroup> animalGroups = study.getAnimalGroups();

		if (animalGroups != null)
		{
			for (AnimalGroup animalGroup : animalGroups)
			{
				List<Animals> animals2 = animalGroup.getAnimals();
				for (Animals animals3 : animals2)
				{
					animals3.setGroupName(animalGroup.getGroupName());
					animals.add(animals3);
				}
//				animals.addAll(animalGroup.getAnimals());
			}
		}

		return animals;
	}

}
