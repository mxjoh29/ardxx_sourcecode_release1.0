package com.ardxx.ardxxapi.animalmanagement.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ardxx.ardxxapi.animalmanagement.domain.AnimalHistory;
import com.ardxx.ardxxapi.animalmanagement.domain.Animals;
import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureSchedule;
import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureTest;
import com.ardxx.ardxxapi.animalmanagement.domain.Study;
import com.ardxx.ardxxapi.animalmanagement.domain.StudyProcedure;
import com.ardxx.ardxxapi.animalmanagement.dto.AnimalsTPRWDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.ScheduleAnimalDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.ScheduleCloneDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.StudyDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.StudyProcedureDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.VolumeDTO;
import com.ardxx.ardxxapi.common.domain.Constants;

public interface ProcedureService
{

	StudyProcedure createProcedure(Long studyId,StudyProcedure procedure);

	StudyProcedure getProcedure(Long procedureId);

	Page<StudyProcedure> getProcedureList(int page, int size);

	StudyProcedure assignTechnician(Long studyId, Long procedureId, String type, Long userId);

	List<Constants> getAllProcedureValues();

	StudyProcedure assignAnimalsToProcedure(Long studyId, Long procedureId, StudyDTO studyDTO);

	StudyProcedure deleteAnimalsFromProcedure(Long studyId, Long procedureId, Long animalId);

	StudyProcedure scheduleProcedure(Long studyId, Long procedureId, ProcedureSchedule procedureSchedule);

	StudyProcedure createProcedureTests(Long studyId, Long procedureId, ProcedureTest procedureTest) throws Exception;

	StudyProcedure changeProcedureStatus(Long studyId, Long procedureId, String status);

	ProcedureSchedule updateScheduleProcedure(Long studyId, Long procedureId, Long scheduleId, ProcedureSchedule procedureSchedule) throws Exception;

	void deleteScheduleProcedure(Long studyId, Long procedureId, Long scheduleId) throws Exception;

	Iterable<StudyProcedureDTO> getProcedureSchedules(Long userid);

	ProcedureSchedule updateAnimalsInSchedule(Long studyId, Long procedureId, Long scheduleId, Long animalId,ScheduleAnimalDTO scheduleAnimalDTO);

	StudyProcedure getProcedures(Long scheduleId);

	Iterable<StudyProcedureDTO> getProcedureSchedulesForLabTec(Long userid);

	List<AnimalHistory> getAnimalListForSchedule(Long studyId, Long procedureId, Long scheduleId, ProcedureSchedule procedureSchedule);
	
	List<Animals> findAnimalsByProcedureId(Long procedureId);

	Iterable<StudyProcedureDTO> getProcedureSchedulesForPM();

	Iterable<StudyProcedureDTO> getProcedureSchedulesForStudy(Long studyId) throws Exception;

	Iterable<StudyProcedureDTO> cloneProcedureSchedules(Long studyId, ScheduleCloneDTO scheduleCloneDTO);

	Iterable<Animals> calculateVolume(Long studyId, Long scheduleid, VolumeDTO volumeDTO);

	Iterable<Animals> updateTPRWvalue(Long studyId, Long procedureId, Long scheduleId, List<AnimalsTPRWDTO> animalsTPRWDTO);

	Page<StudyProcedure> getClinicalProcedures(int page, int size);;

}
