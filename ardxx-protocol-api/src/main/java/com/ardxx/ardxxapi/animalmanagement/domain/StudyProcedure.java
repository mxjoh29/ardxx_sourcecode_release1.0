package com.ardxx.ardxxapi.animalmanagement.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.tn.security.basic.domain.User;

/**
 * 
 * @author Raj
 *
 */
@Entity
public class StudyProcedure implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private String procedureType;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private BloodSample bloodSample;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private Biopsy biopsy;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private Inoculation inoculation;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private Treatment treatment;

	/*@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private Injection injection;*/

	private String status;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private User vetTechnician;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private User labTechnician;

//	@ElementCollection
	@ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	@JoinTable(name = "study_procedure_animal", joinColumns = @JoinColumn(name = "study_procedure", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "animals_id", referencedColumnName = "id"))
	private List<Animals> animals;
	
	@ElementCollection
	private List<ProcedureSchedule> procedureSchedules;
	
	@Transient
	private String studyName;
	
	@Transient
	private Long studyId;
	
	

	public String getStudyName()
	{
		return studyName;
	}

	public void setStudyName(String studyName)
	{
		this.studyName = studyName;
	}

	public Long getStudyId()
	{
		return studyId;
	}

	public void setStudyId(Long studyId)
	{
		this.studyId = studyId;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getProcedureType()
	{
		return procedureType;
	}

	public void setProcedureType(String procedureType)
	{
		this.procedureType = procedureType;
	}

	public BloodSample getBloodSample()
	{
		return bloodSample;
	}

	public void setBloodSample(BloodSample bloodSample)
	{
		this.bloodSample = bloodSample;
	}

	public Biopsy getBiopsy()
	{
		return biopsy;
	}

	public void setBiopsy(Biopsy biopsy)
	{
		this.biopsy = biopsy;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	/*public Injection getInjection()
	{
		return injection;
	}

	public void setInjection(Injection injection)
	{
		this.injection = injection;
	}*/

	public List<Animals> getAnimals()
	{
		return animals;
	}

	public void setAnimals(List<Animals> animals)
	{
		this.animals = animals;
	}

	public List<ProcedureSchedule> getProcedureSchedules()
	{
		return procedureSchedules;
	}

	public void setProcedureSchedules(List<ProcedureSchedule> procedureSchedules)
	{
		this.procedureSchedules = procedureSchedules;
	}

	public User getVetTechnician()
	{
		return vetTechnician;
	}

	public void setVetTechnician(User vetTechnician)
	{
		this.vetTechnician = vetTechnician;
	}

	public User getLabTechnician()
	{
		return labTechnician;
	}

	public void setLabTechnician(User labTechnician)
	{
		this.labTechnician = labTechnician;
	}

	public Inoculation getInoculation()
	{
		return inoculation;
	}

	public void setInoculation(Inoculation inoculation)
	{
		this.inoculation = inoculation;
	}

	public Treatment getTreatment()
	{
		return treatment;
	}

	public void setTreatment(Treatment treatment)
	{
		this.treatment = treatment;
	}

}
