package com.ardxx.ardxxapi.animalmanagement.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ardxx.ardxxapi.animalmanagement.domain.AnimalGroup;
import com.ardxx.ardxxapi.animalmanagement.domain.Animals;
import com.ardxx.ardxxapi.animalmanagement.domain.Study;
import com.ardxx.ardxxapi.animalmanagement.dto.StudyDTO;

public interface StudyService
{

	/**
	 * Service method to create Study
	 * 
	 * @param studyDTO
	 *            {@link StudyDTO}
	 * @return Returns newly created {@link Study}
	 */
	Study createStudy(StudyDTO studyDTO);

	/**
	 * Service method to get all the studies
	 * 
	 * @param page
	 *            {@link Page} object which has pagination information and the {@link List} of {@link Study}
	 * @param size
	 * @return
	 */
	Page<Study> getStudies(int page, int size);

	/**
	 * Service method to add animals to the existing Study
	 * 
	 * @param studyId
	 *            study id
	 * @param studyDTO
	 *            {@link StudyDTO}
	 * @return Returns updated {@link Study}
	 */
	Study addAnimals(Long studyId, StudyDTO studyDTO);

	/**
	 * Service method to get a {@link Study} for the given id
	 * 
	 * @param studyId
	 * @return Returns {@link Study}
	 */
	Study getStudy(Long studyId);

	/**
	 * Service method to create Animals group for a study
	 * 
	 * @param studyId
	 *            study id
	 * @param animalGroup
	 *            {@link AnimalGroup}
	 * @return Returns newly created List of {@link AnimalGroup}
	 */
	List<AnimalGroup> createAnimalsGroup(Long studyId, AnimalGroup animalGroup);

	/**
	 * Service method to get all the animals from the Study pool
	 * 
	 * @return
	 */
	List<Animals> getAnimals(Long studyId);

	Study assignAnimalsGroup(Long studyId, Long groupId, StudyDTO studyDTO);

	List<Animals> getAnimalsInGroup(Long studyId, Long groupId);

	Study unassignAnimalsGroup(Long studyId, Long groupId, StudyDTO studyDTO);

	List<AnimalGroup> getAnimalsGroups(Long studyId);

	AnimalGroup getAnimalsGroupById(Long studyId, Long groupId);

	Study removeAnimalsInStudy(Long studyId, StudyDTO studyDTO);

	Study updateStudy(Long studyId,StudyDTO studyDTO);

	List<AnimalGroup> deleteAnimalsGroup(Long studyId, Long groupId) throws Exception;

	List<Animals> getAllStudyAnimals(Long studyId);

}
