package com.ardxx.ardxxapi.animalmanagement.dto;

import java.io.Serializable;
import java.util.Date;

public class ScheduleCloneDTO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long studyId;
	private Date fromDate;
	private Date toDate;

	public Long getStudyId()
	{
		return studyId;
	}

	public void setStudyId(Long studyId)
	{
		this.studyId = studyId;
	}

	public Date getFromDate()
	{
		return fromDate;
	}

	public void setFromDate(Date fromDate)
	{
		this.fromDate = fromDate;
	}

	public Date getToDate()
	{
		return toDate;
	}

	public void setToDate(Date toDate)
	{
		this.toDate = toDate;
	}

}
