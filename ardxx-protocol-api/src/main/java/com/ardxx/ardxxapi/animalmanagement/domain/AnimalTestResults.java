package com.ardxx.ardxxapi.animalmanagement.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class AnimalTestResults implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ElementCollection
	private List<AnimalTest> animalTests;

	private String groupNumber;
	private String accessionNumber;
	private String breed;
	private String status;
	
	@ManyToOne
	private Animals animalId;

	private String sex;
	private String species;
	private String age;
	private Date collectedDate;

	private String agRatio;
	private String absoluteBasophil;
	private String absoluteEosinophil;
	private String absoluteLymphocyte;
	private String absoluteMetamyelocytes;
	private String absoluteMonocyte;
	private String absoluteNeutrophilBand;
	private String absoluteNeutrophilSeg;
	private String absolutePromyelocytes;
	private String albumin;
	private String alkalinePhosphatase;
	private String anisocytosis;
	private String bcRatio;
	private String basophil;
	private String bicarbonate;
	private String bun;
	private String calcium;
	private String chloride;
	private String cholesterol;
	private String cpk;
	private String creatinine;
	private String directBilirubin;
	private String eosinophil;
	private String globulin;
	private String glucose;
	private String hct;
	private String heinzBodies;
	private String hemolysisIndex;
	private String hgb;
	private String indirectBilirubin;
	private String lipemiaIndex;
	private String lymphocyte;
	private String mch;
	private String mchc;
	private String mcv;
	private String metamyelocyte;
	private String monocyte;
	private String myelocyte;
	private String nakRatio;
	private String neutrophilBand;
	private String neutrophilSeg;
	private String nrbc;
	private String phosphorus;
	private String plateletCount;
	private String plateletEstimate;
	private String poikilocytosis;
	private String polychromasia;
	private String potassium;
	private String promyelocyte;
	private String rbc;
	private String remarks;
	private String sgot;
	private String sgpt;
	private String sodium;
	private String totalBilirubin;
	private String totalProtein;
	private String wbc;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getGroupNumber()
	{
		return groupNumber;
	}

	public void setGroupNumber(String groupNumber)
	{
		this.groupNumber = groupNumber;
	}

	public String getAccessionNumber()
	{
		return accessionNumber;
	}

	public void setAccessionNumber(String accessionNumber)
	{
		this.accessionNumber = accessionNumber;
	}

	public String getBreed()
	{
		return breed;
	}

	public void setBreed(String breed)
	{
		this.breed = breed;
	}

	public Animals getAnimalId()
	{
		return animalId;
	}

	public void setAnimalId(Animals animalId)
	{
		this.animalId = animalId;
	}

	public String getSex()
	{
		return sex;
	}

	public void setSex(String sex)
	{
		this.sex = sex;
	}

	public String getSpecies()
	{
		return species;
	}

	public void setSpecies(String species)
	{
		this.species = species;
	}

	public String getAge()
	{
		return age;
	}

	public void setAge(String age)
	{
		this.age = age;
	}

	public Date getCollectedDate()
	{
		return collectedDate;
	}

	public void setCollectedDate(Date collectedDate)
	{
		this.collectedDate = collectedDate;
	}

	public String getAgRatio()
	{
		return agRatio;
	}

	public void setAgRatio(String agRatio)
	{
		this.agRatio = agRatio;
	}

	public String getAbsoluteBasophil()
	{
		return absoluteBasophil;
	}

	public void setAbsoluteBasophil(String absoluteBasophil)
	{
		this.absoluteBasophil = absoluteBasophil;
	}

	public String getAbsoluteEosinophil()
	{
		return absoluteEosinophil;
	}

	public void setAbsoluteEosinophil(String absoluteEosinophil)
	{
		this.absoluteEosinophil = absoluteEosinophil;
	}

	public String getAbsoluteLymphocyte()
	{
		return absoluteLymphocyte;
	}

	public void setAbsoluteLymphocyte(String absoluteLymphocyte)
	{
		this.absoluteLymphocyte = absoluteLymphocyte;
	}

	public String getAbsoluteMetamyelocytes()
	{
		return absoluteMetamyelocytes;
	}

	public void setAbsoluteMetamyelocytes(String absoluteMetamyelocytes)
	{
		this.absoluteMetamyelocytes = absoluteMetamyelocytes;
	}

	public String getAbsoluteMonocyte()
	{
		return absoluteMonocyte;
	}

	public void setAbsoluteMonocyte(String absoluteMonocyte)
	{
		this.absoluteMonocyte = absoluteMonocyte;
	}

	public String getAbsoluteNeutrophilBand()
	{
		return absoluteNeutrophilBand;
	}

	public void setAbsoluteNeutrophilBand(String absoluteNeutrophilBand)
	{
		this.absoluteNeutrophilBand = absoluteNeutrophilBand;
	}

	public String getAbsoluteNeutrophilSeg()
	{
		return absoluteNeutrophilSeg;
	}

	public void setAbsoluteNeutrophilSeg(String absoluteNeutrophilSeg)
	{
		this.absoluteNeutrophilSeg = absoluteNeutrophilSeg;
	}

	public String getAbsolutePromyelocytes()
	{
		return absolutePromyelocytes;
	}

	public void setAbsolutePromyelocytes(String absolutePromyelocytes)
	{
		this.absolutePromyelocytes = absolutePromyelocytes;
	}

	public String getAlbumin()
	{
		return albumin;
	}

	public void setAlbumin(String albumin)
	{
		this.albumin = albumin;
	}

	public String getAlkalinePhosphatase()
	{
		return alkalinePhosphatase;
	}

	public void setAlkalinePhosphatase(String alkalinePhosphatase)
	{
		this.alkalinePhosphatase = alkalinePhosphatase;
	}

	public String getAnisocytosis()
	{
		return anisocytosis;
	}

	public void setAnisocytosis(String anisocytosis)
	{
		this.anisocytosis = anisocytosis;
	}

	public String getBcRatio()
	{
		return bcRatio;
	}

	public void setBcRatio(String bcRatio)
	{
		this.bcRatio = bcRatio;
	}

	public String getBasophil()
	{
		return basophil;
	}

	public void setBasophil(String basophil)
	{
		this.basophil = basophil;
	}

	public String getBicarbonate()
	{
		return bicarbonate;
	}

	public void setBicarbonate(String bicarbonate)
	{
		this.bicarbonate = bicarbonate;
	}

	public String getBun()
	{
		return bun;
	}

	public void setBun(String bun)
	{
		this.bun = bun;
	}

	public String getCalcium()
	{
		return calcium;
	}

	public void setCalcium(String calcium)
	{
		this.calcium = calcium;
	}

	public String getChloride()
	{
		return chloride;
	}

	public void setChloride(String chloride)
	{
		this.chloride = chloride;
	}

	public String getCholesterol()
	{
		return cholesterol;
	}

	public void setCholesterol(String cholesterol)
	{
		this.cholesterol = cholesterol;
	}

	public String getCpk()
	{
		return cpk;
	}

	public void setCpk(String cpk)
	{
		this.cpk = cpk;
	}

	public String getCreatinine()
	{
		return creatinine;
	}

	public void setCreatinine(String creatinine)
	{
		this.creatinine = creatinine;
	}

	public String getDirectBilirubin()
	{
		return directBilirubin;
	}

	public void setDirectBilirubin(String directBilirubin)
	{
		this.directBilirubin = directBilirubin;
	}

	public String getEosinophil()
	{
		return eosinophil;
	}

	public void setEosinophil(String eosinophil)
	{
		this.eosinophil = eosinophil;
	}

	public String getGlobulin()
	{
		return globulin;
	}

	public void setGlobulin(String globulin)
	{
		this.globulin = globulin;
	}

	public String getGlucose()
	{
		return glucose;
	}

	public void setGlucose(String glucose)
	{
		this.glucose = glucose;
	}

	public String getHct()
	{
		return hct;
	}

	public void setHct(String hct)
	{
		this.hct = hct;
	}

	public String getHeinzBodies()
	{
		return heinzBodies;
	}

	public void setHeinzBodies(String heinzBodies)
	{
		this.heinzBodies = heinzBodies;
	}

	public String getHemolysisIndex()
	{
		return hemolysisIndex;
	}

	public void setHemolysisIndex(String hemolysisIndex)
	{
		this.hemolysisIndex = hemolysisIndex;
	}

	public String getHgb()
	{
		return hgb;
	}

	public void setHgb(String hgb)
	{
		this.hgb = hgb;
	}

	public String getIndirectBilirubin()
	{
		return indirectBilirubin;
	}

	public void setIndirectBilirubin(String indirectBilirubin)
	{
		this.indirectBilirubin = indirectBilirubin;
	}

	public String getLipemiaIndex()
	{
		return lipemiaIndex;
	}

	public void setLipemiaIndex(String lipemiaIndex)
	{
		this.lipemiaIndex = lipemiaIndex;
	}

	public String getLymphocyte()
	{
		return lymphocyte;
	}

	public void setLymphocyte(String lymphocyte)
	{
		this.lymphocyte = lymphocyte;
	}

	public String getMch()
	{
		return mch;
	}

	public void setMch(String mch)
	{
		this.mch = mch;
	}

	public String getMchc()
	{
		return mchc;
	}

	public void setMchc(String mchc)
	{
		this.mchc = mchc;
	}

	public String getMcv()
	{
		return mcv;
	}

	public void setMcv(String mcv)
	{
		this.mcv = mcv;
	}

	public String getMetamyelocyte()
	{
		return metamyelocyte;
	}

	public void setMetamyelocyte(String metamyelocyte)
	{
		this.metamyelocyte = metamyelocyte;
	}

	public String getMonocyte()
	{
		return monocyte;
	}

	public void setMonocyte(String monocyte)
	{
		this.monocyte = monocyte;
	}

	public String getMyelocyte()
	{
		return myelocyte;
	}

	public void setMyelocyte(String myelocyte)
	{
		this.myelocyte = myelocyte;
	}

	public String getNakRatio()
	{
		return nakRatio;
	}

	public void setNakRatio(String nakRatio)
	{
		this.nakRatio = nakRatio;
	}

	public String getNeutrophilBand()
	{
		return neutrophilBand;
	}

	public void setNeutrophilBand(String neutrophilBand)
	{
		this.neutrophilBand = neutrophilBand;
	}

	public String getNeutrophilSeg()
	{
		return neutrophilSeg;
	}

	public void setNeutrophilSeg(String neutrophilSeg)
	{
		this.neutrophilSeg = neutrophilSeg;
	}

	public String getNrbc()
	{
		return nrbc;
	}

	public void setNrbc(String nrbc)
	{
		this.nrbc = nrbc;
	}

	public String getPhosphorus()
	{
		return phosphorus;
	}

	public void setPhosphorus(String phosphorus)
	{
		this.phosphorus = phosphorus;
	}

	public String getPlateletCount()
	{
		return plateletCount;
	}

	public void setPlateletCount(String plateletCount)
	{
		this.plateletCount = plateletCount;
	}

	public String getPlateletEstimate()
	{
		return plateletEstimate;
	}

	public void setPlateletEstimate(String plateletEstimate)
	{
		this.plateletEstimate = plateletEstimate;
	}

	public String getPoikilocytosis()
	{
		return poikilocytosis;
	}

	public void setPoikilocytosis(String poikilocytosis)
	{
		this.poikilocytosis = poikilocytosis;
	}

	public String getPolychromasia()
	{
		return polychromasia;
	}

	public void setPolychromasia(String polychromasia)
	{
		this.polychromasia = polychromasia;
	}

	public String getPotassium()
	{
		return potassium;
	}

	public void setPotassium(String potassium)
	{
		this.potassium = potassium;
	}

	public String getPromyelocyte()
	{
		return promyelocyte;
	}

	public void setPromyelocyte(String promyelocyte)
	{
		this.promyelocyte = promyelocyte;
	}

	public String getRbc()
	{
		return rbc;
	}

	public void setRbc(String rbc)
	{
		this.rbc = rbc;
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public String getSgot()
	{
		return sgot;
	}

	public void setSgot(String sgot)
	{
		this.sgot = sgot;
	}

	public String getSgpt()
	{
		return sgpt;
	}

	public void setSgpt(String sgpt)
	{
		this.sgpt = sgpt;
	}

	public String getSodium()
	{
		return sodium;
	}

	public void setSodium(String sodium)
	{
		this.sodium = sodium;
	}

	public String getTotalBilirubin()
	{
		return totalBilirubin;
	}

	public void setTotalBilirubin(String totalBilirubin)
	{
		this.totalBilirubin = totalBilirubin;
	}

	public String getTotalProtein()
	{
		return totalProtein;
	}

	public void setTotalProtein(String totalProtein)
	{
		this.totalProtein = totalProtein;
	}

	public String getWbc()
	{
		return wbc;
	}

	public void setWbc(String wbc)
	{
		this.wbc = wbc;
	}

	public List<AnimalTest> getAnimalTests()
	{
		return animalTests;
	}

	public void setAnimalTests(List<AnimalTest> animalTests)
	{
		this.animalTests = animalTests;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

}
