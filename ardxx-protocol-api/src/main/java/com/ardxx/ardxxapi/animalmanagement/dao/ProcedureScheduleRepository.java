package com.ardxx.ardxxapi.animalmanagement.dao;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureSchedule;

public interface ProcedureScheduleRepository extends PagingAndSortingRepository<ProcedureSchedule, Long>
{
	@Query(nativeQuery=true,value="select procedure_schedules_id from study_procedure_procedure_schedules where study_procedure_id = :studyProcedureId")
	List<BigInteger> findById(@Param("studyProcedureId") Long id);
	
	ProcedureSchedule findByScheduleDateAndId(@Param("scheduleDate") Date scheduleDate,@Param("id") Long id);
}
