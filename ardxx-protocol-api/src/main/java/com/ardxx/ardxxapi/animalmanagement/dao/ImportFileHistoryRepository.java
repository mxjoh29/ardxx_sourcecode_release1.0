package com.ardxx.ardxxapi.animalmanagement.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ardxx.ardxxapi.animalmanagement.domain.ImportFileHistory;

@Repository
public interface ImportFileHistoryRepository extends PagingAndSortingRepository<ImportFileHistory, Long>
{

}
