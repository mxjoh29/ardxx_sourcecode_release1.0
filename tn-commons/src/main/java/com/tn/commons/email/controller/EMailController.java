package com.tn.commons.email.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tn.commons.email.dto.Mail;
import com.tn.commons.email.service.EMailService;

@RestController
@RequestMapping(value = "/email")
public class EMailController {
	
	private final EMailService emailService;

    @Autowired
    EMailController(EMailService emailService) {
        this.emailService = emailService;
    }

    @RequestMapping("/send")
    public void send() {        
        emailService.sendEmail();
    }
    
    @RequestMapping("/sendProtocolEmail")
    public void sendProtocolEmail() { 
    	Mail mail = new Mail();
    	mail.setTemplateName("NewProtocol.vm");
    	mail.setMailTo(new String[]{"gopi@thoughtnotch.com"});
    	mail.setMailFrom("admin@ardxx.com");
    	mail.setMailSubject("New Protocol");
    	
    	Map<String,String> mailContent = new HashMap<String, String>();
    	mailContent.put("firstName", "Gopi");
    	mailContent.put("lastName", "Kris");
    	mailContent.put("location", "Dayton");
    	mail.setMailContent(mailContent);
        emailService.sendEmail(mail);
    }
}
