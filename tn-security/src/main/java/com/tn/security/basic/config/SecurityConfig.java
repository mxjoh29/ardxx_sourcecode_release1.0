package com.tn.security.basic.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
class SecurityConfig extends WebSecurityConfigurerAdapter
{

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private HttpAuthenticationEntryPoint authenticationEntryPoint;

	@Autowired
	private AuthSuccessHandler authSuccessHandler;

	@Autowired
	private AuthFailureHandler authFailureHandler;

	/*
	 * @Bean public AuthenticationProvider authenticationProvider() { DaoAuthenticationProvider authenticationProvider = new
	 * DaoAuthenticationProvider(); authenticationProvider.setUserDetailsService(userDetailsService); authenticationProvider.setPasswordEncoder(new
	 * BCryptPasswordEncoder()); return authenticationProvider; }
	 */

	@Override
	protected void configure(HttpSecurity http) throws Exception
	{

		http.authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/login/**", "/protocol/**", "/users/**", "/animals/**", "/study/**").permitAll().anyRequest()
				.fullyAuthenticated().and().csrf()
				.disable()
				// .authenticationProvider(authenticationProvider())
				.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint).and().formLogin().permitAll().loginProcessingUrl("/login")
				.usernameParameter("email").passwordParameter("password").successHandler(authSuccessHandler).failureHandler(authFailureHandler).and()
				.logout().permitAll();

		http.authorizeRequests().anyRequest().authenticated().antMatchers(HttpMethod.OPTIONS, "/login/**").permitAll();

	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception
	{
		System.out.println("SecurityConfig.configure() >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.QQQQQQQQQQQQQ ");
		auth.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
	}

}