package com.tn.security.basic.service.currentuser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tn.security.basic.domain.CurrentUser;
import com.tn.security.basic.domain.Role;
import com.tn.security.basic.dto.UserDTO;

@Service
public class CurrentUserServiceImpl implements CurrentUserService
{

	private static final Logger LOGGER = LoggerFactory.getLogger(CurrentUserDetailsService.class);

	@Override
	public boolean canAccessUser(CurrentUser currentUser, Long userId)
	{
		LOGGER.debug("Checking if user={} has access to user={}", currentUser, userId);
		return currentUser != null && (currentUser.getRole().iterator().next().getRoleName().equals("ADMIN") || currentUser.getId().equals(userId));
	}

	@Override
	public boolean canChangeStatus(CurrentUser currentUser, UserDTO userDTO)
	{
		System.out.println("CurrentUserServiceImpl.canChangeStatus()" + userDTO.getStatus());
		
		if(isSuperOrLabAdmin(currentUser))
		{
			return true;
		}
		else
		{
			if (userDTO.getStatus() != null && !userDTO.getStatus().equals(currentUser.getUser().getStatus()))
			{
				return false;
			}
			
			if(currentUser.getId() == userDTO.getId())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		

	}
	
	
	private boolean isSuperOrLabAdmin(CurrentUser currentUser)
	{
			for (Role role : currentUser.getRole())
			{
				if (role.getId() == 5)
				{

					return true;
				}
			}

			return false;
	}

}
