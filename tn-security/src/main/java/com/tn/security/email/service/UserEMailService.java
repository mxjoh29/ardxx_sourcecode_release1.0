package com.tn.security.email.service;

import java.util.Date;
import java.util.Map;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.tn.security.email.dto.Mail;

@Service
public class UserEMailService {
	private static final Logger logger = LoggerFactory
			.getLogger(UserEMailService.class);
	
	@Autowired
	private JavaMailSender mailSender ;
	private transient VelocityEngine velocityEngine;
	
	/*
	@Autowired
	public void setMailSender(JavaMailSender mailSender) {
		logger.debug("javamailsender " + mailSender);
		this.mailSender = mailSender;
	}
	*/

	@Autowired
	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}

	public String sendEmail() {

		MimeMessagePreparator preparator = new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {

				mimeMessage.setRecipient(Message.RecipientType.TO,
						new InternetAddress("gopi@thoughtnotch.com"));
				
				mimeMessage.setFrom(new InternetAddress("mail@mycompany.com"));
				mimeMessage.setText("Protocol creation process is initiated ... ");
				
			}
		};
		try {
			this.mailSender.send(preparator);
		} catch (MailException ex) {
			// simply log it and go on...
			System.err.println(ex.getMessage());
		}

		return "hello";
	}

	public void sendEmail(final Mail mail) {
		
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
	        @SuppressWarnings({ "rawtypes", "unchecked" })
			public void prepare(MimeMessage mimeMessage) throws Exception {
	             MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
	             message.setTo(mail.getMailTo());
	             message.setFrom(mail.getMailFrom());
	             message.setSubject(mail.getMailSubject());
	             message.setSentDate(new Date());
	             Map mailContent = mail.getMailContent();	             
	             
	             String text = VelocityEngineUtils.mergeTemplateIntoString(
	                velocityEngine, "./"+mail.getTemplateName(), "UTF-8", mailContent);
	             message.setText(text, true);
	          }
	       };
	      mailSender.send(preparator);			
	}
}
